#define _CRT_SECURE_NO_WARNINGS 1

//https://leetcode.cn/problems/construct-string-from-binary-tree/
//
//class Solution {
//public:
//    string tree2str(TreeNode* root) {
//
//        if (root == nullptr)
//            return "";
//
//        string str = to_string(root->val);
//        if (root->left || root->right)
//        {
//            str += '(';
//            str += tree2str(root->left);
//            str += ')';
//        }
//
//        if (root->right)
//        {
//            str += '(';
//            str += tree2str(root->right);
//            str += ')';
//        }
//
//        return str;
//    }
//};





//https://leetcode.cn/problems/binary-tree-level-order-traversal/

//
//class Solution {
//public:
//    vector<vector<int>> levelOrder(TreeNode* root) {
//        queue<TreeNode*> q;
//        int levelsize = 0;
//        if (root)
//        {
//            q.push(root);
//            levelsize = 1;
//        }
//        vector<vector<int>> vv;
//        while (!q.empty())
//        {
//            vector<int> v;
//            while (levelsize--)
//            {
//                TreeNode* front = q.front();
//                q.pop();
//                v.push_back(front->val);
//
//                if (front->left)
//                    q.push(front->left);
//
//                if (front->right)
//                    q.push(front->right);
//            }
//
//            vv.push_back(v);
//            levelsize = q.size();
//        }
//        return vv;
//    }
//};



//https://leetcode.cn/problems/binary-tree-level-order-traversal-ii/
//
//class Solution {
//public:
//    vector<vector<int>> levelOrderBottom(TreeNode* root) {
//
//        queue<TreeNode*> q;
//        int levelsize = 0;
//        if (root)
//        {
//            q.push(root);
//            levelsize = 1;
//        }
//        vector<vector<int>> vv;
//        while (!q.empty())
//        {
//            vector<int> v;
//            while (levelsize--)
//            {
//                TreeNode* front = q.front();
//                q.pop();
//                v.push_back(front->val);
//
//                if (front->left)
//                    q.push(front->left);
//
//                if (front->right)
//                    q.push(front->right);
//            }
//
//            vv.push_back(v);
//            levelsize = q.size();
//        }
//        reverse(vv.begin(), vv.end());
//        return vv;
//
//
//    }
//};



//https://leetcode.cn/problems/lowest-common-ancestor-of-a-binary-tree/submissions/
//
//class Solution {
//public:
//    bool IsInTree(TreeNode* root, TreeNode* x)
//    {
//        if (root == nullptr)
//            return false;
//
//
//        return root == x
//            || IsInTree(root->left, x)
//            || IsInTree(root->right, x);
//
//    }
//
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        if (root == nullptr)
//            return nullptr;
//
//        if (p == root || q == root)
//            return root;
//
//
//        bool PInleft = IsInTree(root->left, p);
//        bool PInRight = !PInleft;
//
//        bool QInleft = IsInTree(root->left, q);
//        bool QInRight = !QInleft;
//
//
//        if ((PInleft && QInRight) || (PInRight && QInleft))
//            return root;
//
//        else if (PInleft && QInleft)
//            return lowestCommonAncestor(root->left, p, q);
//
//        else
//            return lowestCommonAncestor(root->right, p, q);
//
//
//
//    }
//};
//
//
//class Solution {
//public:
//    bool Getpath(TreeNode* root, TreeNode* x, stack<TreeNode*>& path)
//    {
//        if (root == nullptr)
//            return false;
//
//        path.push(root);
//        if (root == x)
//            return true;
//
//        if (Getpath(root->left, x, path))
//            return true;
//
//        if (Getpath(root->right, x, path))
//            return true;
//
//        path.pop();
//        return false;
//    }
//
//    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
//        stack<TreeNode*> pPath;
//        stack<TreeNode*> qPath;
//
//        Getpath(root, p, pPath);
//        Getpath(root, q, qPath);
//
//        while (pPath.size() != qPath.size())
//        {
//            if (pPath.size() > qPath.size())
//                pPath.pop();
//            else
//                qPath.pop();
//        }
//
//        while (pPath.top() != qPath.top())
//        {
//            pPath.pop();
//            qPath.pop();
//        }
//
//        return pPath.top();
//    }
//};






//https://www.//nowcoder.com/practice/947f6eb80d944a84850b0538bf0ec3a5?tpId=13&&tqId=11179&rp=1&ru=/activity/oj&qru=/ta/coding-interviews/question-ranking
//
//class Solution {
//public:
//	void InOrder(TreeNode* cur, TreeNode*& prev)
//	{
//		if (cur == nullptr)
//			return;
//		InOrder(cur->left, prev);
//
//		cur->left = prev;
//		if (prev)
//			prev->right = cur;
//
//		prev = cur;
//		InOrder(cur->right, prev);
//	}
//	TreeNode* Convert(TreeNode* pRootOfTree) {
//		TreeNode* prev = nullptr;
//		InOrder(pRootOfTree, prev);
//
//		TreeNode* root = pRootOfTree;
//		while (root && root->left)
//			root = root->left;
//
//		return root;
//	}
//};



//https://leetcode.cn/problems/construct-binary-tree-from-preorder-and-inorder-traversal/
//
//class Solution {
//public:
//    TreeNode* _buildTree(vector<int>& preorder, vector<int>& inorder, int& prei, int inbegin, int inend)
//    {
//        if (inbegin > inend)
//            return nullptr;
//
//        TreeNode* root = new TreeNode(preorder[prei]);
//        int rooti = inbegin;
//        while (rooti <= inend)
//        {
//            if (inorder[rooti] != preorder[prei])
//            {
//                rooti++;
//            }
//            else
//            {
//                break;
//            }
//        }
//        ++prei;
//        root->left = _buildTree(preorder, inorder, prei, inbegin, rooti - 1);
//        root->right = _buildTree(preorder, inorder, prei, rooti + 1, inend);
//        return root;
//    }
//    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
//        int prei = 0;
//        return _buildTree(preorder, inorder, prei, 0, inorder.size() - 1);
//    }
//};




//https://leetcode.cn/problems/binary-tree-inorder-traversal/
//
//class Solution {
//public:
//    vector<int> inorderTraversal(TreeNode* root) {
//        stack<TreeNode*> st;
//        vector<int> v;
//        TreeNode* cur = root;
//
//        while (cur || !st.empty())
//        {
//            while (cur)
//            {
//                st.push(cur);
//                cur = cur->left;
//            }
//            TreeNode* top = st.top();
//            st.pop();
//            v.push_back(top->val);
//            cur = top->right;
//
//        }
//        return v;
//    }
//};


//
//https://leetcode.cn/problems/binary-tree-postorder-traversal/submissions/


class Solution {
public:
    vector<int> postorderTraversal(TreeNode* root) {
        vector<int> v;
        stack<TreeNode*> st;
        TreeNode* cur = root;
        TreeNode* prev = nullptr;
        while (cur || !st.empty())
        {
            while (cur)
            {
                st.push(cur);
                cur = cur->left;
            }
            TreeNode* top = st.top();
            if (top->right == nullptr || top->right == prev)
            {
                st.pop();
                v.push_back(top->val);
                prev = top;
            }
            else
            {
                cur = top->right;
            }
        }
        return v;
    }
};