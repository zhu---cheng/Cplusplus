#define _CRT_SECURE_NO_WARNINGS 1
#include<string>
#include<iostream>
using namespace std;
//
//class Solution {
//public:
//    string reverseStr(string s, int k) {
//        size_t prev = 0;
//        size_t begin = 0;
//        size_t end = 0;
//        while (end != s.size())
//        {
//            begin += 1;
//            end += 2;
//            if (end == 2 * k + 1)
//            {
//                reverse(s.begin() + prev, s.begin() + begin - 1);
//            }
//
//            prev = end;
//            begin = end;
//        }
//
//        reverse(s.begin() + prev, s.begin() + begin - 1);
//
//        return s;
//    }
//};
//
//int main()
//{
//    std::string str1, str2, str3;
//    str1 = "Test string: ";   // c-string
//    str2 = 'x';               // single character
//    str3 = str1 + str2;       // string
//
//    std::cout << str3 << '\n';
//    return 0;
//}

//class A
//{
//public:
//	void PrintA()
//	{
//		cout << _a << endl;
//	}
//private:
//	int _a;
//};
//int main()
//{
//	A* p = nullptr;
//	p->PrintA();
//	return 0;
//}



//出年分m和一年中的第n天，算出第n天是几月几号。
//输入描述：
//输入包括两个整数y(1 <= y <= 3000)，n(1 <= n <= 366)。
//输出描述：
//可能有多组测试数据，对于每组数据， 按 yyyy - mm - dd的格式将输入中对应的日期打印出来。
//#include <iostream>
//using namespace std;
//
//
//    int GetMonthDay(int year, int month)
//    {
//        int mon[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//        if (month == 2)
//        {
//            if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0))
//                mon[2] = 29;
//        }
//        return mon[month];
//    }
//
//    void Print(int year, int num)
//    {
//
//        int month = 1;
//        while (num - GetMonthDay(year, month) > 0)
//        {
//            num =num - GetMonthDay(year, month);
//            month++;
//        }
//
//        if (month < 10&&num<10)
//        {
//            cout << year << "-0" << month << "-0" << num << endl;
//
//        }
//        else if (month >=10 && num < 10)
//        {
//            cout << year << "-" << month << "-0" << num << endl;
//
//        }
//        else if (month < 10 && num >= 10)
//        {
//            cout << year << "-0" << month << "-" << num << endl;
//
//        }
//        else 
//        {
//            cout << year << "-" << month << "-" << num << endl;
//
//        }
//    }
//    int main()
//    {
//        int year;
//        int num;
//        cin >> year >> num;
//        Print(year, num);
//    }

//
//设计一个程序能计算一个日期加上若干天后是什么日期。
//输入描述：
//输入第一行表示样例个数m，接下来m行每行四个整数分别表示年月日和累加的天数。
//输出描述：
//输出m行，每行按yyyy - mm - dd的个数输出。


class Date
    
{
    friend ostream& operator<<(ostream& out, Date& d);
public:

    Date(int year, int month, int day)
        :_year(year),
        _month (month),
        _day(day)
    {}
    int GetMonthDay(int year, int month)
            {
                int mon[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
                if (month == 2)
                {
                    if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0))
                        mon[2] = 29;
                }
                return mon[month];
            }

    Date operator+(int num)
    {
        int year = _year;
        int month = _month;
        int day = _day;
        int days = GetMonthDay(year, month);
        while (day + num >days)
        {
            month++;
            if (month > 12)
            {
                year++;
                month = 1;
            }
            num = num - days;
            days = GetMonthDay(year, month);
        }
        day += num;

        return Date(year, month, day);
    }
    
private:
	int _year;
	int _month;
	int _day;
};
//
ostream& operator<<(ostream& out,Date& d)
{
if (d._month < 10 && d._day < 10)
            {
                return out << d._year << "-0" << d._month << "-0" << d._day << endl;
    
            }
            else if (d._month >=10 && d._day < 10)
            {
    return out << d._year << "-" << d._month << "-0" << d._day << endl;
    
            }
            else if (d._month < 10 && d._day >= 10)
            {
    return out << d._year << "-0" << d._month << "-" << d._day << endl;
    
            }
            else 
            {
    return out << d._year << "-" << d._month << "-" << d._day << endl;
    
            }

}

int main()
{

    int count = 0;
    int year = 0, month = 0, day = 0;
    int n = 0;
    cin >> count;
    for (int cur = 0; cur < count; cur++)
    {
        cin >> year >> month >> day >> n;
        Date d(year, month, day);
        Date d1 = d + n;
        cout << d1 << endl;
    }
}