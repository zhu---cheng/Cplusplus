#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
// 包装器 -- 可调用对象类型进行再封装适配
// 函数指针
// 仿函数
// lambda

#include<map>
#include<functional>

int f(int a, int b)
{
	cout << "int f(int a, int b)" << endl;
	return a + b;
}

struct Functor
{
public:
	int operator() (int a, int b)
	{
		cout << "int operator() (int a, int b)" << endl;

		return a + b;
	}
};

class Plus
{
public:
	Plus(int rate = 2)
		:_rate(rate)
	{}

	static int plusi(int a, int b)
	{
		return a + b;
	}

	double plusd(double a, double b)
	{
		return (a + b) * _rate;
	}

private:
	int _rate = 2;
};
//
//int main()
//{
//	//function<int(int, int)> f1 = &Plus::plusi;
//	function<int(int, int)> f1 = Plus::plusi;
//
//	//function<double(Plus*, double, double)> f2 = &Plus::plusd;
//
//	//cout << f1(1, 2) << endl;
//	////cout << f2(&Plus(), 20, 20) << endl;
//
//	//Plus pl(3);
//	//cout << f2(&pl, 20, 20) << endl;
//
//	function<double(Plus, double, double)> f2 = &Plus::plusd;
//
//	cout << f1(1, 2) << endl;
//	cout << f2(Plus(), 20, 20) << endl;
//
//	Plus pl(3);
//	cout << f2(pl, 20, 20) << endl;
//
//	return 0;
//}

//int main()
//{
//	//int(*pf1)(int,int) = f;
//	//map<string, >
//
//	function<int(int, int)> f1 = f;
//	function<int(int, int)> f2 = Functor();
//	function<int(int, int)> f3 = [](int a, int b) {
//		cout << "[](int a, int b) {return a + b;}" << endl;
//		return a + b;
//	};
//
//	cout << f1(1, 2) << endl;
//	cout << f2(10, 20) << endl;
//	cout << f3(100, 200) << endl;
//
//	map<string, function<int(int, int)>> opFuncMap;
//	opFuncMap["函数指针"] = f;
//	opFuncMap["仿函数"] = Functor();
//	opFuncMap["lambda"] = [](int a, int b) {
//		cout << "[](int a, int b) {return a + b;}" << endl;
//		return a + b;
//	};
//	cout << opFuncMap["lambda"](1, 2) << endl;
//
//
//
//	return 0;
//}
//using namespace placeholders;
//
//void Print(int x, int y)
//{
//	cout << x << " " << y << endl;
//}
//
//
//int main()
//{
//	function<void(int, int)> f1= Print;
//	f1(1, 2);
//
//	//bind绑定
//	auto f2 = bind(Print, _2, _1);
//	f2(1, 2);
//
//
//	auto f3 = bind(Print, 3, _1);
//	f3(5);
//}



int Div(int a, int b)
{
	return a / b;
}

int Plus(int a, int b)
{
	return a + b;
}

int Mul(int a, int b, double rate)
{
	return a * b * rate;
}

class Sub
{
public:
	int sub(int a, int b)
	{
		return a - b;
	}
};

using namespace placeholders;

// 11:50继续
int main()
{
	// 调整个数, 绑定死固定参数
	function<int(int, int)> funcPlus = Plus;
	//function<int(Sub, int, int)> funcSub = &Sub::sub;
	function<int(int, int)> funcSub = bind(&Sub::sub, Sub(), _1, _2);
	function<int(int, int)> funcMul = bind(Mul, _1, _2, 1.5);
	map<string, function<int(int, int)>> opFuncMap =
	{
		{ "+", Plus},
		{ "-", bind(&Sub::sub, Sub(), _1, _2)}
	};

	cout << funcPlus(1, 2) << endl;
	cout << funcSub(1, 2) << endl;
	cout << funcMul(2, 2) << endl;

	cout << opFuncMap["+"](1, 2) << endl;
	cout << opFuncMap["-"](1, 2) << endl;



	int x = 2, y = 10;
	cout << Div(x, y) << endl;

	// 调整顺序 -- 鸡肋
	// _1 _2.... 定义在placeholders命名空间中，代表绑定函数对象的形参，
	// _1，_2...分别代表第一个形参、第二个形参...
	//bind(Div, placeholders::_1, placeholders::_2);
	//auto bindFunc1 = bind(Div, _1, _2);
	//function<int(int, int)> bindFunc2 = bind(Div, _2, _1);
	//cout << bindFunc1(x, y) << endl;
	//cout << bindFunc2(x, y) << endl;



	return 0;
}