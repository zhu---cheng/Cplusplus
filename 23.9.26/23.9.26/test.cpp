#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<memory>
using namespace std;

//
//	template<class T>
//	class smart_ptr
//	{
//	public:
//		smart_ptr(T* ptr)
//			:_ptr(ptr)
//		{}
//
//		~smart_ptr()
//		{
//			if (_ptr)
//			{
//				cout << "delete:" << _ptr << endl;
//				delete _ptr;
//			}
//		}
//
//
//
//		T& operator*()
//		{
//			return *_ptr;
//		}
//
//		T& operator->()
//		{
//			return _ptr;
//		}
//	private:
//		T* _ptr;
//	};
//
//
//
//
//
//int div()
//{
//	int a, b;
//	cin >> a >> b;
//	if (b == 0)
//		throw invalid_argument("除0错误");
//
//	return a / b;
//}
////
////void Func()
////{
////	// 1、如果p1这里new 抛异常会如何？
////	// 2、如果p2这里new 抛异常会如何？
////	// 3、如果div调用这里又会抛异常会如何？
////	int* p1 = new int;
////	int* p2 = nullptr;
////	try
////	{
////		p2 = new int;
////	}
////	catch (...)
////	{
////		delete p1;
////		throw;
////	}
////
////	try
////	{
////		cout << div() << endl;
////	}
////	catch (...)
////	{
////		delete p1;
////		delete p2;
////
////		throw;
////	}
////
////	delete p1;
////	delete p2;
////}
//
////
////void Func()
////{
////
////	smart_ptr<int> sp1(new int(1));
////	smart_ptr<int> sp2(new int(2));
////	cout << div() << endl;
////	*sp1 = 10;
////	cout << *sp1 << endl;
////	cout << *sp2 << endl;
////}
////
////
////
////int main()
////{
////	try
////	{
////		Func();
////	}
////	catch (exception& e)
////	{
////		cout << e.what() << endl;
////	}
////
////	return 0;
////}
//
//
//int main()
//{
//	smart_ptr<int> sp1(new int(2));
//	smart_ptr<int> sp2(sp1);
//	//编译器发生了浅拷贝，结束时调用了两次析构函数，同一块空间多次释放，所以报错
//	//但是我们也没办法写深拷贝，因为指针的特性就要求了是浅拷贝，两个指针指向同一块空间
//	//所以我们想办法写出拷贝构造
//
//	return 0;
//}
//


//////////////////////////////////////////////////////////////////////
//#include"SmartPtr.h"

//定制删除器
template<class T>
class DeleteArray
{
	void operator()(T* ptr)
	{
		cout << "void operator()(T* ptr)" << endl;
		delete[] ptr;
	}
};
struct Date
{
	int _year = 0;
	int _month = 0;
	int _day = 0;

	~Date()
	{}
};


void test_shared_deletor()
{
	std::shared_ptr<Date> spa1(new Date[10], DeleteArray<Date>());
	std::shared_ptr<Date> spa2(new Date[10], [](Date* ptr){
		cout << "lambda delete[]"<<ptr << endl;
		delete[] ptr; 
	});

	std::shared_ptr<FILE> spF3(fopen("Test.cpp", "r"), [](FILE* ptr){
		cout << "lambda fclose" << ptr << endl;
		fclose(ptr);
	});
}

int main()
{
	//zc::test_auto();
	//zc::test_shared();


	//zc::test_shared_cycle();
	//zc::test_shared_deletor();

	test_shared_deletor();

}



//
//#include<iostream>
//#include<thread>
//
//using namespace std;
////写一个仿函数
//template<class T>
//struct DeleteArray
//{
//	void operator()(T* ptr)
//	{
//		cout << "delete[]" << ptr << endl;
//
//		delete[] ptr;
//	}
//};
//
//template<class T>
//struct Free
//{
//	void operator()(T* ptr)
//	{
//		cout << "free" << ptr << endl;
//
//		free(ptr);
//	}
//};
//
//struct Fclose
//{
//	void operator()(FILE* ptr)
//	{
//		cout << "fclose" << ptr << endl;
//
//		fclose(ptr);
//	}
//};
//class Date
//{
//	int _year;
//	int _month;
//	int _day;
//
//
//public:
//	~Date()
//	{
//		cout << "~Date()" << endl;
//	}
//};
//int main()
//{
//	//std版本：
//	//传类型  
//	std::unique_ptr<Date> up1(new Date);
//
//	std::unique_ptr<Date, DeleteArray<Date>> up2(new Date[10]);
//
//	std::unique_ptr<Date, Free<Date>> up3((Date*)malloc(sizeof(Date) * 10));
//
//	std::unique_ptr<FILE, Fclose> up4((FILE*)fopen("text.cpp", "r"));
//
//	return 0;
//}
