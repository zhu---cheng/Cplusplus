#pragma once

#include<iostream>
#include<thread>
#include<time.h>
#include<memory>
namespace zc
{
	template<class T>
	class auto_ptr
	{
	public:
		auto_ptr(T* ptr)
			:
			_ptr(ptr)
		{}

		~auto_ptr()
		{
			cout << "delete:" << _ptr << endl;
			delete _ptr;
		}

		//管理权转移
		auto_ptr(auto_ptr<T>& ap)
			:_ptr(ap._ptr)
		{
			ap._ptr = nullptr;
		}
		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}
	private:
		T* _ptr;
	};



	void test_auto()
	{
		auto_ptr<int> ap1(new int(1));
		auto_ptr<int> ap2(ap1);



		*ap1 = 10;//管理权转移导致了ap1悬空，无法访问
		*ap2 = 20;

	}



	//另一种思路，我们可以防止拷贝

	template<class T>
	class unique_ptr
	{
	public:
		unique_ptr(T* ptr)
			:_ptr(ptr)
		{}

		~unique_ptr()
		{
			cout << "delelte:"<<_ptr<<endl;
		}

		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		//C++11思路：
		unique_ptr(const unique_ptr<T>& ap) = delete;
		unique_ptr<T>& operaotr = (const unique_ptr<T>&ap) = delete;

		// 防拷贝
		// 拷贝构造和赋值是默认成员函数，我们不写会自动生成，所以我们不需写
		// C++98思路：只声明不实现，但是用的人可能会在外面强行定义，所以再加一条，声明为私有

	//private:
	//	unique_ptr(const unique_ptr<T>& ap);
	//	unique_ptr<T>& operaotr = (const unique_ptr<T>&ap);
		
	private:
		T* _ptr;
	};

	//void test_unique()
	//{
	//	unique_ptr<int> ap1(new int(1));
	//	unique_ptr<int> ap2(ap1);//无法拷贝
	//}



	//C++11思路，引用计数
	template<class T>
	class shared_ptr
	{
	public:
		shared_ptr(T* ptr=nullptr)
			:_ptr(ptr),
			_pcount(new int(1))
		{}


		template<class D>
		shared_ptr(T* ptr,D del)
			: _ptr(ptr),
			_pcount(new int(1)),
			_del(del)
		{}

		~shared_ptr()
		{
			if (--(*_pcount) == 0)
			{

				_del(_ptr);
				/*cout << "delete:" <<_ptr<<endl;
				delete _ptr;
				delete _pcount;*/
			}
		}

		shared_ptr(const shared_ptr<T>& sp)
			:_ptr(sp._ptr),
			_pcount(sp._pcount)
		{
			++(*_pcount);
		}

		shared_ptr<T>& operator=(const shared_ptr<T>& sp)
		{
			if (_ptr != sp._ptr)
			{
				if (--(*_pcount) == 0)
				{
					cout << "delete:" << _ptr << endl;
					delete _ptr;
					delete _pcount;
				}

				_ptr = sp._ptr;
				_pcount = sp._pcount;
				++(*_pcount);
			}

			return *this;
		}

		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		int use_count()
		{
			return *_pcount;
		}

		T* get() const
		{
			return _ptr;
		}
	private:
		T* _ptr;
		int* _pcount;
		//包装器
		function<void(T*)> _del = [](T* ptr) {
			cout << "lambda delete:" << ptr << endl;
			delete ptr;
		};
	};

	

	template<class T>
	class weak_ptr
	{
	public:
		weak_ptr()
			:_ptr(nullptr)
		{}

		weak_ptr(const shared_ptr<T>& sp)
			:_ptr(sp.get())
		{}

		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		T* get()
		{
			return _ptr;
		}

	private:
		T* _ptr;
	};
	// weak_ptr
	// 1、他不是常规的智能指针，不支持RAII
	// 2、支持像指针一样
	// 3、专门设计出来，辅助解决shared_ptr的循环引用问题
	//    weak_ptr可以指向资源，但是他不参与管理，不增加引用计数

	//void test_shared()
	//{
	//	/*	shared_ptr<int> sp1(new int(1));
	//		shared_ptr<int> sp2(sp1);*/

	//	shared_ptr<int> sp1(new int(1));
	//	shared_ptr<int> sp2(sp1);
	//	shared_ptr<int> sp3(sp2);

	//	shared_ptr<int> sp4(new int(10));

	//	//sp1 = sp4;
	//	sp4 = sp1;

	//	sp1 = sp1;
	//	sp1 = sp2;

	//}


	//struct ListNode
	//{
	//	shared_ptr<ListNode> _next;
	//	shared_ptr<ListNode> _prev;


	//	int _val;

	//	~ListNode()
	//	{
	//		cout << "~ListNode" << endl;
	//	}
	//};

	//void test_shared_cycle()
	//{
	//	ListNode* n1 = new ListNode;
	//	ListNode* n2 = new ListNode;

	//}

	// 循环引用
	struct ListNode
	{
		/*ListNode* _next;
		ListNode* _prev;*/

	/*	shared_ptr<ListNode> _next;
		shared_ptr<ListNode> _prev;*/

		weak_ptr<ListNode> _next;
		weak_ptr<ListNode> _prev;
		int _val;


		~ListNode()
		{
			cout << "~ListNode()" << endl;
		}
	};

	// 循环引用
	void test_shared_cycle()
	{
	/*	ListNode* n1 = new ListNode;
		ListNode* n2 = new ListNode;

		n1->_next = n2;
		n2->_prev = n1;

		delete n1;
		delete n2;*/

		shared_ptr<ListNode> n1(new ListNode);
		shared_ptr<ListNode> n2(new ListNode);

		cout << n1.use_count() << endl;
		cout << n2.use_count() << endl;

		n1->_next = n2;
		n2->_prev = n1;

		cout << n1.use_count() << endl;
		cout << n2.use_count() << endl;
	}


//定制删除器
	template<class T>
	class DeleteArray
	{
		void operator()(T* ptr)
		{
			cout << "void operator()(T* ptr)" << endl;
			delete[] ptr;
		}
	};
	struct Date
	{
		int _year = 0;
		int _month = 0;
		int _day = 0;

		~Date()
		{}
	};


	void test_shared_deletor()
	{
		//shared_ptr<int> i1(new int(1));

		shared_ptr<Date> d1(new Date);

		shared_ptr<Date> d2(new Date[10], DeleteArray<Date>());
	}
}