#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//
//class Date
//{
//	/*1. 每个成员变量在初始化列表中只能出现一次(初始化只能初始化一次)
//	  2. 类中包含以下成员，必须放在初始化列表位置进行初始化：
//		引用成员变量
//		const成员变量
//		自定义类型成员(且该类没有默认构造函数时)*/
//public:
//	Date(int year, int month, int day)
//	:_year(year)
//	,_month(month)
//	,_day(day)
//	{}
//
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};


//
//class Time
//{
//public:
//	Time(int hour = 0)
//		:_hour(hour)
//	{
//		cout << "Time()" << endl;
//	}
//private:
//
//		int _hour;
//};
//
//class Date
//{
//public:
//	Date(int day)
//	{}
//private:
//	int _day;
//	Time _t;
//};
////尽量使用初始化列表初始化，因为不管你是否使用初始化列表，对于自定义类型成员变量，
////一定会先使用初始化列表初始化。
//int main()
//{
//	Date d(1);
//}




//class A
//{
//public:
//	A(int a)
//		:_a1(a)
//		, _a2(_a1)
//	{}
//
//	void Print() {
//		cout << _a1 << " " << _a2 << endl;
//	}
//private:
//	int _a2;
//	int _a1;
//};
//
//
//int main() {
//	A aa(1);
//	aa.Print();
//}
//答案是什么？
//1和随机值
//成员变量在类中声明次序就是其在初始化列表中的初始化顺序，与其在初始化列表中的先后
//次序无关


//
//class A
//{
//public:
//	explicit A(int a)
//		:_a1(a)
//	{
//		cout << "A(int a)" << endl;
//	}
//
//	//explicit A(int a1, int a2)
//	 A(int a1, int a2)
//		:_a1(a1)
//		, _a2(a2)
//	{
//		cout << "A(int a1,int a2)" << endl;
//
//	}
//
//	A(const A& aa)
//		:_a1(aa._a1)
//	{
//		cout << "A(const A& aa)" << endl;
//	}
//
//private:
//	int _a2;
//	int _a1;
//};
//
//int main()
//{ 
//	// 单参数构造函数 C++98
//	A aa1(1);  // 构造函数
//	//A aa2 = 1; // 隐式类型转换   构造+拷贝+优化->构造
//	//const A& ref = 10;
//
//
//	// 多参数构造函数 C++11
//	A aa2(1, 1);
//	A aa3 = { 2, 2 };
//	const A &ref = { 2, 2 };
//
//	int i = 1;
//	double d = i; // 隐式类型转换
//
//	return 0; 
//}



//实现一个类，计算程序中创建中了多少个类对象
//class A
//{
//public:
//	A(int a = 0)
//	{
//		count++;
//	}
//
//	A(const A& a)
//	{
//		count++;
//	}
//		
//	//静态成员函数，为静态变量而生，没有this指针
//	static int GetCount()
//	{
//		//_a++;不能直接访问类对象
//		return count;
//	}
//private:
//	static int count;// 不属于某个对象，所于所有对象，属于整个类
//	int _a=0;
//};
// 
//int A::count = 0;//定义初始化
//
//
//void func()
//{
//
//	A a1(1);
//	A a2(a1);
//	A a3 = 3;
//
//	cout << A::GetCount() << endl;
//	//cout << a1.count << endl;//错误
//	//A* ptr = nullptr;
//	//cout << ptr->GetCount() << endl;//可以
//
//	//A* ptr = nullptr;
//	//cout << ptr->count << endl;//错误
//
//
//}
//int main()
//{
//	func();
//	return 0;
//}



//不用常规方法累加
//class Sum
//{
//
//public:
//	Sum()
//	{
//		_sum += _i;
//		_i++;
//	}
//
//	static int GetSum()
//	{
//		return _sum;
//	}
//private:
//	static int _i;
//	static int _sum;
//};
//
//int Sum::_i = 1;
//int Sum::_sum = 0;
//class Solution
//{
//public:
//	int Sum_Solution(int n)
//	{
//		Sum* ptr = new Sum[n];
//
//		return Sum::GetSum();
//		
//	}
//
//
//	~Solution()
//	{
//		cout << "~Solution()" << endl;
//    }
//
//};
//int main()
//{
//	Solution s;
//	//cout << s.Sum_Solution(10) << endl;
//
//	//cout << Solution().Sum_Solution(10) << endl;//匿名对象
//
//	//匿名对象生命周期只在这一行
//
//	
//
//	return 0;
//}



//
//class A
//{
//
//private:
//	static int k;
//	int h = 1;
//public:
//		// 内部类 -- 跟A是独立，只是受A的类域限制
//	// B天生就是A的友元
//	class B
//	{
//		void foo(const A& a)
//		{
//			cout << k << endl;
//			cout << a.h << endl;//B可以访问A
//		}
//	private:
//		int b = 2;
//	};
//
//};
//
//int  A::k = 0;
//
//int main()
//{
//	A aa;
//	A::B bb;
//
//	
//	cout << sizeof(aa) << endl;
//	cout << sizeof(bb) << endl;
//
//}




class A
{
public:
	A(int a = 0)
		:
		_a(a)
	{
		cout << "A(int a = 0)" << endl;
	}

	A& operator=(const A& aa)
	{
		cout << "A& operator=(const A& aa)" << endl;
		if (this != &aa)
		{
			_a = aa._a;
		}

		return *this;
	}


	~A()
	{
		cout << "~A()" << endl;
	}

private:
	int _a;
};
void func1(A a)
{
	
}

void func2(const A& a)
{

}
//int main()
//{
//	/*A a1 = 1;*/
//	A a1=1 ;//构造+拷贝构造-->直接构造
//
//	func1(a1);//无优化
//	
//	func1(2);//构造+拷贝构造-->直接构造
//
//	func1(A(3));//构造+拷贝构造-->直接构造
//
//	cout << "-----------" << endl;
//
//	func2(a1);
//	func2(2);
//	func2(A(3));
//
//
//}
//
//A func3()
//{
//	A aa;
//	return aa; 
//}
//
//A func4()
//{
//	return A();
//}
//
// //深度探索C++对象模型
//
//int main()
//{
//	func3();
//
//	A aa1 = func3(); // 拷贝构造+拷贝构造  -- 优化为一个拷贝构造
//
//	cout << "****" << endl;
//	A aa2;
//	aa2 = func3();  // 不能优化
//
//	cout << "---------------------------" << endl;
//	func4(); // 构造+拷贝构造 -- 优化为构造
//	A aa3 = func4(); // 构造+拷贝构造+拷贝构造  -- 优化为构造
//
//	return 0;
//}


//总结：返回对象时尽量用拷贝构造接收，不要赋值接收
//返回对象尽可能用匿名对象

//尽量使用const&传参

