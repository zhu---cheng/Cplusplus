#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//
//int main()
//{
//	string s1 = "hello world";
//	string::iterator it = s1.begin();//迭代器写法
//	while (it != s1.end())
//	{
//		cout << *it;
//		++it;
//	}
//	cout << endl;
//
//
//
//	for (auto ch : s1)//循环，语法糖
//	{
//		cout << ch << " ";
//	}
//}

void Func(const string& s)
{
	// 遍历和读容器的数据，不能写
	string::const_iterator it = s.begin();
	while (it != s.end())
	{
		//*it += 1;
		cout << *it << " ";
		++it;
	}
	cout << endl;

	//string::const_reverse_iterator rit = s.rbegin();
	auto rit = s.rbegin();
	while (rit != s.rend())
	{
		cout << *rit << " ";
		++rit;
	}
	cout << endl;
}

//
//int main()
//{
//	string s1("hello world");
//	string::iterator it = s1.begin();
//	while (it != s1.end())
//	{
//		cout << *it;
//		++it;
//	}
//
//	cout << endl;
//
//
//	//反向历遍
//	string::reverse_iterator rit = s1.rbegin();
//	while (rit != s1.rend())
//	{
//		cout << *rit;
//		++rit;//这里是++！！！
//	}
//	cout << endl;
//
//
//	Func(s1);
//
//		try
//	{
//		s1[100];
//		//s1.at(100);
//	}
//	catch (const exception& e)
//	{
//		cout << e.what() << endl;
//	}//抛异常
//}



//insert erase
//效率低，少用
//int main()
//{
//	string s1("hello");
//	s1.insert(5, "world");
//	cout << s1 << endl;
//
//	//s1.insert(5, ' ');//无法编译通过，因为insert没有对应的函数参数形式,下面两种形式是可以的
//
//
//	s1.insert(5, " ");
//	s1.insert(5, 1, ' ');
//	cout << s1 << endl;
//
//
//	string s2("hello world");
//	s2.erase(5, 1);
//	cout << s2 << endl;
//	
//	s2.erase(s2.begin() + 5);//删除第五个位置的一个字符
//	cout << s2 << endl;
//
//	
//
//    s2.erase(5);//位置后面的全删
//
//}



//把' '替换成%20
//replace的使用
//多次使用replace会多次开空间，效率低下，所以我们可以一次性开完
//int main()
//{
//	string s1("hello world i love you");
//	size_t num = 0;
//	
//	for (auto ch : s1)
//	{
//		if (ch == ' ')
//			num++;
//	}
//	s1.reserve(s1.size() + 2 * num);//一次性开辟好提高效率
//
//	size_t pos = s1.find(' ');
//	while (pos != string::npos)
//	{
//		s1.replace(pos, 1, "%20");
//		pos = s1.find(' ', pos + 3);
//	}
//
//	cout << s1 << endl;
//}


////空间换时间：
//int main()
//{
//	string s1("hello world i love you");
//	string newStr;
//	size_t num=0;
//	for (auto ch : s1)
//	{
//		if (ch == ' ')
//			num++;
//	}
//	newStr.reserve(s1.size() + num * 2);
//	for (auto ch : s1)
//	{
//		if (ch != ' ')
//			newStr += ch;
//		else
//			newStr += "%20";
//	}
//
//	cout << newStr << endl;
//
//
//	string s2("XXXXXXXXXXXX");
//	swap(s1, s2);
//	cout << s1 << endl;
//	cout << s2 << endl;
//
//	s1.swap(s2);
//	cout << s1 << endl;
//	cout << s2 << endl;//这种是string库中的，交换指针指向的首地址，而第一种是交换内容
//}



//int main()
//{
//	string s1("hello world");
//	cout << s1 << endl;
//	cout << s1.c_str() << endl;//s1.c_str首元素地址，但流输出会默认输出字符串
//	cout << (void*)s1.c_str() << endl;//加上void*表示是指针
//
//	cout << s1 << endl;
//	cout << s1.c_str() << endl;
//	s1 += '\0';
//	s1 += '\0';
//	s1 += "xxxxx";
//	cout << s1 << endl;//开空间，默认打印这么大空间的内容
//	cout << s1.c_str() << endl;//从首地址打印遇到'\0'会停止
//
//	string filename("test.cpp");
//	FILE* fout = fopen(filename.c_str(), "r");//这里只能给地址
//	if (fout == nullptr)
//		perror("fopen fail");
//
//	char ch = fgetc(fout);
//	while (ch != EOF)
//	{
//		cout << ch;
//		ch = fgetc(fout);
//	}
//
//	fclose(fout);
//
//	return 0;
//}


//
//int main()
//{
//	//string s1("string.txt.tar.zip.cpp");
//	//size_t pos = s1.rfind('.');
//	//if(pos != string::npos)
//	//{
//	//	string suffix = s1.substr(pos);//子串
//	//	cout << suffix << endl;
//	//}
//
//
//	string url("http://www.cplusplus.com/reference/string/string/find/");
//	cout << url << endl;
//	size_t pos=url.find("://");
//
//
//	size_t start = pos + 3;
//	size_t end = url.find( '/',start);
//	string strstr = url.substr(start, end-start);
//
//	cout << strstr << endl;
//}


//
//int main()
//{
//	std::string str("Please, replace the vowels in this sentence by asterisks.");
//	std::size_t found = str.find_first_of("abcdv");//first理解为any，找子集
//	while (found != std::string::npos)
//	{
//		str[found] = '*';
//		found = str.find_first_of("abcdv", found + 1);
//	}
//
//	std::cout << str << '\n';
//
//	string s1("hello world");
//	string s2("hello world");
//	s1 == s2;
//	s1 == "hello world";
//	"hello world" == s1;
//
//	return 0;
//}


//计算字符串最后一个单词的长度，单词以空格隔开，字符串长度小于5000。（注：字符串末尾不以空格为结尾）
#include <iostream>
#include<string>
using namespace std;

int main() {
	//cin >> i >> j;
	string str;
	//cin >> str;遇到空格会停止
	getline(cin, str);
	size_t pos = str.rfind(' ');
	if (pos != string::npos)
	{
		cout << str.size() - pos - 1 << endl;
	}
	else
	{
		cout << str.size() << endl;
	}

	return 0;
}