#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
//
//class Person
//{
//public:
//	void Print()
//	{
//		cout << "_name is " << _name << endl;
//		cout << "_age is " << _age << endl;
//	}
//protected:
//	string _name = "peter";
//	int _age = 18;
//};
//
//
//class Student :public Person
//{
//public:
//	void func()
//	{
//		cout << _name << endl;
//	}
//protected:
//	int _stuid; // 学号
//};
//
//class Teacher : public Person
//{
//protected:
//	int _jobid; // 工号
//};
//
//
//int main()
//{/*
//	Student s;
//	s.func();
//	s.Print();*/
//
//	////基类和派生类对象赋值转换
//	//Person j;
//	//j = s;//子类可以赋值给父类，父类不可以赋子类，“切片”
//	////s = j;
//
//
//		double d = 1.1;
//	int i = d;  // 隐式类型转换,有一个隐式临时变量，具有常性
//	const int& ri = d;
//
//	//子类赋给父类没有隐式类型转换，就是自然的赋值
//	Student s;
//	Person p = s;
//	Person& rp = s;
//	Person* ptrp = &s;
//
//}



//子类和父类中有同名成员，子类成员将屏蔽父类对同名成员的直接访问，这种情况叫隐藏，
//也叫重定义。（在子类成员函数中，可以使用 基类::基类成员 显示访问）
//3. 需要注意的是如果是成员函数的隐藏，只需要函数名相同就构成隐藏。
//4. 注意在实际中在继承体系里面最好不要定义同名的成员。
//
//class Person
//{
//protected:
//	string _name = "小李子"; // 姓名
//	int _num = 111; 		// 身份证号
//};
//
//class Student : public Person
//{
//public:
//	void Print()
//	{
//		cout << Person::_num << endl;
//		cout << _num << endl;
//	}
//
//protected:
//	// 子类和父类中有同名成员，子类成员将屏蔽父类对同名成员的直接访问，这种情况叫隐藏，也叫重定义。
//	int _num = 999; // 学号
//};
//class A
//{
//public:
//	void fun()
//	{
//		cout << "func()" << endl;
//	}
//};
//class B : public A
//{
//public:
//	void fun(int i)
//	{
//		cout << "func(int i)->" << i << endl;
//	}
//};
////B
//// A：两个fun构成函数重载
//// B：两个fun构成隐藏
//// C：编译报错
//// D：以上说法都不对
//
//void Test()
//{
//	//B b;
//	//b.fun(10);
//	B b;
//	b.fun(10);
//	b.A::fun();
//};
//
//int main()
//{
//	Student s;
//	s.Print();
//
//
//	Test();
//	return 0;
//}
//



//
//class Person
//{
//public:
//	Person(const char* name = "peter")
//		: _name(name)
//	{
//		cout << "Person()" << endl;
//	}
//
//	Person(const Person& p)
//		: _name(p._name)
//	{
//		cout << "Person(const Person& p)" << endl;
//	}
//
//	Person& operator=(const Person& p)
//	{
//		cout << "Person operator=(const Person& p)" << endl;
//		if (this != &p)
//			_name = p._name;
//
//		return *this;
//	}
//
//	~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//protected:
//	string _name; // 姓名
//};
//
//class Student : public Person
//{
//public:
//	Student(const char* name, int num)
//		:Person(name)
//		,_num(num)
//	{
//		cout << "Student()" << endl;
//	}
//
//	Student(const Student& s)
//		:Person(s)
//		, _num(s._num)
//	{
//		cout << "Student(const Student& s)" << endl;
//
//	}
//
//	Student& operator=(const Student& s)
//	{
//		if (this != &s)
//		{
//			Person::operator=(s);
//			_num = s._num;
//		}
//		cout << "Student& operator=(const Student& s)" << endl;
//
//
//		return *this;
//	}
//
//	// 析构函数会被处理成destructor
//	~Student()
//	{
//		//Person::~Person();
//
//		cout << "~Student()" << endl;
//	}
//	// 子类析构函数完成时，会自定调用父类析构函数，保证先析构子再析构父
//protected:
//	int _num; //学号
//};
//
//
//int main()
//{
//	Student s1("张三", 18);
//	Student s2(s1);
//
//	Person p = s1;
//
//	s1 = s2;
//
//
//	return 0;
//}

//
//class Student;
//class Person
//{
//public:
//	friend void Display(const Person& p, const Student& s);
//protected:
//	string _name; // 姓名
//};
//class Student : public Person
//{
//	friend void Display(const Person& p, const Student& s);
//protected:
//	int _stuNum; // 学号
//};
//
////void Display(const Person& p, const Student& s)
////{
////	cout << p._name << endl;//友元关系不能继承，也就是说基类友元不能访问子类私有和保护成员
////	cout << s._stuNum << endl;
////}
//
//void main()
//{
//	Person p;
//	Student s;
//	//Display(p, s);
//}


//
////基类定义了static静态成员，则整个继承体系里面只有一个这样的成员。无论派生出多少个子
////类，都只有一个static成员实例
//class Person
//{
//public:
//	Person() { ++_count; }
//protected:
//	string _name; // 姓名
//public:
//	static int _count; // 统计人的个数。
//};
//int Person::_count = 0;
//
//class Student : public Person
//{
//protected:
//	int _stuNum; // 学号
//};
//
//class Graduate : public Student
//{
//protected:
//	string _seminarCourse; // 研究科目
//};
//
//int main()
//{
//	Person p;
//	Student s;
//	/*cout << &(p._name) << endl;
//	cout << &(s._name) << endl;*/
//
//	cout << &(p._count) << endl;
//	cout << &(s._count) << endl;//无论派生出多少个子
////类，都只有一个static成员
//
//	Graduate g1;
//	Graduate g2;
//
//	cout << Person::_count << endl;
//	cout << Graduate::_count << endl;
//
//	return 0;
//}


// //实现一个不能被继承的类
//class A
//{
//public:
//	static A CreateObj()
//	{
//		return A();
//	}
//private:
//	A()
//	{}
//};
//
//class B : public A
//{};
//
//int main()
//{
//	A::CreateObj();//A不能被继承，但是如果想用A的构造函数，可以用static这种方法调用
//
//	//B bb;
//
//	return 0;
//}




//复杂的菱形继承及菱形虚拟继承
class Person
{
public:
	string _name; // 姓名
	//int _age;
	//int _tel;
	//int _address;
};
class Student : virtual public Person
{
protected:
	int _num; //学号
};
class Teacher : virtual public Person
{
protected:
	int _id; // 职工编号
};
class Assistant : public Student, public Teacher
{
protected:
	string _majorCourse; // 主修课程
};

void main()
{
	// 这样会有二义性无法明确知道访问的是哪一个
	Assistant a;
	a._name = "张三";

	// 数据冗余 和 二义性

	//虚拟继承可以解决菱形继承的二义性和数据冗余的问题。如上面的继承关系，在Student和
	//Teacher的继承Person时使用虚拟继承，即可解决问题。需要注意的是，虚拟继承不要在其他地
	//	方去使用。
	a.Student::_name = "小张";
	a.Teacher::_name = "老张";
}












///*
//牛客---手套问题
//要想能够让左右手套至少有一副配对
//我们可以先把左手手套每种都至少拿一只，然后再随便拿一只右手手套就可以成功配对
//问题就是如何保证每种手套都能拿一种。
//以【3 7 2 3 5】为例
//拿5种？肯定不行的，有可能都是同一个颜色
//拿7种？不行，有可能都是同一个颜色
//8？也不行
//。。。
//全部拿走？不合适了
//全部拿走再减去最少的，这样有可能就缺了最少的一种颜色，因此再+1
//这个手套数量最少不能是0，如果是0会产生10个手套，取11次的bug
//
//如果某个颜色没有手套，就必须得把该颜色对应的另一边手套累加起来。
//
//先计算出左手和右手手套的总数，然后减去各自的最少的数再加一
//这样就可以保证取出的手套至少每种都有一只
//计算总数的时候，要找出手套数量最少的那个颜色
//比较两者较小的那个数，决定先取左手还是先取右手
//最后再加上另一种手套的一只就行
//*/
//
//    int findMinimum(int n, vector<int> left, vector<int> right) {
//        int leftSum = 0, leftMin = INT_MAX;
//        int rightSum = 0, rightMin = INT_MAX;
//        int ret = 0;
//        for (int i = 0; i < n; ++i)
//        {
//            if (left[i] == 0 || right[i] == 0)
//            {
//                ret += left[i] + right[i];
//            }
//            else
//            {
//                // 更新数量最少的手套
//                leftMin = min(leftMin, left[i]);
//                rightMin = min(rightMin, right[i]);
//                leftSum += left[i];
//                rightSum += right[i];
//            }
//        }
//        // 遍历结束，判断左边的最少还是右边最少 加入左手最少的，就再从右手随便取一只
//        ret += min(leftSum - leftMin + 1, rightSum - rightMin + 1) + 1;
//        return ret;
//    }
//
//
//int main()
//{
//    vector<int> left;
//    left.push_back(0);
//    left.push_back(7);
//    left.push_back(1);
//    left.push_back(6);
//    vector<int> right;
//    right.push_back(1);
//    right.push_back(5);
//    right.push_back(0);
//    right.push_back(6);
//    int n = 4;
//
//
//    int ret = findMinimum(n, left, right);
//    cout << ret;
//}