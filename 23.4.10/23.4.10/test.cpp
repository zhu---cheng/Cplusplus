#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
class Date
{
public:
	Date(int year = 1900, int month = 1, int day = 1)
		: _year(year)
		, _month(month)
		, _day(day)
	{}

	bool operator<(const Date& d)const
	{
		return (_year < d._year) ||
			(_year == d._year && _month < d._month) ||
			(_year == d._year && _month == d._month && _day < d._day);
	}

	bool operator>(const Date& d)const
	{
		return (_year > d._year) ||
			(_year == d._year && _month > d._month) ||
			(_year == d._year && _month == d._month && _day > d._day);
	}

	friend ostream& operator<<(ostream& _cout, const Date& d)
	{
		_cout << d._year << "-" << d._month << "-" << d._day;
		return _cout;
	}

private:
	int _year;
	int _month;
	int _day;
};


////函数模板特化
//template<>
//bool Less<Date*>(Date* left,Date* right)
//{
//	return *left < *right;
//}
//
////但是函数模板特化不实用，一般情况下如果函数模板遇到不能处理或者处理有误的类型
////，为了实现简单通常都是将该函数直接给出。
////函数重载即可
//
//////因此函数模板不建议特化
////bool Less(Date* left, Date* right)
////{
////	return *left < *right;
////}
////
////
////
//int main()
//{
//	cout << Less(1, 2) << endl;   // 可以比较，结果正确
//
//	Date d1(2022, 7, 7);
//	Date d2(2022, 7, 8);
//	cout << Less(d1, d2) << endl;  // 可以比较，结果正确
//
//	Date* p1 = &d1;
//	Date* p2 = &d2;
//	cout << Less(p1, p2) << endl;  // 可以比较，结果错误（没有函数模板特化前）
//
//	//int* p3 = new int(1);
//	//int* p4 = new int(2);
//	//cout << Less(p3, p4) << endl;  // 可以比较
//
//	return 0;
//}
//


//
//template<class T1, class T2>
//class Data
//{
//public:
//	Data() { cout << "Data<T1, T2>" << endl; }
//private:
//	T1 _d1;
//	T2 _d2;
//};
//template<>
//class Data<int, char>
//{
//public:
//	Data() { cout << "Data<int, char>" << endl; }
//private:
//	int _d1;
//	char _d2;
//
//};
//void TestVector()
//{
//	Data<int, int> d1;
//	Data<int, char> d2;
//}

//
////半特化 / 偏特化（）半特化不是特化一半
////1、将部分模板参数列表中的一部分参数特化
//template<class T1>
//class Date<T1, char>
//{
//public:
//	Date() { cout << "Date<T1, char>" << endl; }
//};
//
////2、偏特化并不仅仅是指特化部分参数，而是针对模板参数更进一步的条件限制所设计出来的一个特化版本
////只要T1 和 T2是指针就走这个 -- 针对指针特殊化处理
//template<class T1, class T2>
//class Date<T1*, T2*>
//{
//public:
//	Date() { cout << "Date<T1*, T2*>" << endl; }
//};
//
//template<class T1, class T2>
//class Date<T1&, T2&>
//{
//public:
//	Date() { cout << "Data<T1&, T2&>" << endl; }
//};
//
//int main()
//{
//	Date<int, int> d1;
//	Date<int, double> d2;
//
//	//只要第二个是char都会匹配：半特化/偏特化
//	Date<int, char> d3;
//	Date<char, char> d4;
//
//	//只要是两个指针
//	Date<int*, int*> d5;
//	Date<int*, char*> d6;
//	Date<int*, string*> d7;
//	Date<int*, void*> d8;
//	//void不是类型，但是void*是一个类型，void*是不能解引用不能++
//
//	Date<int*, int> d9;//匹配原生的指针
//	Date<int&, char&> d10;
//
//	return 0;
//}
//
//
//

//template<class T>

// 函数模板 -- 参数匹配
//template<class T>
//bool Less(T left, T right)
//{
//	return left < right;
//}

//template<class T>
//struct Less
//{
//	bool operator()(const T& x, const T& y) const
//	{
//		return x < y;
//	}
//};
//int main()
//{
//	Date d1(2022, 7, 7);
//	Date d2(2022, 7, 6);
//	Date d3(2022, 7, 8);
//	vector<Date> v1;
//	v1.push_back(d1); v1.push_back(d2);
//	v1.push_back(d3);
//	// 可以直接排序，结果是日期升序
//	sort(v1.begin(), v1.end(), Less<Date>());
//
//	for (auto ch1: v1)
//	{
//		cout << ch1<<" ";
//	}
//	cout << endl;
//	vector<Date*> v2;
//	v2.push_back(&d1);
//	v2.push_back(&d2);
//	v2.push_back(&d3);
//
//	// 可以直接排序，结果错误日期还不是升序，而v2中放的地址是升序
//	// 此处需要在排序过程中，让sort比较v2中存放地址指向的日期对象
//	// 但是走Less模板，sort在排序时实际比较的是v2中指针的地址，因此无法达到预期
//	sort(v2.begin(), v2.end(), Less<Date*>());
//
//	for (auto ch2 : v1)
//	{
//		cout << ch2<<" ";
//	}
//	cout << endl;
//	return 0;
//}





//模板的特化 -- 应用
template<class T>
struct Less
{
	bool operator()(const T& x, const T& y) const
	{
		return x < y;
	}
};

template<>
struct Less<Date*>
{
	bool operator()(Date* x, Date* y) const
	{
		return *x < *y;
	}
};

//偏特化
//只要是指针都走这里
template<class T>
struct Less<T*>
{
	bool operator()(T* x, T* y) const
	{
		return *x < *y;
	}
};

int main()
{
	Date d1(2022, 7, 7);
	Date d2(2022, 7, 6);
	Date d3(2022, 7, 8);

	vector<Date> v1;
	v1.push_back(d1);
	v1.push_back(d2);
	v1.push_back(d3);
	// 可以直接排序，结果是日期升序
	sort(v1.begin(), v1.end(), Less<Date>());

	vector<Date*> v2;
	v2.push_back(&d1);
	v2.push_back(&d2);
	v2.push_back(&d3);

	//可以直接排序，结果错误日期还不是升序，而v2中放的地址是升序
	//此处需要在排序过程中，让sort比较v2中存放地址指向的日期对象
	//但是走Less模板，sort在排序时实际比较的是v2中指针的地址，因此无法达到预期
	sort(v2.begin(), v2.end(), Less<Date*>());

	vector<int*> v3;
	v3.push_back(new int(3));
	v3.push_back(new int(1));
	v3.push_back(new int(2));
	sort(v3.begin(), v3.end(), Less<int*>());

	
	return 0;
}
