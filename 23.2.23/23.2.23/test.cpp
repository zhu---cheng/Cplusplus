#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>

#include <vector>

using namespace std;

//int main(void)
//
//{
//
//	vector<int>array;
//
//	array.push_back(100);
//
//	array.push_back(300);
//
//	array.push_back(300);
//
//	array.push_back(300);
//
//	array.push_back(300);
//
//	array.push_back(500);
//
//	vector<int>::iterator itor;
//
//	for (itor = array.begin(); itor != array.end(); itor++)
//
//	{
//
//		if (*itor == 300)
//
//		{
//
//			itor = array.erase(itor);
//
//		}
//
//	}
//
//	for (itor = array.begin(); itor != array.end(); itor++)
//
//	{
//
//		cout << *itor << " ";
//
//	}
//
//	return 0;
//
//}
//


////最小公倍数
//#include<iostream>
//using namespace std;
//int main()
//{
//    int a, b;
//    cin >> a ;
//    cin >> b;
//    int c = a * b;
//    int gys = a % b;
//    while (gys)
//    {
//        a = b;
//        b = gys;
//        gys = a % b;
//    }
//    int gbs = c / b;
//    cout << gbs;
//    return 0;
//}


//输入指定行并比较字符顺序和长度

#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main() {
    int n;
    cin >> n;
    vector<string> v(n);
    for (int i = 0; i < n; ++i)
        cin >> v[i];
    bool strsort = true, lensort = true; // 标记排序情况 默认是满足的
    for (int i = 1; i < n; ++i) { // 避免越界，i从1开始访问
        if (v[i - 1] > v[i]) {
            strsort = false;
            break;
        }
    }
    for (int i = 1; i < n; ++i) { // 避免越界，i从1开始访问
        if (v[i - 1].size() > v[i].size()) {
            lensort = false;
            break;
        }
    }
    if (strsort == true && lensort == false)
        cout << "lexicographically" << endl;
    else if (strsort == false && lensort == true)
        cout << "lengths" << endl;
    else if (strsort && lensort)
        cout << "both" << endl;
    else
        cout << "none" << endl;

    return 0;
}