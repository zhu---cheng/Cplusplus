#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//函数重载
//函数重载：是函数的一种特殊情况，C++允许在同一作用域中声明几个功能类似的同名函数，这
//些同名函数的形参列表(参数个数 或 类型 或 类型顺序)不同，常用来处理实现功能类似数据类型
//不同的问题。

//lunix环境下翻译成
//_Z3Addii
//1参数类型不同
//int Add(int left, int right)
//{
//	cout << "int Add(int left, int right)" << endl;
//	return left + right;
//}
////_Z3Adddd
//double Add(double left, double right)
//{
//	cout << "double Add(double left, double right)" << endl;
//	return left + right;
//}
//
//
//// 2、参数个数不同
//void f()
//{
//	cout << "f()" << endl;
//}
//void f(int a)
//{
// cout << "f(int a)" << endl;
//}
//
//
//// 3、参数类型顺序不同
////_Z1fii
//void f(int a, char b)
//{
//	cout << "f(int a,char b)" << endl;
//}
//void f(char b, int a)
//{
//	cout << "f(char b, int a)" << endl;
//}
//int main()
//{
//	Add(1, 2);
//	Add(1.1, 3.3);
//
//	f();
//	f(2);
//
//
//	f(1, 'a');
//	f('a', 1);
//	return 0;
//}
//注意，只有返回值不同不是函数重载




//引用
//int main()
//{
//
//	int i = 0;
//	int& k = i;
//	int j = i;
//
//
//	cout << &i << endl;
//	cout << &k << endl;
//	cout << &j << endl;
//
//	++k;
//	++j;
//	int& n = i;
//	int& m = k;
//	++n;
//
//	return 0;
//
//}

//引用应用
void swap(int& m, int& n)
{
	int tmp = m;
	m = n;
	n = tmp;
}
typedef struct Node
{
	struct Node* next;
	int val;
}Node, * PNode;

//void PushBack(Node*& phead, int x)
void PushBack(PNode& phead, int x)
{
	Node* newNode = (Node*)malloc(sizeof(Node));
	if (phead == nullptr)
	{
		phead = newNode;
	}
}

typedef struct SeqList
{
	int* a;
	int size;
	int capaicty;
}SQ;

void PushBack(SQ& s, int x)
{

}



int main()
{
	int i = 0;
	int j = 2;
	swap(i, j);//有了引用，不需要传指针就可以交换数据了
	cout << i << endl;
	cout << j << endl;

	Node* plist = NULL;
	PushBack(plist, 1);//不需要传地址使用二级指针了
	PushBack(plist, 2);
	PushBack(plist, 3);

	return 0;
}