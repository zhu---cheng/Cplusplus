#define _CRT_SECURE_NO_WARNINGS 1
////https://leetcode.cn/problems/top-k-frequent-words/description/
//class Solution {
//public:
//
//    // struct Compare
//    // {
//    //     bool operator()(const pair<string,int>& v1,const pair<string,int>& v2)
//    //     {
//    //         return v1.second>v2.second;
//    //     }
//    // };
//
//    struct Compare
//    {
//        bool operator()(const pair<string, int>& v1, const pair<string, int>& v2)
//        {
//            return (v1.second > v2.second || (v1.second == v2.second && v1.first < v2.first));
//        }
//    };
//    vector<string> topKFrequent(vector<string>& words, int k) {
//
//        map<string, int> countMap;
//        for (auto& str : words)
//        {
//            countMap[str]++;
//        }
//
//        vector<pair<string, int>> v(countMap.begin(), countMap.end());
//        // stable_sort(v.begin(),v.end(),Compare());
//        sort(v.begin(), v.end(), Compare());
//
//        vector<string> ret;
//        for (int i = 0; i < k; i++)
//        {
//            ret.push_back(v[i].first);
//        }
//        return ret;
//    }
//};




//https://leetcode.cn/problems/intersection-of-two-arrays/
//
//class Solution {
//public:
//    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
//
//        set<int> s1(nums1.begin(), nums1.end());
//        set<int> s2(nums2.begin(), nums2.end());
//
//        auto it1 = s1.begin();
//        auto it2 = s2.begin();
//
//        vector<int> v;
//        while (it1 != s1.end() && it2 != s2.end())
//        {
//            if (*it1 < *it2)
//            {
//                it1++;
//            }
//            else if (*it2 < *it1)
//            {
//                it2++;
//            }
//            else
//            {
//                v.push_back(*it1);
//                it1++;
//                it2++;
//            }
//        }
//        return v;
//    }
//};