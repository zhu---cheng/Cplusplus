#define _CRT_SECURE_NO_WARNINGS 1
//#include<iostream>
//int main()
//{
//	std::cout << "hello world" << std::endl;
//
//}

//using namespace std;
//int main()
//{
//	cout << "hello world" << endl;
//}


//
//#include<stdio.h>
//#include<stdlib.h>
//int rand = 10;
//int main()
//{
//	printf("%d\n", rand);
//
//}
//这里报错rand重定义，因为stdlib库里有rand函数
//c++提出用namespace来解决


//
//#include<stdio.h>
//#include<stdlib.h>
//int a = 2;
//
//void f1()
//{
//	int a = 0;
//	printf("%d", a);
//	printf("%d", ::a);//::域作用限定符
//}
//
//
//int main()
//{
//	f1();
//	return 0;
//}


//
//#include"List.h"
//#include"Queue.h"
//int main()
//{
//	struct AQueue::Node node1;
//	struct BList::B::Node node2;
//
//	bit::A::min++;
//	BList::B::min++;
//
//
//	return 0;
//}


//1、指定命名空间访问
// 2、全局展开. 一般情况，不建议全局展开的。
// 3、部分
//#include<iostream>
//#include"Queue.h"
//#include"List.h"
//using namespace bit;
//using namespace std;
////全局
//int main()
//{
//	struct Queue q1;
//	QueueInit(&q1);
//	QueuePush(&q1, 2);
//	
//	//cout << "11111" << endl;
//	std::cout << "11111" ;//指定
//
//	return 0;
//}



#include<iostream>
using namespace std;
//// 实际开发的项目工程
//// 1、指定命名空间访问
//// 3、常用部分展开
//
//// 小的程序，日常练习，不太会出现冲突
//// 2、全局展开.一般情况，不建议全局展开的。
//
//// 常用展开
//using std::cout;
//using std::endl;
//int main()
//{
//	cout << "1111" << endl;
//	cout << "1111" << endl;
//	cout << "1111" << endl;
//	cout << "1111" << endl;
//
//	//指定
//	int i = 0;
//	std::cin >> i;
//
//
//	return 0;
//}


//#include<iostream>
//using namespace std;
////<< 流插入
////endl    '\n'
//
//int main()
//{
//	cout << "hello world" << endl;
//	cout << "hello world" << '\n';
//
//	//自动识别模型
//	int n = 0;
//	cin >> n;
//
//	double* a = (double*)malloc(sizeof(double) * n);
//	if (a == NULL)
//	{
//		perror("malloc fail");
//		exit(-1);
//	}
//	for (int i = 0; i < n; i++)
//	{
//		cin >> a[i];
//	}
//
//	for (int i = 0; i < n; i++)
//	{
//		cout << a[i] << ' ';
//	}
//	cout << endl;
//
//	char name[10] = "张三";
//	int age = 18;
//	cout << "姓名：" << name << endl;
//	cout << "年龄：" << age << endl;
//
//	return 0;
//}


//////////////////////////////////////////////////
#include<iostream>
using namespace std;
//void Func(int a = 0)
//{
//	cout << a << endl;
//}
//
//int main()
//{
//	Func(1);
//	Func();
//}

//void Func(int a = 10, int b = 20, int c = 30)
//{
//	cout << "a=" << a << endl;
//	cout << "b=" << b << endl;
//	cout << "c=" << c << endl;
//	cout << endl;
//}
//
//int main()
//{
//	Func(1, 2, 3);
//	Func(1, 2);
//	Func(1);
//	Func();
//
//	return 0;
//	//半缺省，只能从右往左缺省
//}




void Func(int a, int b = 10, int c = 20)
{
	cout << "a = " << a << endl;
	cout << "b = " << b << endl;
	cout << "c = " << c << endl;
}

struct Stack
{
	int* a;
	int top;
	int capacity;
};

void StackInit(struct Stack* ps, int defaultCapacity = 4)
{
	ps->a = (int*)malloc(sizeof(int) * defaultCapacity);
	if (ps->a == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	ps->top = 0;
	ps->capacity = defaultCapacity;
}

int main()
{
	// 使用缺省值，必须从右往左连续使用
	Func(1, 2, 3);
	Func(1, 2);
	Func(1);

	Stack st1; // 最多要存100个数
	StackInit(&st1, 100);

	Stack st2; // 不知道多少数据
	StackInit(&st2);

	return 0;
}