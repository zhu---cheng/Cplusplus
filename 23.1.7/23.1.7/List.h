#pragma once

namespace BList
{
	namespace B
	{
		struct Node
		{
			struct Node* prev;
			struct Node* next;
			int val;
		};

		int min = 1;
	}
}

