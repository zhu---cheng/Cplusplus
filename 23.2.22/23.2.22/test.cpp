#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<assert.h>
using namespace std;

//如果一个类中没有任何成员，那么这个类叫做空类。
//那么空类中真的什么都没有吗？
//其实空类中含有默认成员函数
//默认成员函数：用户没有显式实现，编译器会生成的成员函数称为默认成员函数。



//
class Stack
{
public:
	//构造函数
	Stack()
	{
		cout << "Stack()" << endl;
		_a = nullptr;
		_size = _capacity = 0;
	}

	Stack(int n)//构造函数可以重载
	{
		cout << "Stack()" << endl;
		_a = (int*)malloc(sizeof(int)*n);
		if (_a == nullptr)
		{
			perror("Stack()");
			exit(-1);
		}
		_size = 0;
		_capacity = n;
	}


	//析构函数
	~Stack()
	{
		cout << "~Stack()" << endl;
		free(_a);
		_a = nullptr;
		_size = _capacity = 0;
	}

	void Push(int x)
	{
		//...
		_a[_size++] = x;
	}

	bool Empty()
	{
		// ...
		return _size == 0;
	}

	int Top()
	{
		//...
		return _a[_size - 1];
	}

private:
	int* _a;
	int _size;
	int _capacity;

};

//
//int main()
//{
//	Stack s1(4);
//	/*对于Date类，可以通过 Init 公有方法给对象设置日期，但如果每次创建对象时都调用该方法设置
//		信息，未免有点麻烦，那能否在对象创建时，就将信息设置进去呢？
//		构造函数是一个特殊的成员函数，名字与类名相同, 创建类类型对象时由编译器自动调用，以保证
//		每个数据成员都有 一个合适的初始值，并且在对象整个生命周期内只调用一次*/
//	s1.Push(1);
//	s1.Push(2);
//	s1.Push(3);
//	s1.Push(4);
//
//	//这里虽然没写，但是调用了析构函数
//	return 0;
//}



//class Date
//{
//public:
//
//	//Date()
//	//{
//	//	_year = 1;
//	//	_month = 1;
//	//	_day = 1;
//	//}
//	//Date(int year,int month,int day)
//	//{
//	//	_year = year;
//	//	_month = month;
//	//	_day = day;
//	//}
//
//	//以上两种可以合写为如下
//	/*Date(int year=1, int month=1, int day=1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}*/
//
//	void Print()
//	{
//		cout << _year << "年" << _month << "月" << _day << "日" << endl;
//
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//
//

//
//
//};

//
//int main()
//{
//	Date d1(2022, 3, 2);
//	Date d2;
//	Date d3(2022);
//	d1.Print();
//	d2.Print();
//	d3.Print();
//}


////默认生成构造函数，对内置类型不处理
//int main()
//{
//	Date d1;
//	//如果不写构造函数，编译器自己会生成，我们可以调试看看
//	//我们发现初始化一些随机值，难道编译器的初始化没用吗？
//
//
//	/*解答：C++把类型分成内置类型(基本类型)和自定义类型。内置类型就是语言提供的数据类
//		型，如：int / char...，自定义类型就是我们使用class / struct / union等自己定义的类型，看看
//		下面的程序，就会发现编译器生成默认的构造函数会对自定类型成员_t调用的它的默认成员
//		函数。*/
//	d1.Print();
//	
//}

//class Time
//{
//public:
//	Time()
//	{
//		cout << "Time()" << endl;
//		 _hour = 0;
//		 _minute = 0;
//		 _second = 0;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//
//class Date
//{
//public:
//	//	/*Date(int year=1, int month=1, int day=1)
////	{
////		_year = year;
////		_month = month;
////		_day = day;
////	}*/
//	void Print()
//	{
//		cout << _year << "年" << _month << "月" << _day << "日" << endl;
//
//	}
//
//private:
//	//内置类型
// //注意：C++11 中针对内置类型成员不初始化的缺陷，又打了补丁，即：内置类型成员变量在类中声明时可以给默认值。
//	int _year=1;
//	int _month=1;
//	int _day=1;
//
//	//自定义类型
//	Time _t;
//
//
//};
//int main()
//{
//	Date d1;
//	return 0;
//}



//////////////////////////////////////////////////

//class Date
//{
//public:

	//Date()
	//{
	//	_year = 1;
	//	_month = 1;
	//	_day = 1;
	//}
	//Date(int year,int month,int day)
	//{
	//	_year = year;
	//	_month = month;
	//	_day = day;
	//}

	//以上两种可以合写为如下
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	void Print()
//	{
//		cout << _year << "年" << _month << "月" << _day << "日" << endl;
//
//	}
//
//	//拷贝构造函数，
//	// 传值方式编译器直接报错，
//	//因为会引发无穷递归调用。
//	Date(const Date& d)
//	{
//		cout << "Date(Date& d)" << endl;
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//	}
//
//
//
//private:
//	int _year;
//	int _month;
//	int _day;
//
//};
//
//int main()
//{
//	Date d1;
//
//	Date d2(d1);
//	d1.Print();
//	d2.Print();
//	return 0;
//}

//class Time
//{
//public:
//	Time()
//	{
//		_hour = 1;
//		_minute = 1;
//		_second = 1;
//	}
//	Time(const Time& t)
//	{
//		_hour = t._hour;
//		_minute = t._minute;
//		_second = t._second;
//		cout << "Time::Time(const Time&)" << endl;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//
//	class Date
//	{
//	public:
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	void Print()
//	{
//		cout << _year << "年" << _month << "月" << _day << "日" << endl;
//
//	}
//
//	
//	
//private:
//	int _year;
//	int _month;
//	int _day;
//
//	Time _t;
//};

//int main()
//{
//	Date d1;
//
//	Date d2(d1);
//
//	//Date没有显性的拷贝构造函数，编译器会生成默认的拷贝构造函数
//	//默认的拷贝构造函数是按照内存存储按
//	//字节序完成拷贝，这种拷贝叫做浅拷贝，或者值拷贝。
//	d1.Print();
//	d2.Print();
//	return 0;
//}



// 这里会发现下面的程序会崩溃掉？这里就需要我们以后讲的深拷贝去解决。
//
//int main()
//{
//	Stack s1(4);
//	s1.Push(1);
//	s1.Push(2);
//	s1.Push(3);
//	s1.Push(4);
//	Stack s2(s1);
//
//	/*注意：类中如果没有涉及资源申请时，拷贝构造函数是否写都可以；一旦涉及到资源申请
//		时，则拷贝构造函数是一定要写的，否则就是浅拷贝。*/
//	return 0;
//}


	class Date
	{
	public:
		Date(int year = 1900, int month = 1, int day = 1)
		{
			_year = year;
			_month = month;
			_day = day;
		}

		// 拷贝构造
		// Date d2(d1);
		Date(const Date& d)
		{
			cout << "Date(Date& d)" << endl;

			_year = d._year;
			_month = d._month;
			_day = d._day;

			/*d._year = _year;
			d._month = _month;
			d._day = _day;*/
		}

		int GetMonthDay(int year, int month)
		{
			assert(month > 0 && month < 13);

			int monthArray[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
			if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400) == 0))
			{
				return 29;
			}
			else
			{
				return monthArray[month];
			}
		}

		// d1.GetAfterXDay(100);
		//Date GetAfterXDay(int x)
		//{
		///*	Date tmp;
		//	tmp._year = _year;
		//	tmp._month = _month;
		//	tmp._day = _day;*/
		//	//Date tmp(*this);
		//	Date tmp = *this;

		//	tmp._day += x;
		//	while (tmp._day > GetMonthDay(tmp._year, tmp._month))
		//	{
		//		// 进位
		//		tmp._day -= GetMonthDay(tmp._year, tmp._month);
		//		++tmp._month;
		//		if (tmp._month == 13)
		//		{
		//			tmp._year++;
		//			tmp._month = 1;
		//		}
		//	}

		//	return tmp;
		//}

		// +
		Date Add(int x)
		{
			Date tmp = *this;
			tmp._day += x;
			while (tmp._day > GetMonthDay(tmp._year, tmp._month))
			{
				// 进位
				tmp._day -= GetMonthDay(tmp._year, tmp._month);
				++tmp._month;
				if (tmp._month == 13)
				{
					tmp._year++;
					tmp._month = 1;
				}
			}

			return tmp;
		}

		// +=
		Date& AddEqual(int x)
		{
			_day += x;
			while (_day > GetMonthDay(_year, _month))
			{
				// 进位
				_day -= GetMonthDay(_year, _month);
				++_month;
				if (_month == 13)
				{
					_year++;
					_month = 1;
				}
			}

			return *this;
		}

		void Print()
		{
			//cout << _year << "/" << _month << "/" << _day << endl;
			cout << _year << "年" << _month << "月" << _day << "日" << endl;
		}

	private:
		int _year;
		int _month;
		int _day;
	};

	int main()
	{
		Date d1(2023, 2, 3);
		Date d2 = d1.Add(100);
		Date d3 = d1.Add(150);

		d1.Print();
		d2.Print();
		d3.Print();

		d1.AddEqual(200);
		d1.Print();

		// 实现一个函数，获取多少以后的一个日期

		return 0;
	}