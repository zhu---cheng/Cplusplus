#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>

using namespace std;
//
//
//void Fun(int& x) { cout << "左值引用" << endl; }
//void Fun(const int& x) { cout << "const 左值引用" << endl; }
//
//void Fun(int&& x) { cout << "右值引用" << endl; }
//void Fun(const int&& x) { cout << "const 右值引用" << endl; }
//
////std::forward<T>(t)在传参的过程中保持了t的原生类型属性。
//
//template<typename T>
//void PerfectForward(T&& t)
//{
//	//完美转发，按照原封不动的方式进行转发
//	//Fun(t);
//	Fun(std::forward<T>(t));
//}
//
//int main()
//{
//	PerfectForward(10);           //右值
//	
//	int a;
//	PerfectForward(a);            //左值
//	PerfectForward(std::move(a)); //右值/
//	const int b = 8;
//	PerfectForward(b);		      //const 左值
//	PerfectForward(std::move(b)); //const 右值
//
//
//	//Fun(10);
//
//	//int a=1;
//	//Fun(a);
//	//Fun(move(a));
//
//	//const int b = 12;
//	//Fun(b);
//	//Fun(move(b));
//
//	return 0;
//}


//
////语法不支持这么玩
//template<class ...Args>
//void ShowList(Args...args)
//{
//	cout << sizeof...(args)<< endl;
//	for (int i = 0; i < sizeof...(args); i++)
//	{
//		cout << args[i] << " ";
//	}
//	cout << endl;
//}
//void ShowList()
//{
//	cout << endl;
//}
//int main()
//{
//	ShowList(1, 2, 3, 4, '1', 'a');
//}



// 了解
// Args是一个模板参数包，args是一个函数形参参数包
// 声明一个参数包Args...args，这个参数包中可以包含0到任意个模板参数。
// 
// 如何解析出可变参数包呢？
// 递归推导思维
//
//template <class T, class ...Args>
//void ShowList(const T& val, Args... args)
//{
//	cout << __FUNCTION__ << "(" << sizeof...(args) <<")" << endl;
//
//	cout << val << " ";
//	ShowList(args...);
//}
//void ShowList()
//{
//	cout << endl;
//}
//int main()
//{
//	//ShowList();
//	//ShowList(1);
//	//ShowList(1, 'A');
//	ShowList(1, 'A', std::string("sort"));
//
//	return 0;
//}


//

//void _ShowList()
//{
//	cout << endl;
//}
//
//template <class T, class ...Args>
//void _ShowList(const T& val, Args... args)
//{
//	cout << __FUNCTION__ << "(" << sizeof...(args) << ")" << endl;
//
//	cout << val << " ";
//	_ShowList(args...);
//}
//
//template <class ...Args>
//void ShowList(Args... args)
//{
//	_ShowList(args...);
//}
//
//int main()
//{
//	ShowList(1, 'A', std::string("sort"));
//
//	return 0;
//}

//
//template <class T>
//int PrintArg(T t)
//{
//	cout << t << " ";
//
//	return 0;
//}
//
//template <class ...Args>
//void ShowList(Args... args)
//{
//	int arr[] = { PrintArg(args)... };
//	cout << endl;
//}
//
//// 编译器编译推演生成了一下代码
////void ShowList(char a1, char a2, std::string a3)
////{
////	int arr[] = { PrintArg(a1),PrintArg(a2),PrintArg(a3) };
////	cout << endl;
////}
//
//int main()
//{
//	ShowList(1, 'A', std::string("sort"));
//
//	//ShowList(1, 2, 3);
//	//ShowList(1);
//
//	return 0;
//}


#include<vector>
#include<iostream>
#include<algorithm>
using namespace std;
struct Goods
{
	string _name;
	double _price;
	int _evaluate;

	Goods(const char* str,double price,int evaluate)
		:
		_name(str),
		_price(price),
		_evaluate(evaluate)
	{}
};

struct ComparePriceLess
{
	bool operator()(const Goods& gl, const Goods& gr)
	{
		return gl._price < gr._price;
	}
};


struct ComparePriceGreater
{
	bool operator()(const Goods& gl, const Goods& gr)
	{
		return gl._price > gr._price;
	}
};
//
//int main()
//{
//	vector<Goods> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "橙子", 2.2, 3 }, { "菠萝", 1.5, 4 } };
//
//	// <
//	sort(v.begin(), v.end(), ComparePriceLess());
//	// >
//	sort(v.begin(), v.end(), ComparePriceGreater());
//}
//


//int main()
//{
//	auto add1 = [](int x, int y)->int {return x + y; };
//	cout << add1(2, 3) << endl;
//
//
//	auto add2 = [](int x, int y) {return x + y;  };
//	cout << add2(1, 6) << endl;
//
//	//最简单的lanbda形式：[]{}
//	return 0;
//}
//

//
//
//int main()
//{
//	vector<Goods> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "橙子", 2.2, 3 }, { "菠萝", 1.5, 4 } };
//
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2)->bool {return g1._price < g2._price; });
//
//
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2)->bool {return g1._evaluate > g2._evaluate; });
//
//	return 0; 
//
//}


int main()
{
	int x = 0, y = 1;
	int m = 0, n = 1;
	auto swap1 = [](int& rx, int& ry)
	{
		int tmp = rx;
		rx = ry;
		ry = tmp;
	};

	swap1(x, y);
	cout << x << " " << y << endl;


	//传值捕捉
	auto swap2 = [x, y]()mutable
	{
		int tmp = x;
		x = y;
		y = tmp;
	};
	swap2();
	cout << x << " " << y << endl;
	//达不到效果，需要引用捕捉


	//引用捕捉
	auto swap3 = [&x, &y]() {int tmp = x;
	x = y; y = tmp; };
	swap3();
	cout << x << " " << y << endl;

	//全引用捕捉
	auto func2 = [&]() {
	//....
	};
	//全传值捕捉
	auto fun3 = [=]()
	{
		//...
	};

	//全引用捕捉，x传值捕捉
	auto func4=[&, x]()
	{
		//...
	};

	return 0;
}