#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
using namespace std;
//
//class Person {
//public:
//	virtual void BuyTicket() { cout << "买票-全价" << endl; }
//};
//
//
//int main()
//{
//
//	return 0;
//}



////多态只用的样例：
//class Person
//{
//public:
//	Person(const char* name)
//		:_name(name)
//	{}
//	//虚函数
//	virtual void BuyTicket() { cout << _name << " Person: 买票-全价 100￥" << endl; }
//
//protected:
//	string _name;
//	//int _id;
//};
//
//class Student : public Person
//{
//public:
//	Student(const char* name)
//		:Person(name)
//	{}
//	//虚函数 + 函数名/参数/返回值 -> 重写/覆盖
//	virtual void BuyTicket() { cout << _name << " Student: 买票-半价 50￥" << endl; }
//};
//
//class Soldier : public Person
//{
//public:
//	Soldier(const char* name)
//		:Person(name)
//	{}
//	//虚函数 + 函数名/参数/返回值 -> 重写/覆盖
//	virtual void BuyTicket() { cout << _name << " Soldier: 优先买预留票-88折 100￥" << endl; }
//};
//
//void Pay(Person* ptr)
//{
//	ptr->BuyTicket();
//	delete ptr;
//}
//
////赋值兼容的转换，父类指针可以指向父类对象，也可以指向子类对象
//void Pay(Person& ptr)
//{
//	ptr.BuyTicket();
//}

//全部都去调用父类去了 -- 不构成多态
//void Pay(Person ptr)
//{
//	ptr.BuyTicket();
//}

//int main()
//{
//	int option = 0;
//	cout << "=========================================" << endl;
//	do
//	{
//		cout << "请选择身份：";
//		cout << "1、普通人 2、学生 3、军人" << endl;
//		cin >> option;
//
//		cout << "请输入名字：";
//		string name;
//		cin >> name;
//
//		//switch case语句里面，是不能支持定义对象的，要加一个域{}
//		//加完域之后就是局部域了
//		switch (option)
//		{
//		case 1:
//		{
//			Person p(name.c_str());
//			Pay(p);
//			break;
//		}
//		case 2:
//		{
//			Student s(name.c_str());
//			Pay(s);
//			break;
//		}
//		case 3:
//		{
//
//			Soldier s(name.c_str());
//			Pay(s);
//			break;
//		}
//		default:
//			cout << "输入错误，请从新输入" << endl;
//			break;
//		}
//		cout << "=========================================" << endl;
//	} while (option != -1);
//
//	return 0;
//}


//
//class A
//{
//public:
//	virtual void func(int val = 1) { std::cout << "A->" << val << std::endl; }
//	virtual void test() { func(); }
//};
//
//class B : public A
//{
//public:
//	void func(int val = 0) { std::cout << "B->" << val << std::endl; }
//};
//
//int main()
//{
//	B* p = new B;
//	p->test();
//
//	return 0;
//}



//class Person {
//public:
//	virtual ~Person() { cout << "~Person()" << endl; }
//	//~Person() { cout << "~Person()" << endl; }
//};
//class Student : public Person {
//public:
//	virtual ~Student() { cout << "~Student()" << endl; }
//	//~Student() { cout << "~Student()" << endl; }
//};
//// 只有派生类Student的析构函数重写了Person的析构函数，下面的delete对象调用析构函
////数，才能构成多态，才能保证p1和p2指向的对象正确的调用析构函数。
//int main()
//{
//	Person* p1 = new Person;
//	Person* p2 = new Student;
//	delete p1;
//	delete p2;
//	return 0;
//}

//
//class Car
//{
//public:
//	virtual void Drive() final {}
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive() { cout << "Benz-舒适" << endl; }
//};

//
//class Car {
//public:
//	void Drive() {}
//};
//class Benz :public Car {
//public:
//	virtual void Drive() override { cout << "Benz-舒适" << endl; }
//};
//int main()
//{
//	Benz b;
//	return 0;
//}
//抽象类
//class Car
//{
//public:
//	//纯虚函数
//	virtual void Drive() = 0;
//};
//
//int main()
//{
//	Car c;
//
//	return 0;
//}



class Car
{
public:
	virtual void Drive() = 0;
};
class Benz :public Car
{
public:
	virtual void Drive()
	{
		cout << "Benz-舒适" << endl;
	}
};
class BMW :public Car
{
public:
	virtual void Drive()
	{
		cout << "BMW-操控" << endl;
	}
};
int main()
{
	Car* pBenz = new Benz;
	pBenz->Drive();
	Car* pBMW = new BMW;
	pBMW->Drive();
}
