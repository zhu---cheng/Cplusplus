﻿#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<list>
#include<vector>
#include<assert.h>
#include<map>
#include<string>

using namespace std;

struct Point
{
	Point(int x = 1, int y = 2)
		:_x(x),
		_y(y)
	{}

	int _x;
	int _y;
};
//int main()
//{
//	//int x1 = 1;
//	//int x2 = int();
//	//cout << x1 << endl;
//
//	//int* p1 = new int(1);
//	//int* p2 = new int[3]{ int(1),int(2),int(3) };
//	//int* p3 = new int[3]{ 1,2,3 };
//
//
//	//Point p4(1, 2);
//	//Point p5{ 1,2 };//c++11
//
//	//int array1[]{ 1,2,3,4,5 };//c++11
//	//int x2{ 3 };//c++11
//
//	
//	/// //////////////////////////////////////////////
//	//98中至少需要这样定义，98只能new单个对象，new多个对象没办法很好的初始化了
//	/*Point p1, p2, p3, p4;
//	Point* pp1 = new Point[]{ p1, p2, p3, p4 };
//	Point* pp2 = new Point[]{ Point(1, 1), Point(2, 2), Point(3, 3), Point(4, 4) };*/
//
//
//	Point p1[] = { {1, 1}, {2, 2}, {3, 3}, {4, 4} };
//	Point p2[]{ {1, 1}, {2, 2}, {3, 3}, {4, 4} };
//	Point* p3 = new Point[]{ {1, 1}, {2, 2}, {3, 3}, {4, 4} };
//
//	
//}

//
//
//int main()
//{
//	vector<int> lt{ 1,2,3,4 };
//	cout << typeid(lt).name() << endl;
//
//	auto i1={ 1,2,3,4,5,6,7,8,9 };
//	cout << typeid(i1).name() << endl;
//
//	//有了std::initializer_list，之前学的容器也都支持了用{ }列表初始化
//}

class Date
{
public:
	//explicit Date(int year, int month, int day)
	Date(int year, int month, int day)
		:_year(year)
		, _month(month)
		, _day(day)
	{
		cout << "Date(int year, int month, int day)" << endl;
	}

private:
	int _year;
	int _month;
	int _day;
};

////以vector，和map为例，C++11之后就我们之前学的容器可以直接通过{ } 列表初始化了：
//int main()
//{
//	vector<Date> v1 = { { 2023, 3, 7 }, { 2023, 3, 7 }, { 2023, 3, 7 } };
//	vector<Date> v2{ { 2023, 3, 7 }, { 2023, 3, 7 }, { 2023, 3, 7 } };
//
//	map<string, string> dict1 = { { "string", "字符串" }, { "sort", "排序" } };
//	map<string, string> dict2{ { "string", "字符串" }, { "sort", "排序" } };
//
//	return 0;
//}

//
//int main()
//{
//	int x = 1;
//	int y{ 2 };
//	decltype (x * y) ret;
//	cout << typeid(ret).name() << endl;
//
//	vector<decltype(x* y)> v1;
//}




//int main()
//{
//	double x = 1.1, y = 2.2;
//	// 以下几个都是常见的右值
//	10;
//	x + y;
//	fmin(x, y);
//	// 以下几个都是对右值的右值引用
//	int&& rr1 = 10;
//	double&& rr2 = x + y;
//	double&& rr3 = fmin(x, y);
//	// 这里编译会报错：error C2106: “=”: 左操作数必须为左值
//	/*10 = 1;
//	x + y = 1;
//	fmin(x, y) = 1;*/
//	return 0;
//}
//
//int main()
//{
//	double x = 1.1, y = 2.2;
//	int&& rr1 = 10;
//	const double&& rr2 = x + y;
//	rr1 = 20;
//	//rr2 = 5.5;  // 报错
//	return 0;
//}


//
//左值引用总结：
//1. 左值引用只能引用左值，不能引用右值。
//2. 但是const左值引用既可引用左值，也可引用右值
//int main()
//{
//	int a = 1;
//	int& aa1 = a;
//	//int& aa2 = 1;//编译不通过
//	int&& aa2 = 1;//右值引用
//
//	const int& aa3 = 1;//见上
//
//
//}


//
//右值引用总结：
//1. 右值引用只能右值，不能引用左值。
//2. 但是右值引用可以move以后的左值。
//int main()
//{
//	int a1 = 1;
//	int a2 = 2;
//	int&& aa1 = move(a1);
//	int&& aa2 = 2;
//	int&& aa3 = a1 + a2;
//
//}



namespace zc
{
	class string
	{
	public:
		typedef char* iterator;
		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}

		string(const char* str = "")
			:_size(strlen(str))
			, _capacity(_size)
		{
			//cout << "string(char* str)" << endl;

			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}

		// s1.swap(s2)
		void swap(string& s)
		{
			::swap(_str, s._str);
			::swap(_size, s._size);
			::swap(_capacity, s._capacity);
		}

		// 拷贝构造
		string(const string& s)
			:_str(nullptr)
		{
			cout << "string(const string& s) -- 深拷贝" << endl;

			string tmp(s._str);
			swap(tmp);
		}

		// 移动构造
		string(string&& s)
			:_str(nullptr)
		{
			cout << "string(string&& s) -- 移动拷贝" << endl;
			swap(s);
		}

		// 赋值重载
		string& operator=(const string& s)
		{
			cout << "string& operator=(string s) -- 深拷贝" << endl;
			string tmp(s);
			swap(tmp);

			return *this;
		}

		~string()
		{
			delete[] _str;
			_str = nullptr;
		}

		char& operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];
		}

		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;

				_capacity = n;
			}
		}

		void push_back(char ch)
		{
			if (_size >= _capacity)
			{
				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(newcapacity);
			}

			_str[_size] = ch;
			++_size;
			_str[_size] = '\0';
		}

		//string operator+=(char ch)
		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}

		string operator+(char ch)
		{
			string tmp(*this);
			tmp += ch;
			return tmp;
		}

		const char* c_str() const
		{
			return _str;
		}
	private:
		char* _str;
		size_t _size;
		size_t _capacity; // 不包含最后做标识的\0
	};

	zc::string to_string(int value)
	{
		bool flag = true;
		if (value < 0)
		{
			flag = false;
			value = 0 - value;
		}

		zc::string str;
		while (value > 0)
		{
			int x = value % 10;
			value /= 10;

			str += ('0' + x);
		}

		if (flag == false)
		{
			str += '-';
		}

		std::reverse(str.begin(), str.end());
		return str;
	}
}

//int main()
//{
//	zc::string s1("hello world");
//
//	zc::string ret1 = s1;
//	zc::string ret2 = (s1+'!');
//
//	zc::string ret3 = move(s1);
//
//	return 0;
//}

//// 左值引用：直接减少拷贝。1、左值引用传参  2、传引用返回。（函数内的局部对象，不能用引用返回）
//
//int main()
//{
//	zc::string valStr = zc::to_string(1234);
//	cout << valStr.c_str() << endl;
//
//	std::string s1("hello");
//
//	/*std::string s2 = s1;
//	std::string s3 = move(s1);*/
//
//	move(s1);
//	std::string s3 = s1;
//
//	return 0;
//}
//
//C++11中编译器会直接将传值返回识别成一个右值，然后调用移动构造
//int main()
//{
//	list<zc::string> lt;
//
//	zc::string s1("hello world");
//	lt.push_back(s1);
//	lt.push_back(move(s1));
//
//	lt.push_back(zc::string("hello world"));
//	lt.push_back("hello world");
//
//	return 0;
//}

//
//
//void Fun(int& x) { cout << "左值引用" << endl; }
//void Fun(const int& x) { cout << "const 左值引用" << endl; }
//
//void Fun(int&& x) { cout << "右值引用" << endl; }
//void Fun(const int&& x) { cout << "const 右值引用" << endl; }
//
//
//
//// 万能引用(引用折叠)：既可以引用左值，也可以引用右值
////完美转发
//template<typename T>
//void PerfectForward(T&& t)
//{
//	Fun(forward<T>(t));
//}
//
//int main()
//{
//	PerfectForward(10);           // 右值
//
//	int a;
//	PerfectForward(a);            // 左值
//	PerfectForward(std::move(a)); // 右值
//
//	const int b = 8;
//	PerfectForward(b);		      // const 左值
//	PerfectForward(std::move(b)); // const 右值
//
//	/*int&& rr1 = 10;
//	cout << &rr1 << endl;
//	rr1++;*/
//
//	/*string s1("hello world");
//	string s2("hello world");
//	string s3 = s1 + s2;*/
//
//	return 0;
//}
//
////#include "List.h"
////
////int main()
////{
////	bit::list<bit::string> lt;
////
////	bit::string s1("hello world");
////	lt.push_back(s1);
////
////	lt.push_back(bit::string("hello world"));
////	lt.push_back("hello world");
////
////	return 0;
////}

//
//class Person
//{
//public:
//	Person(const char* name = "", int age = 0)
//		:_name(name)
//		, _age(age)
//	{}
//	Person(const Person& p) = delete;
//private:
//	zc::string _name;
//	int _age;
//};
//int main()
//{
//	Person s1;
//	Person s2 = s1;
//	Person s3 = std::move(s1);
//	return 0;
//}