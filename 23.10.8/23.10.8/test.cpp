#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<vector>
#include<string>
#include<time.h>
using namespace std;

//只能在堆上开辟空间的类

//方法一
//class HeapOnly
//{
//public:
//	void Destroy()
//	{
//		delete this;
//	}
//
//private:
//	~HeapOnly()
//	{
//		cout << "~HeapOnly()" << endl;
//	}
//
//	int _x;
//};
//int main()
//{
//	/*HeapOnly ho1;
//	static HeapOnly ho2;*/
//
//	HeapOnly* pho3 = new HeapOnly;
//	pho3->Destroy();
//
//	return 0;
//}


//方法二
//class HeapOnly
//{
//public:
//	static HeapOnly* CreateObj(int x = 0)
//	{
//		HeapOnly* p = new HeapOnly(x);
//		return p;
//	}
//private:
//	HeapOnly(int x=0)
//		:_x(x)
//	{}
//
//	HeapOnly(const HeapOnly& hp) = delete;
//	HeapOnly& operator=(const HeapOnly& hp) = delete;
//	int _x;
//};
//
//
//int main()
//{
//	/*HeapOnly ho1;
//	static HeapOnly ho2;
//	HeapOnly* pho3 = new HeapOnly;*/
//
//	HeapOnly* p1 = HeapOnly::CreateObj(1);
//	return 0;
//}


//////////////////////////////////////////

//只能在栈上开辟的类
//class StackOnly
//{
//public:
//	static StackOnly CreateObj(int x = 0)
//	{
//		return StackOnly(x);
//	}
//
//	StackOnly(StackOnly&& st)
//		:_x(st._x)
//	{}
//
//private:
//	StackOnly(int x = 0)
//		:_x(x)
//	{}
//
//	StackOnly(const StackOnly& st) = delete;
//
//	int _x;
//
//};
//
//int main()
//{
//	/*StackOnly st1;
//	static StackOnly st2;
//	StackOnly* st3 = new StackOnly;*/
//
//	StackOnly st1 = StackOnly::CreateObj(1);
//	//static StackOnly st2 = st1;
//	//static StackOnly st2 = move(st1);
//
//	return 0;
//}


/////////////////////////////////////////////////

//单例模式
// 饿汉模式
//class Singleton
//{
//public:
//	static Singleton* GetInstance()
//	{
//		cout << _spInst << endl;
//		return _spInst;
//	}
//
//	void Print();
//private:
//	Singleton()
//	{}
//
//	//防不住拷贝，直接将其删除
//	Singleton(const Singleton&) = delete;
//
//	static Singleton _sInst; //声明
//	static Singleton* _spInst; //声明
//
//	//饿汉模式初始化一个成员
//	int _a = 0;
//};
//
//void Singleton::Print()
//{
//	cout << _a << endl;
//}
//
//Singleton Singleton::_sInst; //定义
//Singleton* Singleton::_spInst = new Singleton; //定义
//
//int main()
//{
//	//GetInstance() 可以或者这个Singleton类的单例对象
//	Singleton::GetInstance()->Print();
//
//	//在外面就定义不出来对象了
//	//Singleton st1;
//	//Singleton* st2 = new Singleton;
//
//	//拷贝删除掉
//	//Singleton copy(*Singleton::GetInstance());
//
//	return 0;
//}

//
//
////懒汉模式
//class InfoMgr
//{
//public:
//	static InfoMgr* GetInstance()
//	{
//		//还需要加锁，这个后面讲  -- 双检查加锁
//		if (_spInst == nullptr)
//		{
//			_spInst = new InfoMgr;
//		}
//
//		return _spInst;
//	}
//
//	void SetAddress(const string& s)
//	{
//		_address = s;
//	}
//
//	string& GetAddress()
//	{
//		return _address;
//	}
//
//	//实现一个内嵌垃圾回收类    
//	class CGarbo
//	{
//	public:
//		~CGarbo()
//		{
//			if (_spInst)
//			{
//				delete _spInst;
//			}
//		}
//	};
//
//	//定义一个静态成员变量，程序结束时，系统会自动调用它的析构函数从而释放单例对象
//	static CGarbo Garbo;
//
//private:
//	InfoMgr()
//	{}
//
//	~InfoMgr()
//	{
//		//假设析构时需要信息写到文件持久化，目的是单例对象销毁时要做一些必不可少的动作
//	}
//
//	InfoMgr(const InfoMgr&) = delete;
//
//	string _address;
//	int _secretKey;
//
//	static InfoMgr* _spInst; //声明
//};
//
////先初始化成空
//InfoMgr* InfoMgr::_spInst = nullptr; //定义
//InfoMgr::CGarbo Garbo;
//
//int main()
//{
//	//全局只有一个InfoMgr对象
//	InfoMgr::GetInstance()->SetAddress("122333");
//
//	cout << InfoMgr::GetInstance()->GetAddress() << endl;
//
//	return 0;
//}

#include<thread>
#include<mutex>
class Singleton
{
public:
	static Singleton* GetInstance()
	{
		// C++11之前，这里不能保证初始化静态对象的线程安全问题
		// C++11之后，这里可以保证初始化静态对象的线程安全问题
		static Singleton inst;

		return &inst;
	}

	void Add(const string& str)
	{
		_vmtx.lock();

		_v.push_back(str);

		_vmtx.unlock();
	}

	void Print()
	{
		_vmtx.lock();

		for (auto& e : _v)
		{
			cout << e << endl;
		}
		cout << endl;

		_vmtx.unlock();
	}

	~Singleton()
	{
		// 持久化
		// 比如要求程序结束时，将数据写到文件，单例对象析构时持久化就比较好
	}

private:
	// 限制类外面随意创建对象
	Singleton()
	{
		cout << "Singleton()" << endl;
	}

	// 防拷贝
	Singleton(const Singleton& s) = delete;
	Singleton& operator=(const Singleton& s) = delete;

private:
	mutex _vmtx;
	vector<string> _v;
};


//int main()
//{
//	Singleton::GetInstance();
//	Singleton::GetInstance();
//
//
//	return 0;
//}