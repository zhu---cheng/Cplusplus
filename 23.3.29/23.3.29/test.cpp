#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
using namespace std;

////另类加法
//class UnusualAdd {
//public:
//	int addAB(int A, int B) {
//		if (A == 0) return B;
//		if (B == 0) return A;
//		int a = A ^ B;//求和后当前位的数据
//		int b = (A & B) << 1;//求和后进位的数据
//		return addAB(a, b);//递归两个数进行相加，任意为0时截止
//	}
//};


//多少条路径
//#include<iostream>
//using namespace std;
//int pathNum(int n, int m)
//{
//	if (n > 1 && m > 1)
//		//b情况，递归
//		return pathNum(n - 1, m) + pathNum(n, m - 1);
//	else if (((n >= 1) && (m == 1)) || ((n == 1) && (m >= 1)))
//		// a情况，终止条件
//		return n + m;
//	else
//		//格子为0时， 路径为0
//		return 0;
//} int main()
//{
//	int n, m;
//	while (cin >> n >> m)
//	{
//		cout << pathNum(n, m) << endl;
//	} return 0;
//}
//



//删除有序数组重复项
//class Solution {
//public:
//    int removeDuplicates(vector<int>& nums) {
//        if (nums.size() < 2)
//            return 1;
//        int i = 0; int j = 0;
//
//        for (i = 1; i < nums.size(); i++)
//        {
//            if (nums[j] != nums[i])
//            {
//                nums[++j] = nums[i];
//
//            }
//        }
//
//        return ++j;
//    }
//};


//超过一半的数字
//class Solution {
//public:
//    int MoreThanHalfNum_Solution(vector<int> numbers) {
//        sort(numbers.begin(), numbers.end());
//        int max = 0;
//        int i = 0; int j = 1;
//        while (j < numbers.size())
//        {
//            if (numbers[i] != numbers[j])
//            {
//                i = j;
//                j++;
//
//            }
//            else if (numbers[i] == numbers[j])
//            {
//                if (j - i > max)
//                {
//                    max = j;
//                }
//
//                j++;
//            }
//        }
//        return numbers[max];
//    }
//};


//int globalVar = 1;
//static int staticGlobalVar = 1;
//void Test()
//{
//	static int staticVar = 1;
//	int localVar = 1;
//	int num1[10] = { 1, 2, 3, 4 };
//	char char2[] = "abcd";
//	const char* pChar3 = "abcd";
//	int* ptr1 = (int*)malloc(sizeof(int) * 4);
//	int* ptr2 = (int*)calloc(4, sizeof(int));
//	int* ptr3 = (int*)realloc(ptr2, sizeof(int) * 4);
//	free(ptr1);
//	free(ptr3);
//}

//
//void Test()
//{
//	// 动态申请一个int类型的空间
//	int* ptr4 = new int;
//
//	// 动态申请一个int类型的空间并初始化为10
//	int* ptr5 = new int(10);
//
//	// 动态申请10个int类型的空间
//	int* ptr6 = new int[3];
//	delete ptr4;
//	delete ptr5;
//	delete[] ptr6;
//}




class A
{
public:
	A(int a = 0)
		: _a(a)
	{
		cout << "A():" << this << endl;
	}
	~A()
	{
		cout << "~A():" << this << endl;
	}
private:
	int _a;
};
//int main()
//{/*
//		A* p1 = (A*)malloc(sizeof(A));
//	A* p2 = new A(1);
//	free(p1);
//	delete p2;*/
//	//// 内置类型是几乎是一样的
//	//int* p3 = (int*)malloc(sizeof(int)); // C
//	//int* p4 = new int;
//	//free(p3);
//	//delete p4;
//	A* p5 = (A*)malloc(sizeof(A) * 10);
//	A* p6 = new A[10];
//	free(p5);
//	delete[] p6;
//	return 0;
//}



class Stack
{
public:
	Stack()
	{
		cout << "Stack()" << endl;
		_a = new int[4];
		top = 0;
		capacity = 4;
	}

	~Stack()
	{
		cout << "~Stack()" << endl;
		delete[]_a;
		top = 0;
		capacity = 0;
	}

private:
	int* _a;
	int top;
	int capacity;
};
//
//int main()
//{
//		 //失败了抛异常
//	int* p1 = (int*)operator new(sizeof(int*));
//
//	// 失败返回nullptr
//	int* p2 = (int*)malloc(sizeof(int*));
//	if (p2 == nullptr)
//	{
//		perror("malloc fail");
//	}
//
//	 //申请空间 operator new -> 封装malloc
//	 //调用构造函数
//	A* p5 = new A;
//
//	// 先调用析构函数
//	// 再operator delete p5指向的空间
//	// operator delete -> free
//	delete p5;
//
//	// 申请空间 operator new[] ->perator new-> 封装malloc
//	// 调用10次构造函数
//	A* p6 = new A[10];
//	
//	// 先调用10次析构函数
//	// 再operator delete[] p6指向的空间
//	delete[] p6;
//
//
//	int* p7 = new int[10];
//	free(p7);  // 正常释放
//
//	A* p8 = new A;
//	//free(p8); // 少调用的析构函数
//	delete p8;
//	//对于无空间开辟的少调用析构函数可以正常运行
//
//
//	//Stack st;
//
//	Stack* pts = new Stack;
//	//free(pts);//这样就少调用了析构函数，会造成内存泄漏
//	delete pts;
//
//
//	return 0;
//}
#include<iostream>
using namespace std;
int guardcode(const string& code)
{
    int score = 0;
    int flag = 0;
    int count1 = 0, count2 = 0, count3 = 0, count4 = 0;
    if (code.size() <= 4)
        score += 5;
    if (code.size() > 4 && code.size() < 8)
        score += 10;
    if (code.size() >= 8)
        score += 25;

    for (auto ch : code)
    {
        if (ch <= 'z' && ch >= 'a')
        {
            count1++;
        }
        if (ch <= 'Z' && ch >= 'A')
        {
            count2++;
        }
    }
    if (count1 == code.size())
    {
        score += 10;
    }
    if (count2 == code.size())
    {
        score += 10;
    }
    if (count1 != code.size() && count2 != code.size() && count1 != 0 && count2 != 0)
    {
        score += 20;
    }


    for (auto ch : code)
    {
        if (ch >= '0' && ch <= '9')
            count3++;
    }
    if (count3 == 1)
        score += 10;
    if (count3 > 1)
        score += 20;

    for (auto ch : code)
    {
        if (ch >= 0x21 && ch <= 0x2F || ch <= 0x40 && ch >= 0x3A || ch <= 0x60 && ch >= 0x5B || ch <= 0x7E && ch >= 0x7B) {
            count4++;
        }
    }
    if (count4 == 1)
        score += 10;
    if (count4 > 1)
        score += 25;

    if ((count1 != 0 || count2 != 0) && count3 != 0 && count4 == 0)
        score += 2;
    if ((count1 != 0 || count2 != 0) && count3 != 0 && count4 != 0)
        score += 3;
    if (count1 != 0 && count2 != 0 && count3 != 0 && count4 == 0)
        score += 5;


    return score;

}
int main()
{
    string code;
    cin >> code;
    int result = guardcode(code);
    if (result >= 90) {
        cout << "VERY_SECURE";
    }
    else if (result >= 80) {
        cout << "SECURE";
    }
    else if (result >= 70) {
        cout << "VERY_STRONG";
    }
    else if (result >= 60) {
        cout << "STRONG";
    }
    else if (result >= 50) {
        cout << "AVERAGE";
    }
    else if (result >= 25) {
        cout << "WEAK";
    }
    else {
        cout << "VERY_WEAK";
    }

}