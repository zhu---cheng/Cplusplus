#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<vector>
#include<list>
#include"stack.h"
#include"queue.h"
int main()
{
	//zc::test_stack();
	zc::test_queue();


	cout << endl;


	//优先级队列
	//priority_queue<int> v1;//默认降序
	priority_queue<int, vector<int>, greater<int>>v1;//仿函数/函数对象   升序
	v1.push(1);
	v1.push(6);
	v1.push(2);
	v1.push(3);
	v1.push(9);
	v1.push(7);
	v1.push(5);
	v1.push(4);


	while (!v1.empty())
	{
		cout << v1.top() << " ";
		v1.pop();
}

	cout << endl;
	return 0;
}

//仿函数实现就是一个类中有（）的运算符重载，然后类声明的对象可以调用函数
//template<class T>
//struct Less
//{
//	bool operator()(const T& x, const T& y)
//	{
//		return x < y;
//	}
//};
//
//int main()
//{
//	//test_stack();
//	//bit::test_queue();
//	//test_priority_queue();
//
//	Less<int> lessFunc;
//	cout << lessFunc(1, 2) << endl;
//
//	void(*ptr1)();
//
//	return 0;
//}