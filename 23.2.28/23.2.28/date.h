#pragma once
#include<iostream>
#include<assert.h>
using namespace std;

class Date
{
	friend ostream& operator<<(ostream& out, const Date& d);
	friend istream& operator>>(istream& in, Date& d);
//友元
public:
	Date(int year = 1900, int month = 1, int day = 1);
	void Print();
	int GetMonthDay(int year, int month);

	bool operator==(const Date& d) const;
	bool operator!=(const Date& d) const;
	bool operator<(const Date& d) const;
	bool operator<=(const Date& d) const;
	bool operator>(const Date& d) const;
	bool operator>=(const Date& d) const;

	Date& operator+=(int day) ;
	Date operator+(int day) ;
	Date& operator-=(int day) ;

	// d1 - 100
	Date operator-(int day);

	// d1 - d2;
	int operator-(const Date& d) const;

	// ++d1
	Date& operator++();

	// d1++
	// int参数 仅仅是为了占位，跟前置重载区分

	Date operator++(int);


	Date& operator--();
	Date operator--(int);



private:
	int _year;
	int _month;
	int _day;
};

//cout在ostream库类型中，out是cout的引用
inline ostream& operator<<(ostream& out, const Date& d)
{
	out << d._year << "年" << d._month << "月" << d._day << "日" << endl;
	return out;
}

inline istream& operator>>(istream& in, Date& d)
{
	in >> d._year >> d._month >> d._day;
	return in;
}
//直接写在。h里，如果内联函数写在data.cpp中链接不到会报错，在.h里不需要链接直接调用
//访问不到私有函数可以使用友元