#define _CRT_SECURE_NO_WARNINGS 1


#include"date.h"

int Date::GetMonthDay(int year, int month)
{
	assert(month > 0 && month < 13);
	int MonthArray[13] = { 0, 31,28,31,30,31,30,31,31,30,31,30,31 };

	if (month == 2)
	{
		if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
			return 29;
	}

	else
		return MonthArray[month];
}


Date::Date(int year, int month, int day)
{


	_year = year;
	_month = month;
	_day = day;


}


void Date::Print()
{
	cout << _year << "-" << _month << "-" << _day << endl;
}



bool Date::operator==(const Date& d) const
{
	return _year == d._year
		&& _month == d._month
		&& _day == d._day;
}

bool Date::operator!=(const Date& d) const
{
	return !(*this == d);
}


bool Date::operator<(const Date& d) const
{
	if (_year < d._year)
		return true;
	if (_year == d._year && _month < d._month)
		return true;
	if (_year == d._year && _month == d._month && _day < d._day)
		return true;

	else
		return false;
}

bool Date::operator<=(const Date& d) const
{
	return (*this < d) || (*this == d);
}


bool Date::operator>(const Date& d) const
{
	return !((*this) <= d);
}

bool Date::operator>=(const Date& d) const
{
	return !(*this < d);
}

Date& Date::operator+=(int day) 
{
	if (day < 0)
	{
		*this -= -day;
		return *this;
	}
	_day += day;
	while (_day > GetMonthDay(_year, _month))
	{
		_day -= GetMonthDay(_year, _month);
		_month++;

		if (_month == 13)
		{
			_year++;
			_month = 1;
		}
	}

	return *this;
}


Date Date::operator+(int day) 
{
	Date tmp(*this);
	tmp += day;

	return tmp;
}


// Date Date::operator+(int day)
//{
//	Date tmp(*this);
//
//	tmp._day += day;
//	while (tmp._day > GetMonthDay(tmp._year, tmp._month))
//	{
//		tmp._day -= GetMonthDay(tmp._year, tmp._month);
//		tmp._month++;
//		if (tmp._month == 13)
//		{
//			++tmp._year;
//			tmp._month = 1;
//		}
//	}
//
//	return tmp;
//}
//
//// d1 += 100
//Date& Date::operator+=(int day)
//{
//	*this = *this + day;
//
//	return *this;
//}



Date& Date::operator++() 
{
	*this += 1;
	return *this;
}


Date Date::operator++(int) //这里的int没作用，只是和上面的++a区别
{
	Date tmp(*this);
	*this += 1;
	return tmp;
}

Date& Date::operator-=(int day) 
{
	if (day < 0)
	{
		*this += -day;
		return *this;
	}
	_day -= day;
	while (_day <= 0)
	{
		--_month;
		if (_month == 0)
		{
			--_year;
			_month = 12;
		}
		_day += GetMonthDay(_year, _month);
	}

	return *this;
}

Date Date::operator-(int day) 
{
	Date tmp(*this);
	tmp -= day;
	return tmp;
}


Date& Date::operator--() 
{
	*this -= 1;
	return *this;
}


Date Date::operator--(int) 
{
	Date tmp(*this);
	*this -= 1;
	return tmp;
}



int Date::operator-(const Date& d) const
{
	Date max = *this;
	Date min = d;
	int flag = 1;

	if (*this < d)
	{
		max = d;
		min = *this;
		flag = -1;
	}

	int n=0;
	while (max != min)
	{
		++min;
		++n;
	}


	return n * flag;
}


