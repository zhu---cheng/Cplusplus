#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//int main()
//{
//	int a = 0;
//	int& b = a;
//	int& c = a;
//
//	cout << &a << endl;
//	cout << &b << endl;
//	cout << &c << endl;
//	//引用就是变量的另一个别名，地址是一样的
//
//	int x = 0;
//	c = x;//赋值是不一样的
//	cout << &x << endl;
//	cout << &c << endl;
//
//
//}


/////////////////////////////////////////////////////////
//引用可以作为形参
//这样形参的改变可以改变实参

//void swap(int* a, int* b)
//{
//	int tmp = *a;
//	*a = *b;
//	*b = tmp;
//}
//
//void swap(int& a, int& b)
//{
//	int tmp = a;
//	a = b;
//	b = tmp;
//}
//
//
//int main()
//{
//	int a = 1;
//	int b = 2;
//	swap(&a, &b);
//	cout << a << endl;
//	cout << b << endl;
//	
//
//	swap(a,b);//函数重载
//	cout << a << endl;
//	cout << b << endl;
//}



//// 通过前序遍历的数组"ABD##E#H##CF##G##"构建二叉树
////BTNode* BinaryTreeCreate(char* a, int* pi)  // c
////BTNode* BinaryTreeCreate(char* a, int& ri) // cpp
////{}
//
//typedef struct SeqList
//{
//	int* a;
//	int size;
//	int capaicty;
//}SL;
//
//void SLPushBack(SL& sl, int x);
//
//typedef struct Node
//{
//	struct Node* next;
//	int val;
//}Node, *PNode;
//
////typedef struct Node* PNode;
////void SLTPushBack(Node** pphead, int x);  // C
//
////void SLTPushBack(Node*& phead, int x);  // CPP
//void SLTPushBack(PNode& phead, int x);
//
//int main()
//{
//	int a = 0, b = 1;
//	swap(&a, &b);
//	swap(a, b);
//
//	char* ptr = "ABD##E#H##CF##G##";
//	int i = 0;
//	//BinaryTreeCreate(ptr, i);
//
//	Node* plist = NULL;
//	//SLTPushBack(&plist, 1);
//	SLTPushBack(plist, 1);
//
//	return 0;
//}

///////////////////////////////////////////////////
//2.做返回值

//int count()
//{
//	static int n = 0;
//	n++;
//	return n;
//}

//这里的n先给一个临时变量，然后才返回给到ret上

//int& count()
//{
//	static int n = 0;
//	n++;
//	return n;
//}
////相当于直接给到寄存器然后给到ret
////也可以理解为n的是ret的别名，可以直接改变ret，效率提升
//
//
//int main()
//{
//	const int& ret = count();//可以范围缩小
//	//取别名权限缩小
//	cout << ret << endl;
//}

//struct A
//{
//	int a[10000];
//};
//
//void TestFunc1(A a)
//{
//
//}
//
//void TestFunc2(A& a)
//{
//
//}
//
//void TestRefAndValue()
//{
//	A a;
//
//	//以值作为函数参数
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 10000; i++)
//	{
//		TestFunc1(a);
//	}
//	size_t end1 = clock();
//
//	//以引用作为函数参数
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 10000; i++)
//	{
//		TestFunc2(a);
//	}
//	size_t end2 = clock();
//
//	//分别计算两个函数运行结束后的时间
//	cout << "TestFunc1(A)-time:" << end1 - begin1 << endl;
//	cout << "TestFunc2(A&)-time:" << end2 - begin2 << endl;
//}
//
//int main()
//{
//	TestRefAndValue();
//
//	return 0;
//}


//如果函数返回时，出了函数作用域，如果返回对象还未还给系统，则可以使用引用返回，如果已经还给系统了，则必须使用传值返回
//int& Add(int a, int b)
//{
//	int c = a + b;
//	return c;
//}
//int main()
//{
//	int& ret = Add(1, 2);
//	Add(3, 4);
//	cout << "Add(1, 2) is :" << ret << endl;
//	cout << "Add(1, 2) is :" << ret << endl;
//	cout << "Add(1, 2) is :" << ret << endl;
//	return 0;
//}


//int& Count()
//{
//	//static int n = 0;
//	int n = 0;
//	n++;
//
//	return n;
//}
//
//int main()
//{
//	int& ret = Count();
//
//	cout << ret << endl;
//	cout << ret << endl;
//	cout << ret << endl;
//
//	return 0;
//}
//



//
//#include<iostream>
//using namespace std;
//
//#include <time.h>
//struct A { int a[10000]; };
//A a;
//
//// 值返回
//A TestFunc1() { return a; }
//
//// 引用返回
//A& TestFunc2() { return a; }
//
//void TestReturnByRefOrValue()
//{
//	// 以值作为函数的返回值类型
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc1();
//	size_t end1 = clock();
//
//	// 以引用作为函数的返回值类型
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc2();
//	size_t end2 = clock();
//
//	// 计算两个函数运算完成之后的时间
//	cout << "TestFunc1 time:" << end1 - begin1 << endl;
//	cout << "TestFunc2 time:" << end2 - begin2 << endl;
//}
//
//int main()
//{
//	TestReturnByRefOrValue();
//
//	return 0;
//}
//因为值返回的是需要拷贝的，而引用返回是不需要拷贝的
//当返回的数据占的空间很大时，引用返回的优势就显而易见了








// C++推荐
// const和enum替代宏常量
// inline去替代宏函数

// 宏缺点：
// 1、不能调试
// 2、没有类型安全的检查
// 3、有些场景下非常复杂,容易出错，不容易掌握

// 要求实现ADD宏函数
//#define ADD(a,b) ((a)+(b))
//int main()
//{
//	int ret=ADD(1, 2);//整体代换
//	cout << ret << endl;
//	return 0;
//}

//（问题） 频繁调用短小的函数，频繁创建栈帧效率不高
//内联函数会在调用的地方展开，适用于频繁调用的短小函数
//（重点） 如果在上述函数前增加inline关键字将其改成内联函数，在编译期间编译器会用 函数体替换函数的调用

//inline  内联函数 
//吊用的时候不用申请栈帧
//
//inline是一种以空间换时间的做法，省去调用函数额开销。所以代码很长或者有循环 / 递归的函数不适宜使用作为内联函数。
//
//inline对于编译器而言只是一个建议，编译器会自动优化，如果定义为inline的函数体内有循环 / 递归等，编译器优化时会忽略掉内联。
//
//inline不建议声明和定义分离，分离会导致链接错误。因为inline被展开，就没有函数地址了，链接就会找不到。
//
//递归inline的话就无穷无尽了，长的代码一般超过十行就不要展，具体取决于编译器，inline对编译器是一种建议
//inline int Add(int x, int y)
//{
//	int z = x + y;
//	return z;
//}
//
//int main()
//{
//	Add(1, 2);
//
//	return 0;
//}


//
//int& TestAuto()
//{
//	int a = 0;
//	return a;
//}
//int main()
//{
//	const int a = 10;
//	auto b = &a;
//	auto c = 'a';
//	const auto& d = TestAuto();
//	int x = 1;
//	auto* m = &x;//指定类型是一种指针的类型，必须赋值是一种指针
//	//typeid - 可以看一个变量的类型
//
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	cout << typeid(d).name() << endl;
//	cout << typeid(m).name() << endl;
//
//	//auto e; //无法通过编译，使用auto定义变量时必须对其进行初始化
//	return 0;
//}


//typedef char* pstring;
//int main()
//{
//	const pstring p1; // 编译成功还是失败？
//	const pstring* p2; // 编译成功还是失败？
//	return 0;
//}




int main()
{
	int array[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	//遍历数组
	//C语言的方式
	for (int i = 0; i < sizeof(array) / sizeof(array[0]); i++)
	{
		array[i] *= 2;
	}

	for (int i = 0; i < sizeof(array) / sizeof(array[0]); i++)
	{
		cout << array[i] << " ";
	}
	cout << endl;

	for (auto e : array)//e是数组中每个值的拷贝，想改改变的话就用引用用别名
	{
		e /= 2;
	}

	//C++的方式
	//C++11中的范围for
	for (auto e : array)
	{
		cout << e << " ";

	}

	cout << endl;

	return 0;
}
