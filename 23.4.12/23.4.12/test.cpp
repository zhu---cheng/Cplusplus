#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//class Teacher
//{
//	string _name;
//	string _tel;
//	string _address;
//	int _age;
//	string _workId;//工号
//};
//
//class Student
//{
//	string _name;
//	string _tel;
//	string _address;
//	int _age;
//	string _stuId;//学号
//};
//
//class Person
//{
//public:
//	void Print()
//	{
//		cout << "name:" << _name << endl;
//		cout << "age:" << _age << endl;
//	}
//protected:
//	string _name = "peter"; // 姓名
//	int _age = 18;
//};
//// 继承后父类的Person的成员（成员函数+成员变量）都会变成子类的一部分。这里体现出了
////Student和Teacher复用了Person的成员。下面我们使用监视窗口查看Student和Teacher对象，可
////以看到变量的复用。调用Print可以看到成员函数的复用。
//class Student : public Person
//{
//protected:
//	int _stuid; // 学号
//};
//
//class Teacher : public Person
//{
//protected:
//	int _jobid; // 工号
//};
//int main()
//{
//	Student s;
//	Teacher t;
//	s.Print();
//	t.Print();
//	return 0;
//}


//
//class A
//
//{
//
//public:
//
//    virtual void f()
//
//    {
//
//        cout << "A::f()" << endl;
//
//    }
//
//};
//
//
//
//class B : public A
//
//{
//
//private:
//
//    virtual void f()
//
//    {
//
//        cout << "B::f()" << endl;
//
//    }
//
//};
//
//int main()
//{
//
//    A* pa = (A*)new B;
//    pa->f();
//}




////打印虚表
//class Base
//{
//public:
//	virtual void func1()
//	{
//		cout << "Base::func1()" << endl;
//	}
//
//	virtual void func2()
//	{
//		cout << "Base::func2()" << endl;
//	}
//
//	void func3()
//	{
//		cout << "Base::func3()" << endl;
//	}
//private:
//	int _b=1;
//};
//
//class Derive :public Base
//{
//	virtual void func1()
//	{
//		cout << "Derive::func1()" << endl;
//	}
//
//	virtual void func4()
//	{
//		cout << "Derive::func4()" << endl;
//	}
//
//private:
//	int _b = 10;
//};
//
//
//typedef void(*VF_PTR)();
////void PrintVFTable(VF_PTR table[])
//void PrintVFTable(VF_PTR* table)
//{
//	for (int i = 0; table[i] != nullptr; i++)
//	{
//		printf("[%d]:%p->", i, table[i]);
//		VF_PTR f = table[i];
//		f();
//	}
//
//	cout << endl;
//}
//
//int main()
//{
//	Base b;
//	Derive d;
//
//	PrintVFTable((VF_PTR*)(*(int*)&b));
//	PrintVFTable((VF_PTR*)(*(int*)&d));
//	return 0;
//}

//
//class Base
//{
//public:
//	Base()
//		:_b(10)
//	{
//		++_b;
//	}
//
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//
//private:
//	int _b = 1;
//};
//
//class Derive : public Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Derive::Func1()" << endl;
//	}
//
//	virtual void Func4()
//	{
//		cout << "Derive::Func4()" << endl;
//	}
//private:
//	int _d = 2;
//};
//
//// 用程序打印虚表
//typedef void(*VF_PTR)();
//
////void PrintVFTable(VF_PTR table[])
//void PrintVFTable(VF_PTR* table)
//{
//	for (int i = 0; table[i] != nullptr; ++i)
//	{
//		printf("[%d]:%p->", i, table[i]);
//		VF_PTR f = table[i];
//		f();
//	}
//	cout << endl;
//}
//
//int main()
//{
//	Base b;
//	Derive d;
//
//	//PrintVFTable((VF_PTR*)(*(int*)&b));
//	//PrintVFTable((VF_PTR*)(*(int*)&d));
//
//	// 虚表是什么阶段生成的？          -- 编译
//	// 对象中虚表指针什么时候初始化的？ -- 构造函数的初始化列表
//	// 虚表存在哪里？
//
//	/*PrintVFTable((VF_PTR*)(*(long long*)&b));
//	PrintVFTable((VF_PTR*)(*(long long*)&d));*/
//
//	PrintVFTable((*(VF_PTR**)&b));
//	PrintVFTable((*(VF_PTR**)&d));
//
//	return 0;
//}

//
//
//class Person
//{
//protected:
//	//private:
//
//	string _name; //姓名
//	string _tel; //电话
//	string _address; //地址
//};
//
//class Student : public Person
//{
//protected:
//
//	int _age;
//	string _stuId;
//};
//
//int main()
//{
//	Student s;
//	cout << sizeof(s) << endl;
//
//	return 0;
//}


//
//class Person
//{
//public:
//	string _name; //姓名
//	string _tel;  //电话
//	string _address; //地址
//
//	int _age; //年龄
//};
//
//class Student : public Person
//{
//	string _stuId;
//};
//
//class Teacher : public Person
//{
//	string _workId;
//};
//
//int main()
//{
//	Person p;
//	Student s;
//	s._name = "张三";
//	s._tel = "12345";
//	s._address = "学校";
//
//	p = s;
//	Person& rp = s;
//	Person* ptr = &s;
//
//	cout << &s << endl;
//	cout << &rp << endl;
//	cout << ptr << endl;
//
//	return 0;
//}



//
//class Person
//{
//protected:
//	string _name = "小李子";
//	int _num = 111;
//};
//
//class Student : public Person
//{
//public:
//	void Print()
//	{
//		cout << "姓名:" << _name << endl;
//		cout << "子类的身份证号:" << _num << endl;
//
//		//告诉编译器直接去父类中找
//		cout << "父亲类的身份证号:" << Person::_num << endl;
//
//	}
//protected:
//	int _num = 999;
//};
//
//int main()
//{
//	Student s1;
//	s1.Print();
//
//	return 0;
//};





//成员函数同名 -- 两个func的关系是隐藏
class A
{
public:
	void func()
	{
		cout << "A::func()" << endl;
	}
};

class B : public A
{
public:
	void func()
	{
		cout << "B::func()" << endl;
	}
};

int main()
{
	B b;
	b.func();

	//指定作用域去调用
	b.A::func();

	return 0;
}



