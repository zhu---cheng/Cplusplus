#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>

using namespace std;

//C语言中的类型转换
void test()
{
	int i = 1;
	//隐式类型转换
	double d = i;
	printf("%d,%.2f\n", i, d);

	int* p = &i;
	//显示类型转换
	int address = (int)p;
	printf("%x,%d\n", p, address);
}

//
////static_cast用于非多态类型的转换（静态转换），编译器隐式执行的任何类型转换都可用
////static_cast，但它不能用于两个不相关的类型进行转换
//int main()
//{
//	//test();
//
//	double d = 12.34;
//	int a = static_cast<int>(d);
//	cout << a << endl;
//
//	/*int i = 10;
//	int* p = &i;
//	int address = static_cast<int>(p);*/
//	//错误，无效
//	//可以用下面的reinterpret_cast
//
//	int i = 10;
//	int* p = &i;
//	int address = reinterpret_cast<int>(p);
//	cout << address<<endl;
//
//
//	//const_cast最常用的用途就是删除变量的const属性，方便赋值
//	const int a2 = 2;
//	int* p2 = const_cast<int*>(&a2);
//	*p2 = 3;
//
//	//有没有修改到a
//	cout << a2 << endl;
//	cout << *p2 << endl;
//	//没有修改到，为什么呢？
//	/*这是编译器的优化，编译器看到是const的变量
//		绝大多数的情况const的变量是不会被修改的
//		所以访问a的时候没必要去内存中取
//		每次取会慢，所以要放在缓存当中去
//		有些会放到寄存器当中，有些是直接去取常量
//		就不会去内存中取，直接就变成2了
//		内存中修改了寄存器中却没修改*/
//
//	//我们可以加上volatile
//	volatile const int a3 = 2;
//	int* p3 = const_cast<int*>(&a3);
//	*p3 = 3;
//
//	//有没有修改到a
//	cout << a3 << endl;
//	cout << *p3 << endl;
//
//
//	return 0;
//}



//
//#include<functional>
//#include<set>
//
////dynamic_cast
//class A
//{
//public:
//	virtual void f(){}
//};
//
//class B :public A
//{
//};
////RAII 
////RTII:运行时识别
////1. typeid运算符
////2. dynamic_cast运算符
////3. decltype
//void fun(A* pa, const string& s)
//{
//	cout << "pa指向" << s << endl;
//	
//	//dynamic_cast会先检查是够能转换成功，能则转换，不能则返回
//	B* pb1 = (B*)pa;//不安全
//	B* pb2 = dynamic_cast<B*>(pa);//安全
//
//	cout << "[强制转换]pb1:" << pb1 << endl;
//	cout << "[dynamic_cast转换]pb2:" << pb2 << endl<<endl;
//}
//
//int main()
//{
//	A a;
//	B b;
//	fun(&a, "指向父类对象");
//	fun(&b, "指向子类对象");
//
//	cout << typeid(a).name() << endl;//打印类型的字符串
//	decltype(a) aa;//推导类型
//	
//	function<bool(int, int)> gt = [](int x, int y) {return x > y; };
//	set<int, decltype(gt)> s;
//	
//
//	return 0;
//}




/////////////////////////////////////////////////////////
//IO流

//C++标准库提供了4个全局流对象cin、cout、cerr、clog，使用cout进行标准输出，即数据从内
//存流向控制台(显示器)。使用cin进行标准输入即数据通过键盘输入到程序中，同时C++标准库还
//提供了cerr用来进行标准错误的输出，以及clog进行日志的输出，从上图可以看出，cout、
//cerr、clog是ostream类的三个不同的对象，因此这三个对象现在基本没有区别，只是应用场景不
//同。
//int main()
//{
//	cout << "1111111111" << endl;
//	cerr << "2222222222" << endl;
//	clog << "3333333333" << endl;
//
//
//	return 0;
//}


//
//int main()
//{
//	string str;
//
//	//ctrl+c 信号杀进程
//	//ctrl+z+换行 流对象提取到结束标志
//
//	while (cin >> str)
//	{
//		cout << str << endl;
//	}
//	//	// 凭什么istream的cin对象可以转bool
////	// 凭什么string的str对象可以转bool
////	// 因为istream的这个-》explicit operator bool() const;
////	// 支持自定义类型转内置类型
//
//
//	return 0;
//}



//class A
//{
//public:
//	A(int a)
//		:_a(a)
//	{}
//
//	operator int() const
//	{
//		return _a;
//	}
//private:
//	int _a;
//};
//
//int main()
//{
//	//内置类型转自定义类型
//	//A aa1 = static_cast<A>(1);
//	A aa1 = 1;
//	cout << aa1 << endl;
//
//	//自定义类型转内置类型
//	int x = aa1;
//	cout << x << endl;
//
//
//	return 0;
//}


class Date
{
	friend ostream& operator<<(ostream& out, const Date& d);
	friend istream& operator>>(istream& in, Date& d);
public:
	Date(int year = 1, int month = 1,int day=1)
		:_year(year),
		_month(month),
		_day(day)
	{}
	operator bool() const
	{
		//这里是随意写的，假设输入_year位0，则结束
		if (_year == 0)
			return false;
		else
			return true;
	}
private:
	int _year;
	int _month;
	int _day;
};

istream& operator>>(istream& in, Date& d)
{
	in >> d._year >> d._month >> d._day;
	return in;
}
ostream& operator<<(ostream& out, const Date& d)
{
	out << d._year << " " << d._month << " " << d._day;
	return out;
}
//
//int main()
//{
// // 自定义类型则需要我们自己重载<< 和 >>
//	Date d(2022, 4, 3);
//	cout << d;
//
//	while (d)
//	{
//		cin >> d;
//		cout << d;
//	}
//	return 0;
//}


#include<fstream>

//int main()
//{
//	ofstream ofs("test.txt", ofstream::out | ofstream::app);
//	ofs << "hello world";
//	ofs << "hello world";
//
//	return 0;
//}


struct ServerInfo
{
	char _address[32];

	int _port;
	Date _date;
};

struct ConfigManager
{
public:
	ConfigManager(const char* filename)
		:_filename(filename)
	{}
	
	//二进制读写，读写对象中，不能用string
	void WriteBin(const ServerInfo& info)
	{
		ofstream ofs(_filename, ofstream::out | ofstream::binary);
		ofs.write((char*)&info, sizeof(info));
	}

	void ReadBin(ServerInfo& info)
	{
		ifstream ifs(_filename, ofstream::in | ofstream::binary);
		ifs.read((char*)&info, sizeof(info));
	}

	// 文本读写 C++文本读写更简单
	// 文本读写本质，内存中任何类型都是转成字符串在写
	// c语言文本读写很不方便，因为要不断转字符串
	// c++封装了以后就有很大的优势

	void WriteText(const ServerInfo& info)
	{
		ofstream ofs(_filename);
		ofs << info._address << " ";
		ofs << info._port << endl;
		ofs << info._date << endl;
	}

	void ReadText(ServerInfo& info)
	{
		ifstream ifs(_filename);
		ifs >> info._address;
		ifs >> info._port;
		ifs >> info._date;
	}

private:
	string _filename;
};
//
//int main()
//{
//	ServerInfo winfo = { "192.0.0.1xxxxxxxx",80,{2023,7,11} };
//
//	string str;
//	cin >> str;
//	if (str == "二进制写")
//	{
//		ConfigManager cm("test.txt");
//		cm.WriteBin(winfo);
//	}
//
//	else if (str == "二进制读")
//	{
//		ServerInfo rinfo;
//		ConfigManager cm("test.txt");
//		cm.ReadBin(rinfo);
//		cout << rinfo._address << endl;
//		cout << rinfo._port << endl;
//		cout << rinfo._date << endl;
//	}
//
//	else if (str == "文本写")
//	{
//		ConfigManager cm("test.txt");
//		cm.WriteText(winfo);
//	}
//
//	else if (str == "文本读")
//	{
//		ServerInfo rinfo;
//		ConfigManager cm("test.txt");
//		cm.ReadText(rinfo);
//
//		cout << rinfo._address << endl;
//		cout << rinfo._port << endl;
//		cout << rinfo._date << endl;
//	}
//	return 0;
//}
//




#include<sstream>
//int main()
//{
//	//ostringstream oss;
//	stringstream oss;
//	oss << 100 << " ";
//	oss << 11.22;
//	oss << "hello";
//
//	string str = oss.str();
//	cout << str << endl;
//
//	//istringstream iss(str);
//	stringstream iss(str);
//	int i;
//	double d;
//	string s;
//	iss >> i >> d >> s;
//	cout << i << endl;
//	cout << d << endl;
//	cout << s << endl;
//
//	return 0;
//}



//序列化和反序列化
struct ChatInfo
{
	string _name;//名字
	int _id;//id
	Date _date;//时间
	string _msg;//聊天信息
};

int main()
{
	ChatInfo winfo = { "张三",125246,{2022,4,10},"晚上看电影" };
	stringstream oss;
	oss << winfo._name << " ";
	oss << winfo._id << " ";
	oss << winfo._date << " ";
	oss << winfo._msg;
	string str = oss.str();//str()获取字符串流中的字符串
	cout << str << endl;

	stringstream iss(str);
	ChatInfo rinfo;
	iss >> rinfo._name;
	iss >> rinfo._id;
	iss >> rinfo._date;
	iss >> rinfo._msg;

	cout << "-------------------------------------------------------" << endl;
	cout << "姓名：" << rinfo._name << "(" << rinfo._id << ") ";
	cout << rinfo._date << endl;
	cout << rinfo._name << ":>" << rinfo._msg << endl;
	cout << "-------------------------------------------------------" << endl;
	return 0;
}
