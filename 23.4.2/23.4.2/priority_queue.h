#pragma once

#include<iostream>
#include<list>
#include<string>
#include<queue>

using namespace std;

namespace zc
{
	//仿函数/函数对象
	template<class T>
	struct less
	{
		bool operator()(const T& x,const T& y)
		{
			return x < y;
		}
	};

	template<class T>
	struct greater
	{
		bool operator()(const T& x, const T& y)
		{
			return x > y;
		}
	};

	template<class T,class Container=vector<T>,class Compare=less<T>>
	class priority_queue
	{
	public:

		void adjust_up(int child)
		{
			int parent = (child - 1) / 2;
			Compare com;
			while (child>0)
			{
				if (com(_con[parent],_con[child]))
				{
					swap(_con[child], _con[parent]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else
				{
					break;
				}
					
			}
		}
		void adjust_down(int parent)
		{
			int child = parent * 2 + 1;

			while (child < _con.size())
			{
				Compare com;
				if (child + 1 < _con.size() && com(_con[child], _con[child + 1]))
				{
					child++;
				}

				if (com(_con[parent], _con[child]))
				{
					swap(_con[parent], _con[child]);
					parent = child;
					child = parent * 2 + 1;
				}
				else
				{
					break;
				}
			}
		
		}



		void push(const T& x)
		{
			_con.push_back(x);
			adjust_up(_con.size()-1);
		}

		const T& top()
		{
			return _con[0];
		}

		size_t size()
		{
			return _con.size();
		}
		void pop()
		{
			swap(_con[0], _con[_con.size() - 1]);
			_con.pop_back();
			adjust_down(0);
		}


		bool empty()
		{
			return _con.empty();
		}
		
	private:
		Container _con;
	};


	

	void test_priority_queue1()
	{
		priority_queue<int> pq1;
		pq1.push(1);
		pq1.push(5);
		pq1.push(4);
		pq1.push(6);
		pq1.push(9);
		pq1.push(2);
		pq1.push(8);

		while (!pq1.empty())
		{
			cout << pq1.top() << " ";
			pq1.pop();
		}
		cout << endl;

	}

	/// //////////////////////////////////////////////////////////////////
	

	class Date
	{
	public:
		Date(int year = 1900, int month = 1, int day = 1)
			: _year(year)
			, _month(month)
			, _day(day)
		{}

		bool operator<(const Date& d)const
		{
			return (_year < d._year) ||
				(_year == d._year && _month < d._month) ||
				(_year == d._year && _month == d._month && _day < d._day);
		}

		bool operator>(const Date& d)const
		{
			return (_year > d._year) ||
				(_year == d._year && _month > d._month) ||
				(_year == d._year && _month == d._month && _day > d._day);
		}

		friend ostream& operator<<(ostream& _cout, const Date& d)
		{
			_cout << d._year << "-" << d._month << "-" << d._day;
			return _cout;
		}

	private:
		int _year;
		int _month;
		int _day;
	};

	class PDateLess
	{
	public:
		bool operator()(const Date* p1, const Date* p2)
		{
			return *p1 < *p2;
		}
	};

	class PDateGreater
	{
	public:
		bool operator()(const Date* p1, const Date* p2)
		{
			return *p1 > *p2;
		}
	};


	void test_priority_queue2()
	{// 大堆，需要用户在自定义类型中提供<的重载
		priority_queue<Date> date;

		date.push(Date(2023,4,1));
		date.push(Date(2023, 4, 7));
		date.push(Date(2023, 4, 2));
		date.push(Date(2023, 4, 4));


		while (!date.empty())
		{
			cout << date.top()<< " ";
			date.pop();
		}

		cout << endl;


		priority_queue<Date*,vector<Date*>,PDateGreater> date1;

		date1.push(new Date(2023, 4, 1));
		date1.push(new Date(2023, 4, 7));
		date1.push(new Date(2023, 4, 2));
		date1.push(new Date(2023, 4, 4));
		cout << *(date1.top()) << endl;

	}


}
