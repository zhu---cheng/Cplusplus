#define _CRT_SECURE_NO_WARNINGS 1
#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<map>
#include<set>
#include<string>
using namespace std;
//template <class T1, class T2>
//struct pair
//{
//	typedef T1 first_type;
//	typedef T2 second_type;
//	T1 first;
//	T2 second;
//	pair() : first(T1()), second(T2())
//	{}
//	pair(const T1& a, const T2& b) : first(a), second(b)
//	{}
//};

void test_set1()
{
	set<int> s1;
	s1.insert(2);
	s1.insert(6);
	s1.insert(2);
	s1.insert(3);
	s1.insert(3);
	s1.insert(5);
	s1.insert(8);
	s1.insert(8);
	s1.insert(6);
	s1.insert(7);

	set<int>::iterator it = s1.begin();
	while (it != s1.end())
	{
		// 搜索树不允许修改key，可能会破坏搜索的规则
		//*it1 += 1;
		cout << *it << " ";
		++it;
	}
	cout << endl;


	for (auto e : s1)
	{
		cout << e << " ";
	}

}
void test_set2()
{
	//// 排序 + 去重
	//set<int> s1;
	//s1.insert(2);
	//s1.insert(6);
	//s1.insert(2);
	//s1.insert(3);
	//s1.insert(3);
	//s1.insert(5);
	//s1.insert(8);
	//s1.insert(8);
	//s1.insert(6);
	//s1.insert(7);
	//int x;
	//while (cin >> x)
	//{
	//	/*auto ret = s1.find(x);
	//	if (ret != s1.end())
	//		cout << "在" << endl;
	//	else
	//		cout << "不在" << endl;*/

	//	if (s1.count(x))
	//		cout << "在" << endl;
	//	else
	//		cout << "不在" << endl;
	//}



		set<int> s;
		s.insert(4);
		s.insert(5);
		s.insert(2);
		s.insert(1);
		s.insert(1);
		s.insert(3);
		s.insert(2);
		s.insert(1);

		s.erase(3);//直接给值删除

		for (auto e : s)
		{
			cout << e << " ";
		}
		cout << endl;

		int x;
		while (cin >> x)
		{
			set<int>::iterator pos = s.find(x);
			if (pos != s.end())
			{
				s.erase(pos);//迭代器删除
				cout << "删除" << x << "成功" << endl;
			}
			else
			{
				cout << x << "不在set中" << endl;
			}

			for (auto e : s)
			{
				cout << e << " ";
			}
			cout << endl;
		}
	

}
////键值对———pair的定义
//template <class T1, class T2>
//struct pair
//{
//	typedef T1 first_type;
//	typedef T2 second_type;
//	T1 first;
//	T2 second;
//	pair() : first(T1()), second(T2())
//	{}
//	pair(const T1& a, const T2& b) : first(a), second(b)
//	{}
//};


void test_set3()
{
	multiset<int> s1;
	s1.insert(1);
	s1.insert(1);
	s1.insert(2);
	s1.insert(3);
	s1.insert(3);
	s1.insert(4);
	s1.insert(4);
	s1.insert(4);
	s1.insert(5);

	multiset<int>::iterator it = s1.begin();
	while (it != s1.end())
	{
		cout << *it << " ";
		it++;
	}

	cout << endl;

	auto ret = s1.find(1);
	cout << *ret << endl;
	while (ret != s1.end() && *ret == 1)
	{
		cout << *ret << " ";
		++ret;
	}
	cout << endl;
	cout << s1.count(1) << " ";
	cout << s1.count(5) << " ";

}


void test_map1()
{
	map<string, string> dict;
	//dict.insert(pair<string, string>("sort", "排序"));//匿名对象的好处
	dict.insert(make_pair("sort", "排序"));
	dict.insert(make_pair("string", "字符串"));
	dict.insert(make_pair("count", "计数"));
	//map<string, string>::iterator dit = dict.begin();
	auto dit = dict.begin();
	while (dit != dict.end())
	{
		cout << (*dit).first << ":" << (*dit).second << endl;
		dit++;
	}
	cout << endl;

}

void test_map2()
{
	map<string, string> dict;
	//dict.insert(pair<string, string>("sort", "排序"));
	dict.insert(make_pair("sort", "排序"));
	dict.insert(make_pair("string", "字符串"));
	dict.insert(make_pair("count", "计数"));
	dict.insert(make_pair("count", "(计数)"));//插入失败


	dict["left"] = "左面";//插入+修改
	dict["right"];//插入
	dict["count"] = "（计数）";//修改
	cout << dict["count"] << endl; // 查找

	//map<string, string>::iterator dit = dict.begin();
	auto dit = dict.begin();
	while (dit != dict.end())
	{
		//cout << (*dit).first << ":" << (*dit).second << endl;
		cout << dit->first << ":" << dit->second << endl;

		++dit;
	}
	cout << endl;
}

void test_map3()
{
	string arr[] = { "西瓜", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉", "梨" };
	map<string, int> countMap;
	//for (auto& e : arr)
	//{
	//	auto ret = countMap.find(e);
	//	if (ret == countMap.end())
	//	{
	//		countMap.insert(make_pair(e, 1));
	//	}
	//	else
	//	{
	//		ret->second++;
	//	}
	//}
	////这里operator[]的参数是key，返回值是value的引用
	for (auto& e : arr)
	{
		countMap[e]++;
	}

	for (auto& kv : countMap)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
}



//V& operator[](const K& key)
//{
//	pair<iterator, bool> ret = insert(make_pair(key, V()));
//	return ret.first->second;
//}

int main()
{
	//test_set3();
	test_map2();
	return 0;
}