#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<map>
#include<string>
using namespace std;
//int main()
//{
//	/*int a = 0;
//	auto b = a;
//	auto c = &a;
//	cout << typeid(b).name()<<endl;
//	cout << typeid(c).name() << endl;*/
//
//
//
//	map<string, string> dict;
//	typedef map<string, string>::iterator DictIt;//可以typedef
//	map<string, string>::iterator dit = dict.begin();
//	DictIt dit = dict.begin();
//	auto dit = dict.begin();//可以auto
//	
//		
//	return 0;
//}


//历遍数组简写
//int main()
//{
//	int array[] = { 3,1,5,3,9,7,5,6 };
//
//	for (auto x : array)
//	{
//		cout << x<<' ';
//	}
//	cout << endl;
//
//	for (auto& e : array)
//	{
//		e *= 2;
//		cout << e << ' ';
//	}
//	cout << endl;
//
//	for (auto x : array)
//	{
//		cout << x << ' ';
//	}
//	cout << endl;
//	return 0;
//}


void f(int)
{
	cout << "f(int)" << endl;
}

void f(int*)
{
	cout << "f(int*)" << endl;
}

int main()
{
	int* p1 = NULL;
	int* p2 = nullptr; 
	f(0);
	f(NULL);//c++有bug，会把NULL翻译成0，认成常数
	f(nullptr);

	return 0;
}