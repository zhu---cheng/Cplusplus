#pragma once
#include<stdlib.h>

class Stack
{
public://共有
	// 成员函数
	void Init(int capacity = 4);
	void Push(int x);

private://私有，还有一种protected私有
	// 成员变量
	int* a;
	int size;
	int capacity;
};