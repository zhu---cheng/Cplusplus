#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include"STACK.h"
using namespace std;

//class STACK
//{
//
//	void Init(int n = 4)
//	{
//		a = (int*)malloc(sizeof(int) * n);
//		if (a == nullptr)
//		{
//			perror("malloc fail");
//			exit(-1);
//		}
//		capacity = n;
//		size = 0;
//	}
//
//	void Push(int x)
//	{
//		a[size++] = x;
//	}
//	int* a;
//	int size;
//	int capacity;
//};

//上述是在类里封装函数，下面通过.h文件实现
// 在类中实现，编译器可能当做内联函数处理
// 
//int main()
//{
//	Stack st;
//	st.Init(3);
//	st.Push(1);
//}



//
//// 我们看看这个函数，是不是很僵硬？
//class Date
//{
//public:
//	void Init(int year)
//	{
//		// 这里的year到底是成员变量，还是函数形参？
//		year = year;
//	}
//private:
//	int year;
//};
//// 所以一般都建议这样
//class Date
//{
//public:
//	void Init(int year)
//	{
//		_year = year;
//	}
//private:
//	int _year;
//};



////class的默认访问权限为private，struct为public(因为struct要兼容C)
//解答：C++需要兼容C语言，所以C++中struct可以当成结构体使用。另外C++中struct还可以用来
//定义类。和class定义类是一样的，区别是struct定义的类默认访问权限是public，class定义的类
//默认访问权限是private。注意：在继承和模板参数列表位置，struct和class也有区别，后序给大
//家介绍。

//class Date
//{
//public:
//	void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//public:
//	int _year;
//	int _month;
//	int _day;
//
//};
//
////
////面向对象的三大特性：封装、继承、多态。
////在类和对象阶段，主要是研究类的封装特性，那什么是封装呢？
////封装：将数据和操作数据的方法进行有机结合，隐藏对象的属性和实现细节，仅对外公开接口来
////和对象进行交互。
////封装本质上是一种管理，让用户更方便使用类。
//
//
//// 类中仅有成员函数
////class A2 {
////public:
////	void f2() {}
////};
////// 类中什么都没有---空类
////class A3
////{};
//
////他们所占用的空间都为1，为了标识空间
////类的开辟空间同结构体，内存对齐原则
//
//class A2
//{
//public:
//	void f2()
//	{
//		;
//	}
//};
//
//int main()
//{
//	Date d1;
//	Date d2;//类对象实例化，开辟了空间
//	//设计图--》一栋栋别墅
//	// 为什么成员变量在对象中，成员函数不在对象中呢？
//	// 每个对象成员变量时不一样的，需要独立存储
//	// 每个对象调用成员函数是一样的，放到共享公共区域(代码段）
//
//	d1.Init(2023, 2, 23);
//	d1._year++;
//
//	d2.Init(2022, 2, 3);
//	d2._year++;
//
//	cout << sizeof(d1) << endl;
//
//	A2 aa1;
//	A2 aa2;
//	cout << &aa1 << endl;
//	cout << &aa2 << endl;
//	cout << sizeof(A2) << endl;
//
//	return 0;
//}



class Date
{
public:
	void Init(int year, int month, int day)
	{
		/*_year = year;
		_month = month;
		_day = day;*/
		cout << this << endl;
		this->_day = day;
		this->_month = month;
		this->_year = year;
	}

	void Func()
	{
		cout << this << endl;
		cout << "Func" << endl;
	}

public:
	int _year;
	int _month;
	int _day;

};

int main()
{
	Date d1;
	Date d2;
	d1.Init(2022, 3, 1);
	d2.Init(2023, 3, 2);
	d2.Func();
	//为何d1调用函数时候，函数会知道是d1调用的，而不是d2呢
	//这里就引用了this函数
	//this函数是编译器自带的，不需要我们在传参了
	//this存在哪里？-- 栈，因为他是隐含形参 / vs下面是通过ecx寄存器
	

    //下面看实例
	//	// 编译报错  运行崩溃   正常运行
	Date* ptr = nullptr;
	ptr->Init(2022, 2, 2); //运行崩溃，这里是因为this->_year等空指针解引用了

	ptr->Func();           //正常运行
	(*ptr).Func();           //正常运行   这两个都是this是空指针，通过this标识的地址为前提来访问函数


	/*C++中通过引入this指针解决该问题，即：C++编译器给每个“非静态的成员函数“增加了一个隐藏
		的指针参数，让该指针指向当前对象(函数运行时调用该函数的对象)，在函数体中所有“成员变量”
		的操作，都是通过该指针去访问。只不过所有的操作对用户是透明的，即用户不需要来传递，编
		译器自动完成。
		8.2 this指针的特性
		1. this指针的类型：类类型 * const，即成员函数中，不能给this指针赋值。
		2. 只能在“成员函数”的内部使用
		3. this指针本质上是“成员函数”的形参，当对象调用成员函数时，将对象地址作为实参传递给
		this形参。所以对象中不存储this指针。
		4. this指针是“成员函数”第一个隐含的指针形参，一般情况由编译器通过ecx寄存器自动传
		递，不需要用户传递*/

}

//
//c语言和c++对比
//
//C++中通过类可以将数据 以及 操作数据的方法进行完美结合，通过访问权限可以控制那些方法在
//类外可以被调用，即封装，在使用时就像使用自己的成员一样，更符合人类对一件事物的认知。
//而且每个方法不需要传递Stack* 的参数了，编译器编译之后该参数会自动还原，即C++中 Stack*
//参数是编译器维护的，C语言中需用用户自己维护。