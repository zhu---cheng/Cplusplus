#define _CRT_SECURE_NO_WARNINGS 1
#include"STACK.h"

	void Stack::Init(int n)
	{
		a = (int*)malloc(sizeof(int) * n);
		if (a == nullptr)
		{
			perror("malloc fail");
			exit(-1);
		}
		capacity = n;
		size = 0;
	}

	void Stack::Push(int x)
	{
		a[size++] = x;
	}