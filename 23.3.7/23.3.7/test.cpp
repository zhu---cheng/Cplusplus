#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;
//int main()
//{
//
//	//int* p1 = new int;  // 不会初始化
//	int* p1 = new int(10); // 申请一个int，初始化10
//	int* p3 = new int[10]; // 申请10个int的数组
//	int* p4 = new int[10] {1, 2, 3, 4};
//
//	int* p2 = (int*)malloc(sizeof(int));
//	if (p2 == nullptr)
//	{
//		perror("malloc fail");
//	}
//
//	return 0;
//}


class A
{
public:
	A(int a = 0)
		:
		_a(a)
	{
		cout << "A():" << endl;
	}

	~A()
	{
		cout << "~A():" << endl;
	}
private:
	int _a;
};

//int main()
//{
//	//new delete和malloc free的区别就是
//	//new/delete对于【自定义类型】除了开空间还会调用构造函数和析构函数
//	A* p1 = (A*)malloc(sizeof(int));
//	A* p2 = new A(1);
//
//	free(p1);
//	delete p2;
//
//	//malloc free和new delete 匹配使用，否则有时候会出问题
//
//
//	//对于内置类型来说，两组类型几乎是一样的
//	int* p3 = (int*)malloc(sizeof(int));
//	int* p4 = new int;
//
//	free(p3);
//	delete p4;
//
//
//	A* p5 = (A*)malloc(sizeof(A) * 10);
//	A* p6 = new A[10];
//
//	free(p5);
//	delete [] p6;//注意加[]
//	//其实在调用析构函数前四个字节空间存放了要调用的次数
//	// 
//	// delete相当于调用析构函数和operator delete（free）
//	// new相当于调用了构造函数和operator new（malloc），只是如果开辟不成功是抛异常而不是返回空指针
//	//这里可以看出来调用了10组构造函数和析构函数
//
//
//	return 0;
//}

class Stack
{
public:
	Stack()
	{
		cout << "Stack()" << endl;
		_a = new int[4];
		top = 0;
		capacity = 4;
	}

	~Stack()
	{
		cout << "~Stack()" << endl;
		delete[]_a;
		top = 0;
		capacity = 0;
	}
	
private:
	int* _a;
	int top;
	int capacity;
};

//int main()
//{
		// 失败了抛异常
	//int* p1 = (int*)operator new(sizeof(int*));

	//// 失败返回nullptr
	//int* p2 = (int*)malloc(sizeof(int*));
	//if (p2 == nullptr)
	//{
	//	perror("malloc fail");
	//}

	// 申请空间 operator new -> 封装malloc
	// 调用构造函数
	//A* p5 = new A;

	//// 先调用析构函数
	//// 再operator delete p5指向的空间
	//// operator delete -> free
	//delete p5;

	//// 申请空间 operator new[] ->perator new-> 封装malloc
	//// 调用10次构造函数
	//A* p6 = new A[10];
	//
	//// 先调用10次析构函数
	//// 再operator delete[] p6指向的空间
	//delete[] p6;


	//int* p7 = new int[10];
	//free(p7);  // 正常释放

	//A* p8 = new A;
	////free(p8); // 少调用的析构函数
	//delete p8;
	////对于无空间开辟的少调用析构函数可以正常运行


	////Stack st;

	//Stack* pts = new Stack;
	////free(pts);//这样就少调用了析构函数，会造成内存泄漏
	//delete pts;


//	return 0;
//}




//int main()
//{
//	size_t size = 0;
//	while (1)
//	{
//		int* p1 = (int*)malloc(1024 * 1024 * 4);
//		if (p1 == nullptr)
//		{
//			break;
//		}
//
//		size += 1024 * 1024 * 4;
//		cout << p1 << endl;
//	}
//
//	cout << size << endl;
//	cout << size / 1024 / 1024 << "MB" << endl;
//
//
//	return 0;
//}


//
//int main()
//{
//	size_t size = 0;
//
//	try//c++抛异常处理 try catch，后续会讲
//	{
//		while (1)
//		{
//			int* p1 = new int[1024 * 1024 * 4];
//			size += 1024 * 1024 * 4;
//			cout << p1 << endl;
//		}
//	}
//	catch (const exception& e)
//	{
//		cout << e.what() << endl;
//	}
//
//	cout << size << endl;
//	cout << size / 1024 / 1024 << "MB" << endl;
//
//
//	return 0;
//}




//int main()
//{
//	A aa;
//
//	A* p1 = (A*)malloc(sizeof(A));
//	if (p1 == nullptr)
//	{
//		perror("malloc fail");
//	}
//
//	// 对一块已有的空间初始化 -- 定义new
//	// 平常定义new用不到，但是比如内存池，他是先开辟好一些空间供使用
//	// 使用时就可以用定义new初始化
//	//new(p1)A;
//	new(p1)A(1);
//
//	A* p2 = new A;
//
//	p1->~A();
//	free(p1);
//
//	delete p2;
//
//
//	return 0;
//}

//
//template<class T>
//T Add(T left, T right)
//{
//	return left + right;
//}
//
//int main()
//{
//	cout << Add(1.1, 2.2) << endl;
//	cout << Add(1, 2) << endl;
//}



//void Swap(int& left, int& right)
//{
//	int temp = left;
//	left = right;
//	right = temp;
//}
//
//void Swap(double& left, double& right)
//{
//	double temp = left;
//	left = right;
//	right = temp;
//}

// 泛型编程 -- 模板
//template<class T>
//template<typename T>
//void Swap(T& x, T& y)
//{
//	T tmp = x;
//	x = y;
//	y = tmp;
//}
//
//template<class X, class Y>
//void Func()
//{
//
//}
//
//int main()
//{
//	int a = 1, b = 2;
//	//Swap(a, b);
//	swap(a, b);
//
//	double c = 1.1, d = 2.22;
//	//Swap(a, b);
//	swap(c, d);
//
//	return 0;
//}

//template<class T>
//T Add(const T& left, const T& right)
//{
//	return left + right;
//}
//
//int main()
//{
//	int a1 = 10, a2 = 20;
//	double d1 = 10.11, d2 = 20.22;
//	// 实参传递给形参，自动推演模板类型
//	cout << Add(a1, a2) << endl;
//	cout << Add(d1, d2) << endl;
//	cout << Add(a1, (int)d1) << endl;
//	cout << Add((double)a1, d1) << endl;
//
//	// 显示实例化
//	cout << Add<int>(a1, d1) << endl;
//	cout << Add<double>(a1, d1) << endl;
//
//	return 0;
//}

// 专门处理int的加法函数
int Add(int left, int right)
{
	return left + right;
}

// 通用加法函数
template<class T>
T Add(T left, T right)
{
	return left + right;
}


// 通用加法函数
//template<class T1, class T2>
//T1 Add(T1 left, T2 right)
//{
//	return left + right;
//}

//int main()
//{
//	int a = 1, b = 2;
//	Add(a, b);
//
//	Add<int>(a, b);
//
//	Add(1, 2.2);
//
//	return 0;
//}

//typedef double STDataType;
//class Stackint
//{
//private:
//	STDataType* _a;
//	size_t _top;
//	size_t _capacity;
//};
//
//int main()
//{
//	Stack st1; // int
//	Stack st2; // double
//
//
//	return 0;
//}

template<class T>
class Stack
{
public:
	Stack(int capaicty = 4)
	{
		_a = new T[capaicty];
		_top = 0;
		_capacity = capaicty;
	}

	~Stack()
	{
		delete[] _a;
		_capacity = _top = 0;
	}

private:
	T* _a;
	size_t _top;
	size_t _capacity;
};

int main()
{
	Stack<int> st1; // int
	Stack<double> st2; // double

	vector<int> v;
	for (size_t i = 0; i < v.size(); ++i)
	{
		cout << v[i] << " ";
	}

	return 0;
}