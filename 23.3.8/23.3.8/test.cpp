#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;
//
//template<class T>
//class Vector
//{
//public:
//	Vector(size_t capacity = 10)
//		:
//		_pData(new T[capacity]),
//		_size(0),
//		_capacity(capacity)
//	{}
//
//
//	~Vector();//使用析构函数演示，在类中声明，在类外定义
//
//
//	T& operator[](size_t pos)
//	{
//		if (pos < _size)
//		{
//			return _pData[pos];
//		}
//	}
//private:
//	T* _pData;
//	size_t _size;
//	size_t _capacity;
//};
//
//
//template<class T>//还要写一遍
//Vector<T>::~Vector()
//{
//	delete[]_pData;
//	_pData = nullptr;
//	_size = _capacity = 0;
//}
//
//
//int main()
//{
//	Vector<int> v1;
//	return 0;
//}



//int main()
//{
//	char s1[] = "abcdef";
//	char s2[] = "比赛";
//
//	cout << s1 << endl;
//	cout << sizeof(s2) << endl;//一个汉字占两个字节,末尾还有一个\0
//	cout << sizeof(s1) << endl;
//
//
//	s2[3]--;
//	cout << s2 << endl;
//
//	s2[3]--;
//	cout << s2 << endl;
//
//	s2[3]--;
//	cout << s2 << endl;
//}


//
//int main()
//{
//	string s1("abcdef");
//	cout << s1 << endl;
//
//	string s2 = "123455";
//	cout << s2 << endl;
//
//	string s3 = "hello world";
//	string s4(s3, 6, 3);
//	cout << s4 << endl;
//
//
//	string s5(s3, 6, 11);//超过空间就默认结束为止
//	cout << s5 << endl;
//
//	string s6(s3, 6);//没有最后一个参数，默认到结束
//	cout << s6 << endl;
//
//	string s7("hello world", 5);//前五个
//	cout << s7 << endl;
//
//	string s8(10, '#');//10个#
//	cout << s8 << endl;
//
//	//逐一访问
//	for (size_t i = 0; i < s2.size(); i++)
//	{
//		cout << s2[i] << " ";
//	}
//
//
//
//	return 0;
//}




//
//int main()
//{
//	string s1 = "hello world";
//	cout << s1.length() << endl;
//	cout << s1.size() << endl;//两者是一样的
//	cout << s1.max_size() << endl;
//	cout << s1.capacity() << endl;
//}

//
//
//int main()
//{
//	string s1("hello");
//	//加字符：
//	s1.push_back(' ');
//	s1.push_back('!');
//
//	cout << s1 << endl;
//	//加字符串：
//	s1.append("world!");
//	cout << s1 << endl;
//
//	//简化版本：+=   可以加字符，也可以加字符串
//	s1 += ' ';
//	cout << s1 << endl;
//
//	
//	s1 += "world";
//	cout << s1 << endl;
//}



//int main()
//{
//	string s;
//
//	//s.reserve(100);//扩容开空间
//	
//	size_t sz = s.capacity();
//	cout << "make a grow:" << endl;
//	cout << "capacity changed:" << sz<<endl;
//	for (int i = 0; i < 100; ++i)
//			{
//				s.push_back('c');
//				if (sz != s.capacity())
//				{
//					sz = s.capacity();
//					cout << "capacity changed: " << sz << '\n';
//				}
//			}
//
//}

//
//int main()
//{
//	// 扩容
//	string s1("hello world");
//	s1.reserve(100);
//	cout << s1.size() << endl;
//	cout << s1.capacity() << endl;
//
//	// 扩容+初始化
//	string s2("hello world");
//	s2.resize(100, 'x');
//	cout << s2.size() << endl;
//	cout << s2.capacity() << endl;
//
//	// 比size小，删除数据，保留前5个
//	s2.resize(5);
//	cout << s2.size() << endl;
//	cout << s2.capacity() << endl;//容量还在
//
//	return 0;
//}


//
//
//int main()
//{
//	string s1("hello world");
//	string::iterator it = s1.begin();
//	while (it != s1.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//
//	for (auto ch : s1)
//	{
//		cout << ch << " ";
//	}
//	cout << endl;
//
//	return 0;
//}
//
////迭代器
//int main()
//{
//	string s1("hello world");
//	string::iterator it = s1.begin();
//	while (it != s1.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//
//	for (auto ch : s1)
//	{
//		cout << ch << " ";
//	}
//	cout << endl;
//}