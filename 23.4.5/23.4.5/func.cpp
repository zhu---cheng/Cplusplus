#define _CRT_SECURE_NO_WARNINGS 1

#include "Func.h"

void func()
{
	cout << "void func()" << endl;
}
//
//template<class T>
//T Add(const T& left, const T& right)
//{
//	return left + right;
//}
//
//// 显示实例化可以声明定义分开，但是不常用
//template
//double Add<double>(const double& left, const double& right);
//
//template
//int Add<int>(const int& left, const int& right);