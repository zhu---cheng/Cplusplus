#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<vector>
#include<array>
using namespace std;
//
////N是非类型模板参数，在类中当做常量来使用
////只能是整型常量
//template<class T,int N=20>
//class Array
//{
//public:
//
//
//private:
//
//	T _a[N];
//};
//
//
//
////template<class T, double N = 20.2>
//template<class T, int N = 'a'>
//void func1(const T& a)
//{
//	//N = 10;
//}
//
//template<class T, bool flag = true>
//void func2(const T& a)
//{
//	//N = 10;
//}
//
//int main()
//{
//	Array<int> a0;
//	Array<int ,10> a1;
//
//	func1(1);
//	func2(3);
//
//	array<int, 10> a2;//读写全面检查，检查更细致，但一般还是用vector
//	return 0;
//}

//注意：
//1. 浮点数、类对象以及字符串是不允许作为非类型模板参数的。
//2. 非类型的模板参数必须在编译期就能确认结果。



class Date
{
public:
	Date(int year = 1900, int month = 1, int day = 1)
		: _year(year)
		, _month(month)
		, _day(day)
	{}

	bool operator<(const Date& d)const
	{
		return (_year < d._year) ||
			(_year == d._year && _month < d._month) ||
			(_year == d._year && _month == d._month && _day < d._day);
	}

	bool operator>(const Date& d)const
	{
		return (_year > d._year) ||
			(_year == d._year && _month > d._month) ||
			(_year == d._year && _month == d._month && _day > d._day);
	}

	friend ostream& operator<<(ostream& _cout, const Date& d)
	{
		_cout << d._year << "-" << d._month << "-" << d._day;
		return _cout;
	}

private:
	int _year;
	int _month;
	int _day;
};

//
//// 函数模板 -- 参数匹配
//template<class T>
//bool Less(T left, T right)
//{
//	return left < right;
//}
//
////函数模板特化
////template<>
////bool Less<Date*>(Date* left,Date* right)
////{
////	return *left < *right;
////}
//
////但是函数模板特化不实用，一般情况下如果函数模板遇到不能处理或者处理有误的类型
////，为了实现简单通常都是将该函数直接给出。
////函数重载即可
//
////因此函数模板不建议特化
//bool Less(Date* left, Date* right)
//{
//	return *left < *right;
//}
//
//
//
//int main()
//{
//	cout << Less(1, 2) << endl;   // 可以比较，结果正确
//
//	Date d1(2022, 7, 7);
//	Date d2(2022, 7, 8);
//	cout << Less(d1, d2) << endl;  // 可以比较，结果正确
//
//	Date* p1 = &d1;
//	Date* p2 = &d2;
//	cout << Less(p1, p2) << endl;  // 可以比较，结果错误（没有函数模板特化前）
//
//	int* p3 = new int(1);
//	int* p4 = new int(2);
//	cout << Less(p3, p4) << endl;  // 可以比较
//
//	return 0;
//}
//

/// //////////////////////////////////////////////////
//主要应用   类模板的特化
template <class T>
struct Less
{
	bool operator()(const T& l, const T& r)const
	{
		return l < r;
	}
};

//全特化
//template<>
//struct Less<Date*>
//{
//	bool operator()(const Date* l, const Date* r)const
//	{
//		return *l < *r;
//	}
//};

//
////偏特化---进一步限制
//template<class T>
//struct Less<T*>
//{
//	bool operator()(const T* l, const T* r)const
//	{
//		return *l < *r;
//	}
//};
//
//
//
//int main()
//{
//	Date d1(2022, 7, 7);
//	Date d2(2022, 7, 8);
//	cout << Less<Date>()(d1, d2) << endl;
//
//	Date* p1 = &d1;
//	Date* p2 = &d2;
//	cout << Less<Date*>()(p1, p2) << endl;
//
//	int* p3 = new int(1);
//	int* p4 = new int(2);
//	cout << Less<int*>()(p3, p4) << endl;
//
//	return 0;
//}



//
//template<class T1, class T2>
//class Data
//{
//public:
//	Data() { cout << "Data<T1, T2>" << endl; }
//private:
//	T1 _d1;
//	T2 _d2;
//};
//
//template<>
//class Data<int, char>
//{
//public:
//	Data() { cout << "Data<int, char>" << endl; }
//private:
//	int _d1;
//	char _d2;
//};
//
//template<>
//class Data<int*, char*>
//{
//public:
//	Data() { cout << "Data<int*, char*>" << endl; }
//private:
//	int _d1;
//	char _d2;
//};
//
//template <class T1>
//class Data<T1, int>
//{
//public:
//	Data() { cout << "Data<T1, int>" << endl; }
//private:
//	T1 _d1;
//	int _d2;
//};
//
//template <typename T1, typename T2>
//class Data <T1*, T2*>
//{
//public:
//	Data() { cout << "Data<T1*, T2*>" << endl; }
//
//private:
//	T1 _d1;
//	T2 _d2;
//};
//
//template <typename T1, typename T2>
//class Data <T1&, T2&>
//{
//public:
//	Data(const T1& d1 = T1(), const T2& d2 = T2())
//		: _d1(d1)
//		, _d2(d2)
//	{
//		cout << "Data<T1&, T2&>" << endl;
//	}
//
//private:
//	const T1& _d1;
//	const T2& _d2;
//};
//
//template <typename T1, typename T2>
//class Data <T1&, T2*>
//{
//public:
//	Data(const T1& d1 = T1())
//
//	{
//		cout << "Data<T1&, T2*>" << endl;
//	}
//};
//int main()
//{
//	Data<int, int> d1;
//	Data<int*, char*> d2;
//	Data<int*, int> d3;
//	Data<double, int> d4;
//
//	Data<int*, int*> d5;
//
//	Data<int&, char&> d6;
//	Data<int&, char*> d7;
//
//	return 0;
//}
//


#include"func.h"
//void func();

int main()
{
	Add(1, 2);
	Add(1.0, 2.0); // call func(0xdadada)

	//func(); // call func(?)

	return 0;
}












//////////////////////////////////////////////////////////////////
//#include <iostream>
//using namespace std;
//#include <vector>
//int main()
//{
//	vector<int> v{ 1,2,3,4,5,6 };
//
//	auto it = v.begin();
//
//	// 将有效元素个数增加到100个，多出的位置使用8填充，操作期间底层会扩容
//	//v.resize(100, 8);
//
//	// reserve的作用就是改变扩容大小但不改变有效元素个数，操作期间可能会引起底层容量改变
//	// v.reserve(100);
//
//	// 插入元素期间，可能会引起扩容，而导致原空间被释放
//	// v.insert(v.begin(), 0);
//	// v.push_back(8);
//
//	// 给vector重新赋值，可能会引起底层容量改变
//	v.assign(100, 8);
//
//	/*
//	出错原因：以上操作，都有可能会导致vector扩容，也就是说vector底层原理旧空间被释放掉，
//   而在打印时，it还使用的是释放之间的旧空间，在对it迭代器操作时，实际操作的是一块已经被释放的
//   空间，而引起代码运行时崩溃。
//	解决方式：在以上操作完成之后，如果想要继续通过迭代器操作vector中的元素，只需给it重新
//   赋值即可。
//	*/
//	while (it != v.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//	return 0;
//}


//
//#include <iostream>
//using namespace std;
//#include <vector>
//int main()
//{
//	vector<int> v{ 1, 2, 3, 4 };
//	auto it = v.begin();
//	while (it != v.end())
//	{
//		if (*it % 2 == 0)
//			v.erase(it);
//		++it;
//	}
//
//	return 0;
//}
//int main()
//{
//	vector<int> v{ 1, 2, 3, 4 };
//	auto it = v.begin();
//	while (it != v.end())
//	{
//			if (*it % 2 == 0)
//				it = v.erase(it);
//			else
//				++it;
//	}
//	return 0;
//}