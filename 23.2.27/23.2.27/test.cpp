#define _CRT_SECURE_NO_WARNINGS 1
#include"Date.h"
int main()
{
	Date d1(2022, 3, 2);
	Date d2(d1);
	Date d3;
	cout << (d2 == d1) << endl;

	d1 += 2;
	d1.Print();


	cout << "=========================" << endl;

	d2 = d1 + 3;
	d2.Print();

	d2++;
	d2.Print();
	
	d3 = d2++;
	d3.Print();


	return 0;
}

