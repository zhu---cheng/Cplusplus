#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//class Date
//{
//public:
//	Date(int year=1, int month=1, int day=1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//
//	//Date(const Date& d)
//	//{
//	//	_year = d._year;
//	//	_month = d._month;
//	//	_day = d._day;
//	//}
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//
//
//typedef int DataType;
//class Stack
//{
//public:
//	Stack(size_t capacity = 10)
//	{
//		_array = (DataType*)malloc(capacity * sizeof(DataType));
//		if (nullptr == _array)
//		{
//			perror("malloc申请空间失败");
//			return;
//		}
//		
//			_size = 0;
//		_capacity = capacity;
//	}
//	void Push(const DataType& data)
//	{
//		// CheckCapacity();
//		_array[_size] = data;
//		_size++;
//	}
//
//	Stack(const Stack& st)
//	{
//		cout << "Stack(const Stack & st)" << endl;
//		_array = (DataType*)malloc(sizeof(DataType) * (st._capacity));
//		if (_array == nullptr)
//		{
//			perror("malloc fail");
//			exit(-1);
//		}
//		memcpy(_array, st._array, sizeof(DataType) * (st._size));
//		_capacity = st._capacity;
//		_size = st._size;
//	}
//	//写完拷贝构造就可以正常运行了
//	~Stack()
//	{
//		if (_array)
//		{
//			free(_array);
//			_array = nullptr;
//			_capacity = 0;
//			_size = 0;
//		}
//	}
//private:
//	DataType* _array;
//	size_t _size;
//	size_t _capacity;
//};
//
//
//class MyQueue
//{
//public:
//	//	// 默认生成构造
////	// 默认生成析构
////	// 默认生成拷贝构造
//
//private:
//	Stack _PushST;//自定义类型，可以直接用默认构造和析构函数
//	Stack _PopST;
//	int _size;
//};
//int main()
//{
//	Date d1(2022, 3, 2);
//	d1.Print();
//
//	//Date d2(d1);
//	Date d2 = d1;
//	//如果不给拷贝构造函数，编译器会自动生成默认拷贝构造函数
//	//但是这样是浅拷贝，也称值拷贝
//	d2.Print();
//
//	// 这里会发现下面的程序会崩溃掉？这里就需要我们以后讲的深拷贝去解决。
//	Stack s1;
//	s1.Push(1);
//	s1.Push(2);
//	s1.Push(3);
//	s1.Push(4);
//	Stack s2(s1);
//	//所以涉及到空间资源管理的我们要写拷贝构造函数，浅拷贝的不需要写
//
//	cout << "============================" << endl;
//	MyQueue q1;
//	MyQueue q2(q1);
//
//	return 0;
//}




////////////////////////////////////////////////////
//运算符重载
class Date
{
public:
	Date(int year = 1900, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
    }

	void Print()
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}
//d1==d2->  d1.operator==(d2)

	bool operator == (const Date & d)
	{
		return _year == d._year &&
			_month == d._month &&
			_day == d._day;
	}

	//d1<d2
	bool operator<(const Date& d)
	{
		/*if (_year < d._year)
			return true;
		if (_year == d._year && _month < d._month)
			return true;
		if (_year ==d._year && _month == d._month && _day < d._day)
			return true;
		else
			return false;*/

		return (_year < d._year) ||
			(_year == d._year && _month < d._month) ||
			(_year == d._year && _month == d._month && _day < d._day);


	}


	bool operator<=(const Date& d)
	{
		return (*this < d) || (*this == d);
	}


	bool operator>(const Date& d)
	{
		return !(*this <= d);
	}

	bool operator>=(const Date& d)
	{
		return !(*this < d);

	}

	bool operator!=(const Date& d)
	{
		return !(*this == d);
	}

	//返回值为了支持连续赋值，保持运算符的特性

	Date operator=(const Date& d)
	{
		if (this != &d)
		{
			_year = d._year;
			_month = d._month;
			_day = d._day;
		}

		return *this;
	}
private:
	int _year;
	int _month;
	int _day;
};

int main()
{
	Date d1(2028, 2, 1);
	Date d2(2022, 2, 1);
	cout << (d1 == d2) << endl;

	cout << (d1 < d2) << endl;

	cout << (d1 <= d2) << endl;

	d1 = d2;
	d1.Print();
}