#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include"BSTree.h"
using namespace std;
void TestBSTree1()
{
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	key::BSTree<int> t1;

	//for (auto e : a)
	//{
	//	t1.Insert(e);
	//}
	for (auto e : a)
	{
		t1.InsertR(e);
	}
	//t1.InOrder(t1.GetRoot());
	t1.InOrder();

	//t1.Erase(4);
	//t1.InOrder();

	//t1.Erase(14);
	//t1.InOrder();

	//t1.Erase(3);
	//t1.InOrder();

	//t1.Erase(8);
	//t1.InOrder();


	for (auto e : a)
	{
		t1.EraseR(e);
		t1.InOrder();
	}

	t1.InOrder();
}

void TestBSTree2()
{
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	key::BSTree<int> t1;

	for (auto e : a)
	{
		t1.InsertR(e);
	}
	t1.InOrder();

	t1.EraseR(10);
	t1.EraseR(14);
	t1.EraseR(13);
	t1.InOrder();


	for (auto e : a)
	{
		t1.EraseR(e);
		t1.InOrder();
	}

	t1.InOrder();
}

void TestBSTree3()
{
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	key::BSTree<int> t1;
	for (auto e : a)
	{
		t1.InsertR(e);
	}

	t1.InOrder();

	key::BSTree<int> t2(t1);
	t2.InOrder();
}

void TestBSTree4()
{
	key_value::BSTree<string, string> dict;
	dict.Insert("sort", "����");
	dict.Insert("left", "���");
	dict.Insert("right", "�ұ�");
	dict.Insert("string", "�ַ���");
	dict.Insert("insert", "����");
	dict.Insert("erase", "ɾ��");

	string str;
	while (cin >> str)
	{
		auto ret = dict.Find(str);
		if (ret)
		{
			cout << ":" << ret->_value << endl;
		}
		else
		{
			cout << "�޴˵���" << endl;
		}
	}
}

void TestBSTree5()
{
	string arr[] = { "����", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����", "ƻ��", "�㽶", "ƻ��", "�㽶", "��" };

	key_value::BSTree<string, int> countTree;
	for (auto str : arr)
	{
		//key_value::BSTreeNode<string, int>* ret = countTree.Find(str);
		auto ret = countTree.Find(str);
		if (ret == nullptr)
		{
			countTree.Insert(str, 1);
		}
		else
		{
			ret->_value++;
		}
	}

	countTree.InOrder();
}

int main()
{
	TestBSTree5();

	return 0;
}
