#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//class Date
//{
//public:
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print()
//	{
//		cout << "Print()" << endl;
//		cout << "year:" << _year << endl;
//		cout << "month:" << _month << endl;
//		cout << "day:" << _day << endl << endl;
//	}
//	/*void Print() const
//	{
//		cout << "Print()const" << endl;
//		cout << "year:" << _year << endl;
//		cout << "month:" << _month << endl;
//		cout << "day:" << _day << endl << endl;
//	}*/
//private:
//	int _year; // 年
//	int _month; // 月
//	int _day; // 日
//};
//void Test()
//{
//	Date d1(2022, 1, 13);
//	d1.Print();
//	const Date d2(2022, 1, 13);
//	d2.Print();
//}
//int main()
//{
//	Test();
//}
// 
// 
//class Date
//{
//public:
//	Date(int year, int month, int day)
//
//		: _year(year)
//		, _month(month)
//		, _day(day)
//	{}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
// 
//class A
//{
//public:
//	A(int a)
//		:_a1(a)
//		, _a2(_a1)
//	{}
//
//	void Print() {
//		cout << _a1 << " " << _a2 << endl;
//	}
//private:
//	int _a2;
//	int _a1;
//};
//int main() {
//	A aa(1);
//	aa.Print();
//}
//
//class Date
//{
//public:
//	
//		// 1. 单参构造函数，没有使用explicit修饰，具有类型转换作用
//		// explicit修饰构造函数，禁止类型转换---explicit去掉之后，代码可以通过编译
//	//explicit Date(int year)
//	//	:_year(year)
//	//{}
//	
//	// 2. 虽然有多个参数，但是创建对象时后两个参数可以不传递，没有使用explicit修饰，具
//   //有类型转换作用
//	// explicit修饰构造函数，禁止类型转换
//	explicit Date(int year, int month = 1, int day = 1)
//	: _year(year)
//	, _month(month)
//	, _day(day)
//	{}
//	
//	Date& operator=(const Date& d)
//	{
//		if (this != &d)
//		{
//			_year = d._year;
//			_month = d._month;
//			_day = d._day;
//		}
//		return *this;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//void Test()
//{
//	Date d1(2022);
//	// 用一个整形变量给日期类型对象赋值
//	// 实际编译器背后会用2023构造一个无名对象，最后用无名对象给d1对象进行赋值
//	//d1 = 2023;
//	// 将1屏蔽掉，2放开时则编译失败，因为explicit修饰构造函数，禁止了单参构造函数类型转
//	//换的作用
//}
//int main()
//{
//	Test();
//}
//class A
//
//{
//public:
//	A() { ++_scount; cout << "A()" << endl; }
//	A(const A& t) { ++_scount; cout << "const A()" << endl;
//	}
//	~A() { --_scount; cout << "~A()" << endl;
//	}
//	static int GetACount() { return _scount; }
//private:
//	static int _scount;
//};
//int A::_scount = 0;
//int main()
//{
//	cout << A::GetACount() << endl;
//	A a1, a2;
//	A a3(a1);
//	cout << A::GetACount() << endl;
//}

//
//class A
//{
//public:
//	A(int a = 0)
//		:_a(a)
//	{
//		cout << "A(int a)" << endl;
//
//	}
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//private:
//	int _a;
//};
//class Solution {
//public:
//	int Sum_Solution(int n) {
//		//...
//		return n;
//	}
//};
//int main()
//{
//	A aa1;
//	// 不能这么定义对象，因为编译器无法识别下面是一个函数声明，还是对象定义
//	//A aa1();
//	// 但是我们可以这么定义匿名对象，匿名对象的特点不用取名字，
//	// 但是他的生命周期只有这一行，我们可以看到下一行他就会自动调用析构函数
//	A();
//	A aa2(2);
//	// 匿名对象在这样场景下就很好用，当然还有一些其他使用场景，这个我们以后遇到了再说
//	Solution().Sum_Solution(10);
//	return 0;
//}
//class A
//{
//public:
//	A(int a = 0)
//		:_a(a)
//	{
//		cout << "A(int a)" << endl;
//	}
//	A(const A& aa)
//		:_a(aa._a)
//	{
//		cout << "A(const A& aa)" << endl;
//	}
//	
//		A& operator=(const A& aa)
//	{
//		cout << "A& operator=(const A& aa)" << endl;
//		if (this != &aa)
//		{
//			_a = aa._a;
//		}
//		return *this;
//	}
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//private:
//	int _a;
//};
//void f1(A aa)
//{}
//A f2()
//{
//	A aa;
//	return aa;
//}
//int main()
//{
//	// 传值传参
//	A aa1;
//	f1(aa1);
//	cout << endl;
//	// 传值返回
//	f2();
//	cout << endl;
//	// 隐式类型，连续构造+拷贝构造->优化为直接构造
//	f1(1);
//	// 一个表达式中，连续构造+拷贝构造->优化为一个构造
//	f1(A(2));
//	cout << endl;
//	// 一个表达式中，连续拷贝构造+拷贝构造->优化一个拷贝构造
//	A aa2 = f2();
//	cout << endl;
//	// 一个表达式中，连续拷贝构造+赋值重载->无法优化
//	aa1 = f2();
//	cout << endl;
//	return 0;
//}
//class A
//{
//private:
//	static int k;
//	int h=100;
//public:
//	class B // B天生就是A的友元
//	{
//	public:
//		void foo(const A& a)
//		{
//			cout << k << endl;//OK
//			cout << a.h << endl;//OK
//		}
//	};
//};
//int A::k = 1;
//int main()
//{
//	A::B b;
//	b.foo(A());
//
//	return 0;
//}
////#include<iostream>
//using namespace std;
//int main()
//{
//	int a = 21;
//	a ^= (1 << 5) - 1;
//	cout << a << endl;
//}
//
//#include<iostream>
//#include<string>
//using namespace std;
//
//int main()
//{
//    string s1;
//    string s2;
//    string s3;
//    getline(cin, s1);
//    getline(cin, s2);
//
//    //They are students.
//    //    aeiou
//    //    Thy r stdnts.
//    for (int i = 0; i < s1.size(); i++)
//    {
//        for(int j=0;j<s2.size();j++)
//        {
//            if (s1[i] == s2[j])
//                break;
//            else if (s1[i]!=s2[j]&&(j == s2.size() - 1))
//            {
//                s3 += s1[i];
//            }
//        }  
//        
//    }
//
//    cout << s3;
//}


//
//#include<iostream>
//#include <algorithm>
//#include<string>
//using namespace std;
//
//int main()
//{
//    int n;
//    scanf("%d", &n);
//
//    int a_i[10000] = { 0 };
//    
//    for (int i = 0; i < 3 * n; i++)
//    {
//       scanf("%d",&a_i[i]);
//      
//    }
//
//    sort(a_i,a_i+3*n);
//
//    int sum=0;
//    for (int i = 0; i < n; i++)
//    {
//        sum += a_i[i + n];
//    }
//    cout << sum;
//}
