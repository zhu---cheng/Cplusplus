#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//class Base
//{
//public:
//	void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//
//	virtual void Func2()
//	{
//		cout << "Func3()" << endl;
//	}
//
//	virtual void Func3()
//	{
//		cout << "Func1()" << endl;
//	}
//
//private:
//	int _b = 1;
//	char _ch;
//};
//
////有了虚函数之后对象中就有了一个表 -- 虚表(虚函数表)
//int main()
//{
//	Base b;
//	cout << sizeof(Base) << endl;
//
//	return 0;
//}

//
//
//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//
//private:
//	int _b = 1;
//};
//
//class Derive : public Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Derive::Func1()" << endl;
//	}
//
//	void Func3()
//	{
//		cout << "Derive::Func3()" << endl;
//	}
//private:
//	int _d = 2;
//};
//
//int main()
//{
//	Base b;
//	Derive d;
//
//	return 0;
//}

//
//
//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//
//private:
//	int _b = 1;
//};
//
//class Derive : public Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Derive::Func1()" << endl;
//	}
//
//	void Func3()
//	{
//		cout << "Derive::Func3()" << endl;
//	}
//
//	virtual void Func4()
//	{
//		cout << "Derive::Func4()" << endl;
//	}
//private:
//	int _d = 2;
//};
////正确定义：
//typedef void(*V_FUNC)();
//
////void PrintVFTable(V_FUNC a[]) -- 数组在传参的时候都会退化成了指针
//
////void PrintVFTable(void(**a)())-- 不用typedef的写法
//void PrintVFTable(V_FUNC* a)
//{
//	printf("vfptr:%p\n", a);
//
//	//**切记这里要记得清理解决方案** -- 不然会有非法访问
//	//g++的话在这里就要写死，因为它的虚表中不存在空指针
//	for (size_t i = 0; a[i] != nullptr; i++)
//	{
//		//printf("[%d]:%p\n", i, a[i]);
//
//		printf("[%d]:%p->", i, a[i]);
//
//		//用函数的地址直接去调用函数 -- 通过函数打印出结果便于观察
//		V_FUNC f = a[i];
//		f();
//	}
//	cout << endl;
//}
//
////int main()
////{
////	Derive d;
////	PrintVFTable((V_FUNC*)(*((int*)&d)));
////	return 0;
////}
//
//
//
//
//int c = 2;
//
//int main()
//{
//	Base b1;
//	Base b2;
//	Base b3;
//	Base b4;
//
//	//打印虚表
//	PrintVFTable((V_FUNC*)(*((int*)&b1)));
//	PrintVFTable((V_FUNC*)(*((int*)&b2)));
//	PrintVFTable((V_FUNC*)(*((int*)&b3)));
//	PrintVFTable((V_FUNC*)(*((int*)&b4)));
//
//	//方向验证 -- 对比验证
//	int a = 0;
//	static int b = 1;
//	const char* str = "hello world";
//	int* p = new int[10];
//
//	printf("栈：%p\n", &a);
//	printf("静态区/数据段：%p\n", &b);
//	printf("静态区/数据段：%p\n", &c);
//	printf("常量区/代码段：%p\n", str);
//	printf("堆：%p\n", p);
//	cout << endl;
//
//	printf("虚表：%p\n", (*((int*)&b4)));
//	cout << endl;
//
//	//成员函数取地址都得这么玩
//	//函数编译完了是一段指令，第一句指令的地址就可以认为是函数的地址
//	printf("函数地址：%p\n", &Derive::Func3);
//	printf("函数地址：%p\n", &Derive::Func2);
//	printf("函数地址：%p\n", &Derive::Func1);
//
//	return 0;
//}
class Base1 {
public:
	virtual void func1() { cout << "Base1::func1" << endl; }
	virtual void func2() { cout << "Base1::func2" << endl; }
private:
	int b1;
};
class Base2 {
public:
	virtual void func1() { cout << "Base2::func1" << endl; }
	virtual void func2() { cout << "Base2::func2" << endl; }
private:
	int b2;
};
class Derive : public Base1, public Base2 {
public:
	virtual void func1() { cout << "Derive::func1" << endl; }
	virtual void func3() { cout << "Derive::func3" << endl; }
private:
	int d1;
};
typedef void(*VFPTR) ();
void PrintVTable(VFPTR vTable[])
{
	cout << " 虚表地址>" << vTable << endl;
	for (int i = 0; vTable[i] != nullptr; ++i)
	{
		printf(" 第%d个虚函数地址 :0X%x,->", i, vTable[i]);
		VFPTR f = vTable[i];
		f();
	}
	cout << endl;
}
int main()
{
	Derive d;
	VFPTR* vTableb1 = (VFPTR*)(*(int*)&d);
	PrintVTable(vTableb1);
	VFPTR* vTableb2 = (VFPTR*)(*(int*)((char*)&d + sizeof(Base1)));

	PrintVTable(vTableb2);

	printf("%p", &Derive::func1);
	return 0;
}