#pragma once

//
//#include<iostream>
//#include<assert.h>
//#include<algorithm>
//
//using namespace std;
//
//
//template<class K,class V>
//struct AVLTreeNode
//{
//	AVLTreeNode<K, V>* _left;
//	AVLTreeNode<K, V>* _right;
//	AVLTreeNode<K, V>* _parent;
//	pair<K, V> _kv;
//	int _bf;//平衡因子
//
//	AVLTreeNode(const pair<K, V>& kv)
//		:
//		_left(nullptr),
//		_right(nullptr),
//		_parent(nullptr),
//		_kv(kv),
//		_bf(0)
//	{}
//};
//
//
//template<class K,class V>
//struct AVLTree
//{
//	typedef AVLTreeNode<K, V> Node;
//
//public:
//	bool insert(const pair<K, V>& kv)
//	{
//		if (_root == nullptr)
//		{
//			_root = new Node(kv);
//			return true;
//		}
//
//		Node* parent = nullptr;
//		Node* cur = _root;
//
//		while (cur)
//		{
//			if (cur->_kv.first < kv.first)
//			{
//				parent = cur;
//				cur = cur->_right;
//			}
//
//			else if (cur->_kv.first > kv.first)
//			{
//				parent = cur;
//				cur = cur->_left;
//			}
//			else
//			{
//				return false;
//			}
//		}
//		cur = new Node(kv);
//		if (parent->_kv.first > kv.first)
//		{
//			parent->_left = cur;
//		}
//		else if (parent->_kv.first < kv.first)
//		{
//			parent->_right = cur;
//		}
//		cur->_parent = parent;
//
//
//		//更新平衡因子
//
//		while (parent)
//		{
//			if (parent->_right == cur)
//			{
//				parent->_bf++;
//			}
//			if (parent->_left == cur)
//			{
//				parent->_bf--;
//			}
//
//			if (parent->_bf == 1 || parent->_bf == -1)
//			{
//				parent = parent->_parent;
//				cur = cur->_parent;
//			}
//
//			else if (parent->_bf == 0)
//			{
//				break;
//			}
//
//			else if (parent->_bf == 2 || parent->_bf == -2)
//			{
//				// 需要旋转处理 -- 1、让这颗子树平衡 2、降低这颗子树的高度
//				if (parent->_bf == 2 && cur->_bf == 1)
//				{
//					RotateL(parent);
//				}
//
//				else if (parent->_bf == -2 && cur->_bf == -1)
//				{
//					RotateR(parent);
//				}
//
//				else if (parent->_bf == 2 && cur->_bf == -1)
//				{
//					RotateRL(parent);
//				}
//
//				else if (parent->_bf == -2 && cur->_bf == 1)
//				{
//					RotateLR(parent);
//				}	
//				break;
//			}
//			else
//				{
//					assert(false);
//				}
//
//		}
//		return true;
//	}
//
//	void InOrder()
//	{
//		_InOrder(_root);
//		cout << endl;
//	}
//
//	bool IsBalance()
//	{
//		return _IsBalance(_root);
//	}
//
//	int Height()
//	{
//		return _Height(_root);
//	}
//
//	private:
//		void _InOrder(Node* root)
//		{
//			if (root == nullptr)
//			{
//				return;
//			}
//
//			_InOrder(root->_left);
//			cout << root->_kv.first << " ";
//			_InOrder(root->_right);
//	    }
//
//		int _Height(Node* root)
//		{
//			if (root == nullptr)
//			{
//				return 0;
//			}
//
//			int leftH = _Height(root->_left);
//			int rightH = _Height(root->_right);
//
//			return leftH > rightH ? leftH + 1 : rightH + 1;
//		}
//
//		bool _IsBalance(Node* root)
//		{
//			if (root == nullptr)
//			{
//				return true;
//			}
//
//			int HeightL = _IsBalance(root->_left);
//			int HeightR = _IsBalance(root->_right);
//
//			if (HeightR - HeightL != root->_bf)
//			{
//				cout << root->_kv.first << "平衡因子异常" << endl;
//				return false;
//			}
//
//			return abs(HeightL - HeightR) < 2 && _IsBalance(root->_left) && _IsBalance(root->_right);
//		}
//	//左单旋
//	void RotateL(Node* parent)
//	{
//		Node* subR = parent->_right;
//		Node* subRL = subR->_left;
//
//		parent->_right = subRL;
//		if (subRL)
//		{
//			subRL->_parent = parent;
//		}
//
//		Node* ppnode = parent->_parent;
//		subR->_left = parent;
//		parent->_parent = subR;
//
//		if (ppnode == nullptr)
//		{
//			_root = subR;
//			_root->_parent = nullptr;
//		}
//
//		else
//		{
//			if (ppnode->_left == parent)
//			{
//				ppnode->_left = subR;
//			}
//
//			else if (ppnode->_right == parent)
//			{
//				ppnode->_right = subR;
//			}
//
//			subR->_parent == ppnode;
//		}
//		parent->_bf = subR->_bf = 0;
//	}
//
//
//	//右单旋
//	void RotateR(Node* parent)
//	{
//		Node* subL = parent->_left;
//		Node* subLR = subL->_right;
//
//		parent->_left = subLR;
//		if (subLR)
//		{
//			subLR->_parent = parent;
//		}
//
//		Node* ppnode = parent->_parent;
//		subL->_right = parent;
//		parent->_parent = subL;
//
//		if (parent == _root)
//		{
//			_root = subL;
//			_root->_parent = nullptr;
//		}
//
//		else
//		{
//			if (ppnode->_left == parent)
//			{
//				ppnode->_left = subL;
//			}
//			else if (ppnode->_right == parent)
//			{
//				ppnode->_right = subL;
//			}
//
//			subL->_parent == ppnode;
//		}
//		parent->_bf = subL->_bf = 0;
//	}
//
//	//左右双旋
//	void RotateLR(Node* parent)
//	{
//		Node* subL = parent->_left;
//		Node* subLR = subL->_right;
//		int bf = subLR->_bf;
//
//		RotateL(parent->_left);
//		RotateR(parent);
//
//
//		if (bf == 1)
//		{
//			parent->_bf = 0;
//			subL->_bf = -1;
//			subLR->_bf = 0;
//		}
//
//
//		else if (bf == -1)
//		{
//			parent->_bf = 1;
//			subL->_bf = 0;
//			subLR->_bf = 0;
//		}
//
//		else if (bf == 0)
//		{
//			parent->_bf = 0;
//			subL->_bf = 0;
//			subLR->_bf = 0;
//		}
//
//		else
//		{
//			assert(false);
//		}
//	}
//
//
//	//右左双旋
//	void RotateRL(Node* parent)
//	{
//		Node* subR = parent->_right;
//		Node* subRL = subR->_left;
//		int bf = subRL->_bf;
//
//		RotateR(parent->_right);
//		RotateL(parent);
//
//		if (bf == 0)
//		{
//			subRL->_bf = 0;
//			parent->_bf = 0;
//			subR->_bf = 0;
//		}
//		else if (bf == 1)
//		{
//			subRL->_bf = 0;
//			parent->_bf = -1;
//			subR->_bf = 0;
//		}
//		else if (bf == -1)
//		{
//			subRL->_bf = 0;
//			parent->_bf = 0;
//			subR->_bf = 1;
//		}
//		else
//		{
//			//subLR->_bf旋转前就有问题
//			assert(false);
//		}
//	}
//
//private:
//	Node* _root = nullptr;
//};

#include<iostream>
#include<assert.h>
#include<algorithm>
using namespace std;
template<class K, class V>
struct AVLTreeNode
{
	AVLTreeNode<K, V>* _left;
	AVLTreeNode<K, V>* _right;
	AVLTreeNode<K, V>* _parent;
	pair<K, V> _kv;
	int _bf;//平衡因子

	AVLTreeNode(const pair<K, V>& kv)
		:
		_left(nullptr),
		_right(nullptr),
		_parent(nullptr),
		_kv(kv),
		_bf(0)
	{}
};



template<class K, class V>
class AVLTree
{
	typedef AVLTreeNode<K, V> Node;
public:
	bool Insert(const pair<K, V>& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}

		Node* parent = nullptr;
		Node* cur = _root;


		while (cur)
		{
			if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}

		cur = new Node(kv);
		if (parent->_kv.first > kv.first)
		{
			parent->_left = cur;
		}
		else
		{
			parent->_right = cur;
		}
		cur->_parent = parent;


		//更新平衡因子
		while (parent)
		{
			if (cur == parent->_right)
			{
				parent->_bf++;
			}
			else
			{
				parent->_bf--;
			}

			if (parent->_bf == 1 || parent->_bf == -1)
			{
				parent = parent->_parent;
				cur = cur->_parent;
			}
			else if (parent->_bf == 0)
			{
				break;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				// 需要旋转处理 -- 1、让这颗子树平衡 2、降低这颗子树的高度

				//子树不平衡了 -- 需要旋转处理（左单旋的特征 -- 右边高）
				if (parent->_bf == 2 && cur->_bf == 1)//左单旋
				{
					RotateL(parent);
				}
				//子树不平衡了 -- 需要旋转处理（右单旋的特征 -- 左边高）
				else if (parent->_bf == -2 && cur->_bf == -1)//右单旋
				{
					RotateR(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == 1)//左右双旋
				{
					RotateLR(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == -1)//右左双旋
				{
					RotateRL(parent);
				}
				//旋转完之后ppNode为根的子树高度不变 -- 所以对ppNode的平衡因子没有影响
				break;
			}
			else // 一定要检查 -- 不保证其他地方不会出现错误
			{
				//插入之前AVL数就存在平衡子树，|平衡因子| >= 2结点
				assert(false);
			}
		}
		return true;
	}


	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}

	bool IsBalance()
	{
		return _IsBalance(_root);
	}

	int Height()
	{
		return _Height(_root);
	}
private:

	bool _IsBalance(Node* root)
	{
		if (root == nullptr)
			return true;

		int HeightL = _Height(root->_left);
		int HeightR = _Height(root->_right);

		if (HeightR - HeightL != root->_bf)
		{
			cout << root->_kv.first << "平衡因子异常" << endl;
			return false;
		}

		return abs(HeightL - HeightR) < 2 && _IsBalance(root->_left) && _IsBalance(root->_right);

		////空树也是AVL树
		//if (nullptr == root)
		//	return true;

		////计算pRoot节点的平衡因子：即pRoot左右子树的高度差
		//int leftHeight = _Height(root->_left);
		//int rightHeight = _Height(root->_right);

		////求差值
		//int diff = rightHeight - leftHeight;

		////如果计算出的平衡因子与pRoot的平衡因子不相等，或者
		////pRoot平衡因子的绝对值超过1，则一定不是AVL树
		//if (abs(diff) >= 2)
		//{
		//	cout << root->_kv.first << "结点平衡因子异常" << endl;
		//	return false;
		//}

		////平衡因子没有异常但是和结点的对不上
		//if (diff != root->_bf)
		//{
		//	//说明更新有问题
		//	cout << root->_kv.first << "结点平衡因子不符合实际" << endl;
		//	return false;
		//}

		////pRoot的左和右如果都是AVL树，则该树一定是AVL树
		////把自己和自己的左右子树都检查了，递归检查
		//return _IsBalance(root->_left)
		//	&& _IsBalance(root->_right);
	}


	int _Height(Node* root)
	{
		if (root == NULL)
			return 0;

		int leftH = _Height(root->_left);
		int rightH = _Height(root->_right);

		return leftH > rightH ? leftH + 1 : rightH + 1;
	}



	void _InOrder(Node* root)
	{
		if (root == nullptr)
			return;

		_InOrder(root->_left);
		cout << root->_kv.first << " ";
		_InOrder(root->_right);
	}

	//左单旋
	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		parent->_right = subRL;
		if (subRL)
		{
			subRL->_parent = parent;
		}

		Node* ppnode = parent->_parent;

		subR->_left = parent;
		parent->_parent = subR;

		if (ppnode == nullptr)
		{
			_root = subR;
			_root->_parent = nullptr;
		}

		else
		{
			if (ppnode->_left == parent)
			{
				ppnode->_left = subR;
			}
			else
			{
				ppnode->_right = subR;
			}

			subR->_parent = ppnode;
		}

		parent->_bf = subR->_bf = 0;
	}

	//右单旋
	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;


		Node* ppnode = parent->_parent;

		subL->_right = parent;
		parent->_parent = subL;
		if (parent == _root)
		{
			_root = subL;
			_root->_parent = nullptr;
		}

		else
		{
			if (ppnode->_left == parent)
			{
				ppnode->_left = subL;
			}
			else
			{
				ppnode->_right = subL;
			}

			subL->_parent = ppnode;
		}

		parent->_bf = subL->_bf = 0;
	}

	void RotateLR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		int bf = subLR->_bf;

		RotateL(parent->_left);
		RotateR(parent);

		if (bf == 1)
		{
			parent->_bf = 0;
			subLR->_bf = 0;
			subL->_bf = -1;
		}
		else if (bf == -1)
		{
			parent->_bf = 1;
			subLR->_bf = 0;
			subL->_bf = 0;
		}
		else if (bf == 0)
		{
			parent->_bf = 0;
			subLR->_bf = 0;
			subL->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}


	void RotateRL(Node* parent)
	{

		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		int bf = subRL->_bf;

		RotateR(parent->_right);
		RotateL(parent);
		if (bf == 1)
		{
			parent->_bf = -1;
			subRL->_bf = 0;
			subR->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 0;
			subRL->_bf = 0;
			subR->_bf = 1;
		}
		else if (bf == 0)
		{
			parent->_bf = 0;
			subRL->_bf = 0;
			subR->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

private:
	Node* _root = nullptr;
};



void Test_AVLTree1()
{
	//int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
	AVLTree<int, int> t1;
	for (auto e : a)
	{
		/*	if (e == 14)
			{
			int x = 0;
			}*/

		t1.Insert(make_pair(e, e));
		cout << e << "插入：" << t1.IsBalance() << endl;
	}

	t1.InOrder();
	cout << t1.IsBalance() << endl;
}



void Test_AVLTree2()
{
	srand(time(0));
	const size_t N = 10;
	AVLTree<int, int> t;
	for (size_t i = 0; i < N; ++i)
	{
		size_t x = rand() + i;
		t.Insert(make_pair(x, x));
		//cout << t.IsBalance() << endl;
	}

	t.InOrder();

	cout << t.IsBalance() << endl;
	cout << t.Height() << endl;
}