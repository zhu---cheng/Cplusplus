#pragma once

#include<iostream>
#include<assert.h>
using namespace std;

namespace ZC
{

	class string 
	{
		

	public:


		typedef char* iterator;
		typedef const char* const_iterator;

		iterator begin()
		{
			return _str;
		}
		iterator end()
		{
			return _str + _size;
		}

		const iterator begin()const
		{
			return _str;
		}
		const iterator end()const
		{
			return _str + _size;
		}
		
		//构造函数
		string(const char* str = "")
			:
			_size(strlen(str))
		{
			_capacity =_size== 0 ? 3 : _size;
			_str = new char[_capacity+1];
			strcpy(_str, str);
		}

		//拷贝构造函数
		string(const string& s)
			:_size(s._size),
			_capacity(s._capacity)
		{
			_str = new char[s._capacity + 1];
			strcpy(_str, s._str);
		}

		string& operator=(const string& s)
		{
			if (this != &s)
			{
				char* tmp = new char[s._capacity + 1];
				strcpy(tmp, s._str);
				delete[]_str;
				_str = tmp;
				
				_size = s._size;
				_capacity = s._capacity;
			}

			return *this;
		}

		//析构函数
		~string()
		{
			delete[] _str;
			_str = nullptr;
			_capacity = _size = 0;
		}

		const char* c_str()
		{
			return _str;
		}

		char& operator[](size_t pos)
		{
			return _str[pos];
		}

		const char& operator[](size_t pos) const
		{
			return _str[pos];
		}

		const size_t size()const
		{
			return _size;
		}

		bool operator>(const string& s)const
		{
			return strcmp(_str, s._str) >0;
		}

		bool operator==(const string& s)const
		{
			return strcmp(_str, s._str) == 0;
		}
		bool operator<(const string& s)const
		{
			return strcmp(_str, s._str) < 0;
		}

		bool operator>=(const string& s)const
		{
			return !(*this<s);
		}

		bool operator<=(const string& s)const
		{
			return *this == s || *this < s;
		}

		bool operator!=(const string& s)const
		{
			return !(*this==s);
		}

		//开空间
		void reserve(size_t n)
		{
			char* tmp = new char[n + 1];
			strcpy(tmp , _str);

			delete[]_str;
			_str = tmp;
			_capacity = n;

		}
		//开空间加初始化
		void resize(size_t n, char ch = '\0')
		{
			if (n < _size)
			{
				// 删除数据--保留前n个
				_size = n;
				_str[_size] = '\0';
			}
			else if (n > _size)
			{
				if (n > _capacity)
				{
					reserve(n);
				}

				size_t i = _size;
				while (i < n)
				{
					_str[i] = ch;
					++i;
				}

				_size = n;
				_str[_size] = '\0';
			}
		}


		void push_back(char ch)
		{
			if (_size + 1 > _capacity)
			{
				reserve(_capacity * 2);
			}
			
			_str[_size] = ch;
			++_size;
			_str[_size] = '\0';
		}


		void append(const char* str)
		{
			size_t len = strlen(str);
			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}

			strcpy(_str + _size, str);
			_size += len;
		}


		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}

		string& operator+=(const char* str)
		{
			append(str);
			return *this;
		}


		void insert(size_t pos, char ch)
		{
			assert(pos <= _size);
			if (_size + 1 > _capacity)
			{
				_capacity *= 2;
			}
			//区别end=_size   while(end>=pos)_str[end -1 ] = _str[end ];这里>=的=没法实现，因为无符号
			size_t end = _size+1;
			while (end > pos)
			{
				_str[end ] = _str[end - 1];
				end--;
			}

			_str[pos] = ch;
			++_size;
		}

		void insert(size_t pos, const char* str)
		{

			assert(pos <= _size);
			size_t len = strlen(str);
			if (_size + 1 > _capacity)
			{
				_capacity = _size + len;
			}
		
			size_t end = _size + len;
			while (end > pos+len-1)
			{
				_str[end] = _str[end - len];
				end--;
			}

			strncpy(_str + pos, str, len);
			_size += len;
		}

		void erase(size_t pos, size_t len = npos)
		{
			assert(pos <= _size);
			if (pos + len >= _size)
			{
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				while (_str[pos+len]!='\0')
				{
					_str[pos] = _str[pos + len];
					pos++;
				}

				_str[pos] = '\0';
				_size -= len;
			}
		}

		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_capacity, s._capacity);
			std::swap(_size, s._size);
		}

		size_t find(char ch, size_t pos = 0)
		{
			assert(pos < _size);

			for (size_t i = pos; i < _size; ++i)
			{
				if (_str[i] == ch)
				{
					return i;
				}
			}

			return npos;
		}

		size_t find(const char* str, size_t pos = 0)
		{
			assert(pos < _size);

			// kmp
			char* p = strstr(_str + pos, str);
			if (p == nullptr)
			{
				return npos;
			}
			else
			{
				return p - _str;
			}
		}

		void clear()
		{
			_str[0] = '\0';
			_size = 0;
		}

	private:
		char* _str;
		size_t _capacity;
		size_t _size;

		static const size_t npos;
	};

	const size_t string::npos = -1;


	ostream& operator<<(ostream& out,const string& s )
	{
		for (auto ch : s)
		{
			out << ch;

		}

		return out;
	}

	//istream& operator>>(istream& in, string& s)
	//{
	//	s.clear();

	//	char ch = in.get();
	//	char buff[128];
	//	size_t i = 0;
	//	while (ch != ' ' && ch != '\n')
	//	{
	//		buff[i++] = ch;
	//		if (i == 127)
	//		{
	//			buff[127] = '\0';
	//			s += buff;
	//			i = 0;
	//		}

	//		ch = in.get();
	//	}

	//	if (i != 0)
	//	{
	//		buff[i] = '\0';
	//		s += buff;
	//	}

	//	return in;
	//}

	istream& operator>>(istream& in, string& s)
	{
		s.clear();
		
		char ch = in.get();

		while (ch != ' ' && ch != '\n')
		{
			s += ch;
			ch++;
			ch = in.get();
		}
		return in;
	}


	void test_string1()
	{
		string s1("hello zzzccc");
		string s2 = "hello world";
		string s3 = s1;

		cout << s1.c_str() << endl;
	}

	void Print()
	{
		string s1 = "hello world";
		for (size_t i = 0; i < s1.size(); i++)
		{
			cout << s1[i];
		}
		cout << endl;

		//迭代器版本
		string::const_iterator it = s1.begin();
		while (it != s1.end())
		{
			cout << *it;
			++it;
		}
		cout << endl;



		for (auto ch : s1)
		{
			cout << ch << " ";
		}
		cout << endl;
	}


	void test_string2()
	{
	  string s1("abcd");
	  string s2("abce");
	  cout << (s1 > s2) << endl;
	  cout << (s1 >= s2) << endl;
	  cout << (s1 < s2) << endl;

	}


	void test_string3()
	{
		string s1 = "hello world";
		s1.reserve(30);
		//s1.resize(30,'a');

		s1.push_back('a');
		cout << s1.c_str() << endl;

		s1.append("abcdef");
		cout << s1.c_str() << endl;

		
		s1+="asdef";
		cout << s1.c_str() << endl;

		s1.insert(2, 'z');
		cout << s1.c_str() << endl;

	}

	void test_string4()
	{
		/*string s1 = "0123456789";
		s1.erase(0, 3);
		cout << s1.c_str() << endl;

		
		s1.insert(0, '2');
		cout << s1 << endl;*/

		string s3;

		cin >> s3;
		cout << s3;

	}
};